\chapter{Transactional Memory}
\label{chap:concurrency}

\section{Concurrent Programming}

Concurrency is difficult: there are many sources of error unique to concurrent programs. In real-world projects, most concurrency bugs can be categorised as \textit{deadlock}, \textit{ordering violations}, or \textit{atomicity violations} \cite{lu2008}.

Deadlock, ordering and atomicity violations are both examples of failures of a concurrent program to maintain an assumption about which threads of execution are accessing which resources at what time. Ordering violations occur when a resource is accessed out of order (\autoref{fig:concurrency:order_violation}), and atomicity violations occur when one thread assumes it has sole access to a resource while another thread is violating that assumption (\autoref{fig:concurrency:atomicity_violation}). 

There is an extensive literature on detecting and avoiding deadlock in a variety of formal systems  \cite{li_survey_2008} \cite{elmagarmid_survey_1986}. For the most part deadlock is orthogonal to security aside from the more general topic of the interaction of security with control flow, which we discuss in \autoref{sec:information_flow:implicit}.

Regarding ordering and atomicity, those regions of the programs relying on shared resources are examples of \textit{critical sections}. When the behaviour of interleaved critical sections is distinguishable from strictly sequential execution of a program (usually resulting from uncontrolled concurrent access to shared resources), this is called a \textit{data race} or \textit{racy behaviour} \cite{carr_race_2001}.

The canonical way to prevent errors in critical sections is to restrict access to resources via locks or semaphores. There are various issues applying them effectively in large projects \cite{michael_scalable_2004}, and so several other techniques have been developed trying to avoid some of these issues, including lock-free programming based on compare-and-swap operators, as well as transactional memory.

\begin{figure}[pt]

\begin{subfigure}{.5\textwidth}
\centering
\begin{lstlisting}
use_field(shared.field);
\end{lstlisting}
\subcaption{Thread A}
\end{subfigure}
% gotta suppress this inner space for some reason, A+ latex
\begin{subfigure}{.5\textwidth}
\centering
\begin{lstlisting}
shared = make_shared(); // leaves 
			// shared.field 
			// as null
shared.field = make_field();
\end{lstlisting}
\subcaption{Thread B}
\end{subfigure}
\vspace{1em}
\begin{center}
\begin{subfigure}{0.5\textwidth}
\centering
\begin{lstlisting}
// shared initially null.
(*\textcolor{blue}{shared = make\_shared();}*)
// shared.field is still null.
(*\textcolor{red}{use\_field(shared.field);}*) // error!
(*\textcolor{blue}{shared.field = make\_field();}*)
\end{lstlisting} 

\subcaption{An example problematic trace. Actions by \textcolor{red}{thread A} are in red, \textcolor{blue}{thread B} in blue.}
\end{subfigure}
\end{center}

\caption[Ordering Violation]{An example of an ordering violation. Thread A may attempt to use the resource \texttt{shared} before thread B has initialised it. Assume \texttt{shared} begins as \texttt{null}. Example adapted from \cite{arpaci_dusseau_2012}.}

\label{fig:concurrency:order_violation}
\end{figure}

\begin{figure}[pt]

\begin{subfigure}{.5\textwidth}
\centering
\begin{lstlisting}
shared = 1;
print(shared);
\end{lstlisting}
\subcaption{Thread A}
\end{subfigure}
% gotta suppress this inner space for some reason, A+ latex
\begin{subfigure}{.5\textwidth}
\centering
\begin{lstlisting}
shared = 2;
print(shared);
\end{lstlisting}
\subcaption{Thread B}
\end{subfigure}
%
\begin{center}
\begin{subfigure}{0.5\textwidth}
\centering
\begin{lstlisting}
(*\textcolor{blue}{shared = 2;}*)
(*\textcolor{red}{shared = 1;}*)
(*\textcolor{red}{print(shared);}*) // prints 1
(*\textcolor{blue}{print(shared);}*) // prints 1, expect 2
\end{lstlisting}
\subcaption{One possible execution. Thread A sees \texttt{shared} as though it had sole access, but thread B does not. Actions by \textcolor{red}{thread A} are in red, \textcolor{blue}{thread B} in blue.}
\end{subfigure}
\end{center}

\caption[Atomicity Violation]{An example of an atomicity assumption violation. Thread A assumes it may safely write to and read from \texttt{shared} assuming it has sole access. Thread B does the same, with the result that neither thread can reliably reason about the behaviour of \texttt{print(shared)}.}
\label{fig:concurrency:atomicity_violation}
\end{figure}

\section{Transactional Memory}
\label{sec:concurrency:transactional_memory}

While lock-based and lock-free programming enforce critical sections by preventing control flow from violating them, transactional memory instead prevents racy behaviour from being \textit{observed}. 

Consider \autoref{fig:concurrency:atomicity_violation}. Say we force the actions of thread A and thread B to run as transactions, and then consider the example trace. At the point where thread A wrote to the shared variable after thread B, the two transactions are in \textit{conflict}: if they were allowed to continue, we might observe data-racy behaviour. Transactional memory attempts to \textit{abort} one of the conflicting transactions, undoing its effects, followed by \textit{retrying} the transaction.

In this example, aborting and retrying thread A's transaction and allowing thread B's to continue uninterupted will remove the racy behaviour. Transactional memory doesn't directly address order violations \cite{volos_applying_2012}, however it can be used to implement other concurrency primitives that allow for the required synchronisation.

When transactional memory is implemented in software (as opposed to hardware), it is called Software Transactional Memory (STM).

\subsection{Semantics}
\label{sec:concurrency:semantics}

A precise and concise definition of the desirable properties of STM is given by \cite{herlihy_transactional_1993}. In particular, if some parts $\{t_1,\dots,t_n\}$ of a program are to be executed transactionally, then these executions must be

\begin{description}
 \item [Serialisable:] The result of executing each of $t_i$ (potentially in parallel) must be indistinguishable from some  execution of each of them in sequence.
 \item [Atomic:] After attempting to run $t_i$, the observable state of the world must be indistinguishable from either running $t_i$ to completion successfully, or not having run $t_i$ at all.
\end{description}

\cite{blundell_subtleties_2006} expand on atomicity, discussing \textit{weak} versus \textit{strong} atomicity:

\begin{description}
 \item [Weak Atomicity:] Each transaction $t_i$ executes atomically \textit{with respect to the other transactions}, and possibly may not execute atomically if a non-transactional part of the program interferes with resources used in $t_i$.
 \item [Strong Atomicity:] Each transaction $t_i$ runs atomically, \textit{even when interfered with by non-transactional parts of the program}.
\end{description}

A sizeable amount of the literature on the implementation of STM in a general imperative setting deals with ensuring these properties \cite{blundell_subtleties_2006}\cite{shpeisman_enforcing_2007}\cite{abadi_semantics_2011}, however we will soon see an implementation of STM that sidesteps these issues. 

\section{STM in Functional Languages}

There is at least one research variant of OCaml, named AtomCaml, with a transactional primitive $\mathtt{atomic}: \forall a.\ (\mathtt{unit} \rightarrow a) \rightarrow a$, with the semantics that $\mathtt{atomic}(f)$ performs $f$, retrying if the guarantees of transactional evaluation are violated, and returning the final value when successful \cite{ringenburg2005}. 

The OCaml family of languages in general doesn't statically track side effects \cite{minsky2013real}, and the authors require users of the library to provide callbacks to implement the rollback of such side effects. For many actions such as hardware IO, this is difficult to implement as acknowledged by the authors. 

The AtomCaml $\mathtt{atomic}$ primitive suffers from the same compositionality issues as similar primitives in other languages,  namely the difficulty in nesting or sequencing transactions. \cite{blundell_subtleties_2006} and \cite{harris_composable_2005} catalogue the potential pitfalls of trying to compose transactions. \cite{gramoli_composing_2010} outline the sensitivity of compositionality to the implementation semantics of STM.

The implementation of STM in Glasgow Haskell\cite{harris_composable_2005} addresses these concerns by using Haskell's sophisticated type system to track whether an expression has an externally observable side effect or is intended to run atomically\cite{benton_monads_2002}. In a minimal formulation, there are two higher-kinded types $\mathtt{IO}\ a$ and $\mathtt{STM}\ a$. $\mathtt{IO}$ tracks terms that produce a value of type $a$ while potentially performing irreversible IO side effects, and similarly $\mathtt{STM}$ tracks those that must run transactionally.

Similarly to AtomCaml, Haskell's STM offers a primitive $\mathtt{atomic}: \forall a.\ \mathtt{STM}\ a \rightarrow \mathtt{IO}\ a$, but there is no method\footnotemark $\mathtt{runIO}: \forall a.\ \mathtt{IO}\ a \rightarrow a$. In addition, creating a variable to access transactionally is an $\mathtt{SMT}$ term. Combined, this prevents programmers writing transactional terms with internal irreversible actions: such an expression $e$ would have type $\mathtt{STM}\ (\mathtt{IO}\ a)$, and $\mathtt{atomic}$ won't evaluate the `inner' $\mathtt{IO}$ term. This deals with the issues of weak atomicity and external observation previously discussed.

The use of a monadic interface allows Haskell's implementation of STM to be highly composable. We will discuss the design of this interface in more detail in \autoref{chap:infoflow_sstml}, where we use it as a foundation for a toy language.

\footnotetext{This is not strictly true. Like most high-level languages, Haskell has an `escape hatch' \texttt{unsafePerformIO}  with this signature, however as the name suggests using it is not a recommended practice.}
