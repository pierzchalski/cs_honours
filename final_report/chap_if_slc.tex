\newcommand{\slcstep}{\rightsquigarrow_v}

\chapter{A Simple Lambda Calculus}
\label{chap:infoflow_slc}

The lambda calculus is one of the foundational models of computation. One of its distinguishing features is the simplicity of its semantics, and so it is a natural area in which to explore information flow before progressing to more sophisticated and realistic models.

\section{Term Representation}

The grammar of the standard lambda calculus is given in \autoref{fig:infoflow_slc:lambda_calculus_grammar}. The term $\lambda x.\ e$ is a function construction primitive: it marks $x$ as a \textit{bound variable} in $\lambda x.\ e$. If a variable isn't bound in a term $e$, then it is \textit{free} in $e$. Terms $f \circ x$ indicate function application: $x$ is applied as an argument to the term $f$, which is hopefully a function\footnotemark. The application term $\circ$ is left-associative so that $a \circ b \circ c = (a \circ b) \circ c$, and $\circ$ binds stronger than $\lambda$ so that $\lambda x.\ x \circ y = \lambda x.\ (x \circ y)$. All other terms are variables. 

\footnotetext{Almost all depictions of the lambda calculus don't have a syntactic construction for application: instead, $a\ b$ is the application of argument $b$ to function $a$. We use a more explicit definition here since most of our language definitions will have similar explicit constructions.}

The lambda calculus comes with a small-step evaluation relation between terms called \textit{beta reduction}, which is given in \autoref{fig:infoflow_slc:lambda_calculus_beta}. The judgement $a \rightarrow_\beta b$ indicates term $a$ evaluates to term $b$ after one `step'. The expression $f[x \mapsto e]$ is the term that results from replacing each free instance of variable $x$ in term $f$ with term $e$, so for instance $(x \circ y)[x \mapsto e] = e \circ y$. 

\subsection{Bound Variables and Alpha Equivalence}

A more complex example of substitution is that $(x \circ \lambda x.\ x)[x \mapsto y] = y \circ \lambda x.\ x$: the inner $\lambda$-term binds $x$, and so only the outer instance of $x$ is substituted. Another example, which demonstrates an unintended side-effect of the informal definition of substitution that we've used so far, is the following:

\begin{center}
\begin{align*}
(\lambda y.\ \lambda x.\ x \circ y) \circ a \circ b & \rightarrow_\beta (\lambda x.\ x \circ y)[y \mapsto a] \circ b \\
	& = (\lambda x.\ x \circ a) \circ b \\
	& \rightarrow_\beta (x \circ a)[x \mapsto b] \\
	& = b \circ a \\
(\lambda y.\ \lambda x.\ x \circ y) \circ x \circ b & \rightarrow_\beta (\lambda x.\ x \circ y)[y \mapsto x] \circ b \\
	& = (\lambda x.\ x \circ x) \circ b \\
	& \rightarrow_\beta (x \circ x)[x \mapsto b] \\
	& = b \circ b
\end{align*}
\end{center}

Here we apply two variables to a function $f = \lambda y.\ \lambda x.\ x \circ y$. In the first case, we apply the variables $a$ and $b$. In the second case, we apply $x$ and $b$. Intuitively looking at the definition of $f$, we expect it to take its' second argument and apply it to its' first argument. However, in the second case we used an argument $x$ which is bound inside part of $f$. This results in unexpected behaviour.

The standard solution to this problem is to re-write inner terms like $\lambda x.\ x \circ y$ in a way that preserves their semantics but removes name collisions. This produces an equivalence between terms called \textit{alpha equivalence}: two terms are alpha-equivalent if they are equal up to the renaming of bound variables, avoiding collisions with any free variables. For instance, $\lambda x.\ x$ and $\lambda y.\ y$ are alpha-equivalent, but $\lambda x.\ x \circ y$ and $\lambda y.\ y \circ y$ are not.

Using this technique we can re-write $f$ into the alpha-equivalent term $\lambda y.\ \lambda z.\ z \circ y$ which will avoid name collisions when we apply $x$ as an argument. In general, if we are about to perform a substitution $a[x \mapsto b]$ and find ourselves needing to rename a bound variable $y$ we will need to choose a new name that is neither bound in $a$ nor free in $b$.

However, having to care about name collisions at all is a heavy burden: many theorems we want to prove about terms in our language will have to show that they are preserved under alpha equivalence, and our evaluation semantics will need to include a way to resolve these name collisions `at run-time' by choosing new variable names under the constraints just discussed.

\subsection{De Bruijn Indices}

Since we care more about run-time semantics than about the aesthetics of terms in our language, we can use the technique of \textit{De Bruijn indices} developed to avoid many of these issues. This involves removing variable bindings from $\lambda$-terms and replacing variable terms with natural numbers (in our case, using a constructor $V$ to disambiguate between any other numerical terms we might add to the language). 

To interpret a number as a variable, we use it as a count of the depth of $\lambda$-terms separating it from its' original binding site. For example, consider the term 

\vspace{-1\baselineskip}
\begin{align*}
\lambda.\ V\ \textit{0}\ \circ\ \lambda.\ V\ \textit{1}\ \circ\ V\ \textit{0}.
\end{align*}

This is equivalent to the classical lambda calculus term 

\vspace{-1\baselineskip}
\begin{align*}
\lambda x.\ x \circ \lambda y.\ x \circ y.
\end{align*}

The first instance of $x$ in the classical term is bound by the closest $\lambda$-term, and so has De Bruijn index 0. The second instance of $x$ is `under' another $\lambda$-term binding $y$; since there is one binding separating $x$ from its' original binding site, this second instance has De Bruijn index 1.

\subsection{De Bruijn Substitution}
\label{chap:infoflow_slc:db_subst}

All the classical beta reduction rules still apply when using De Bruijn indices, except for substitution. Before we had $(\lambda x.\ a) \circ b \rightarrow_\beta a[x \mapsto b]$; now we have $(\lambda.\ a) \circ b$ and need to somehow replace all relevant instances of the `variable' 0 in $a$ with $b$.

Conveniently, terms in the De Bruijn-index lambda calculus are alpha equivalent if and only if they are equal. Thus, the problem of finding new names for variables to avoid collisions is replaced with the problem of adjusting these De Bruijn indices when substituting terms.

Terms using De Bruijn indices are notorious for being hard to read and write, and for requiring error-prone arithmetic when defining many name manipulation operations. However, for our purposes it suffices to use the simple definitions that exist in Isabelle's HOL library examples:

\begin{center}
\vspace{-1\baselineskip}
\exSubstDef
\exLiftDef
\end{center}

Here, both substitution and $\textit{lift}$ are assumed to only be used when handling the term $(\lambda.\ a) \circ b$. Hence, substitution will decrement the index of some of the variables it finds, since there will be fewer $\lambda$-term bindings over them. Similarly, $\textit{lift}$ prepares terms to be substituted into another by incrementing the index of variables in the term depending on how many $\lambda$-binding terms they get substituted behind.

These definitions clearly have a straightforward computational interpretation in terms of evaluating language terms, successfully avoiding any issues with name generation.

\subsection{Data and Security Labels}
\label{chap:infoflow_slc:data_and_labels}

The original lambda calculus has a spartan definition: there are no datatypes beyond functions, type definitions, or type annotations. Despite this, there are several methods of `encoding' advanced datatypes such as natural numbers and lists via the clever use of $\lambda$-terms. 

Unfortunately, these encoding techniques tend to be very verbose. For our purposes of having small, readable examples, it suffices to have a simple fragment of computations on natural numbers: we include natural number literals in our language, as well as a `$+$' operator.

In order to explore security and information flow, we need a way to force terms to be treated as though they are of a particular security level. We opt for the straightforward method of allowing terms to be annotated with a level: $L(e|l)$ labels the term $e$ as having label $l$. 

This is not a very realistic model of security, in that it consists of a program being manually annotated to introduce security labelling at all: we will see a more interesting method of introducing security-relevant terms in \autoref{chap:infoflow_sstml}. The full grammar for our simple labelled lambda calculus is given in 	\autoref{fig:infoflow_slc:slc_grammar}. 

Note that we now need to extend our previous definitions of substitution and $\textit{lift}$ to handle our new terms, although the extension is not very exciting:

\begin{center}
	\vspace{-1\baselineskip}
	\exSubstDefExt
	\exLiftDefExt
\end{center}


\section{Dynamic Semantics}

Beta reduction has one issue that prevents us from using it as our evaluation relation on terms: it is non-deterministic. For example, the expression $(\lambda.\ a) \circ b$ has three possible beta reductions, depending on the values of $a$ and $b$. This increases our workload when proving that a relation between terms is preserved under evaluation. 

Say that we want to show if $a \sim b$ and $a \rightarrow a^\prime$ and $b \rightarrow b^\prime$, then $a \sim b$ for some target relation $\sim$ and evaluation relation $\rightarrow$. We would need to consider all possibe pairs of evaluation steps over $a$ and $b$, which becomes tiresome for little gain. In practice, very few languages have genuinely non-deterministic evaluation semantics\footnotemark, so we opt to restrict ours to the deterministic relation $\slcstep$ described in \autoref{fig:infoflow_slc:eval_rules}. Crucially, determinism requires that terms of the form $\lambda.\ e$ have no further evaluation steps.

\footnotetext{Most sources of non-determinism in real-world programs, such as kernel scheduling, hardware caches, and IO operations, are clearly not part of the semantics of term evaluation. We will discuss how some of these factors affect security analyses in \autoref{chap:infoflow_sstml}.}

\section{Static Semantics}

Our goal in this chapter is to perform some static analysis on a security property of programs written in our labelled lambda calculus. To do so, we follow in the footsteps of \cite{lourenco_dependent_2015} by deriving \textit{typing judgements} on terms. We judge terms as having \textit{security types} of the form $(t\ |\ l)$, where $t$ is a \textit{value type} that may in turn contain security types, and $l$ is a security level. This grammar is defined in \autoref{fig:infoflow_slc:sec_typ_grammar}.

Our typing judgements are of the form $\exSlcTypingJudgement$, where $\Gamma$ is a list of typings indicating the type of variables `in scope', $e$ is the term being judged, and $(t\ |\ l)$ is the resulting security type. The judgement indicates that $e$ has value type $t$ and security level $l$.

Our security typing judgements are defined over security levels that form a complete lattice as described in \autoref{sec:information_flow:policy}. For presentation reasons, the highest lattice element is written as $top$ and the lowest as $bot$. When we judge a term $e$ to have type $(t\ |\ l)$, this means only observers at some level $l^\prime \geq l$ should be able to infer information about the value of $e$.

\subsection{Examples}
\label{chap:infoflow_slc:typing_examples}

Before we present the type inference rules in \autoref{chap:infoflow_slc:typing}, we present some example judgements to get a feel for how they work on our language. For simplicity, assume we have three labels $\textit{Low}$, $\textit{Med}$, and $\textit{High}$ in our security lattice, with $L < H < T$. 

\subsubsection{Labels}

We would want to be able to deduce the following type for a `forced upgrading' function $g = \lambda.\ L(V\ \textit{0}\ |\ \textit{Med})$:

\begin{gather*}
\Gamma \vdash_s g:\ ((\exNat\ |\ \textit{Low}) \Rightarrow (\exNat\ |\ \textit{Med})\ |\ \textit{Low})
\end{gather*}

Notice that the inner labelling should $L$ forces the result type of our function to have a level of \textit{at least} $\textit{Med}$. We could soundly infer that the return type is instead $(\exNat\ |\ \textit{High})$: this would be a \textit{conservative over-approximation} of the sensitivity of the result of the function.

Conversely, we \textit{cannot} soundly infer that the argument type is $\textit{High}$: the inner labelling term must require that its' argument have an equal or lower label. Without this restriction it would be difficult to enforce any security policy.

Interestingly, the level of the function itself is $\textit{Low}$, even though it produces a higher-level result if called. This is sound: when a caller passes a $\textit{Low}$-level argument and gets a $\textit{Med}$-level result, if they use the result we can simply mark their further calculations as having level at least $\textit{Med}$, and so the final type will indicate that sensitive data has been accessed.

\subsubsection{Functions}

Let $f = \lambda.\ \lambda.\ L(V\ \textit{1}\ |\ \textit{Med}) + V\ \textit{0}$ be a function that upgrades the security of its' second argument, then adds that to its' first argument. We want to show that

\begin{gather*}
\Gamma \vdash_s f:\ ((\exNat\ |\ \textit{High}) \Rightarrow ((\exNat\ |\ \textit{Low}) \Rightarrow (\exNat\ |\ \textit{High})\ |\ \textit{Low})\ |\ \textit{Med})
\end{gather*}

Intuitively, $f$ takes in a high-level, sensitive $\exNat$ with security level $\textit{High}$ and a low-level, insensitive $\exNat$ with level $\textit{Low}$, upgrades the latter to level $\textit{Med}$, then adds them together. 

If we want to be conservative when estimating the security level of the $+$ term, we should have the sensitive $\textit{High}$ argument `taint' the result: adding a $\textit{Med}$ term and a $\textit{High}$ should result in a $\textit{High}$ term, which indeed appears in the final result type.

If the reader is familiar with function types in other languages, they will see the familiar function currying pattern: $f$ is a function that takes in a term of type $\exNat$ and security level $\textit{High}$, and returns another function taking a $\exNat$ of level $\textit{Low}$ which finally returns a $\exNat$ of level $\textit{High}$. 

However, all these different function terms have their own, separate, security levels. The outermost function $f$ has level $\textit{Med}$, whereas the intermediate function has level $\textit{Low}$. These unfortunately clutter our typing terms, however they are critical for the soundness of our eventual security result.

\subsubsection{Application}

Say we have terms $f$ and $x$, with $\Gamma \vdash_s f:\ ((\exNat\ |\ \textit{Low}) \Rightarrow (\exNat\ |\ \textit{Low})\ |\ \textit{High})$ and $\Gamma \vdash_s x:\ (\exNat\ |\ \textit{Low})$. That is, both terms are low-level, and $x$ is a valid argument to $f$. What should the type of $f \circ x$ be? The naive answer, that $f \circ x$ should have the same type $(\exNat\ |\ \textit{Low})$ as the codomain of $f$, is unsound. 

Say we had another function $f^\prime $ with the same type. Consider $f^\prime \circ x$. Say Mary is an observer at level $\textit{Med}$. Mary would be allowed to observe the argument $x$ to both functions, but would not be allowed to observe $f$ and $f^\prime$ since the functions have a higher security level than she does.

However, if the types of $f \circ x$ and $f^\prime \circ x$ are both $(\exNat\ |\ \textit{Low})$, then Mary can observe the \textit{results} of the functions - perhaps one is the identity function, but the other always returns 4. This allows her to learn about $f$ and $f^\prime$, which is insecure! 

We can solve this issue by forcing the result of a function to have at least the same level as the function itself. In this case, the result would have $(\exNat\ |\ \textit{High})$, and would be hidden from Mary's attempts to inspect them.

\subsection{Judgement Rules}
\label{chap:infoflow_slc:typing}

The judgement rules are provided in \autoref{fig:infoflow_slc:typing_rules}. The intuition behind the rules is as follows:

\begin{itemize}
	\item {\sc lambdaTypeNat} and {\sc lambdaTypeApp} indicate that \textit{values} are by default assumed to have the lowest security level.
	\item In addition, {\sc lambdaTypeApp} follows the usual construction for the simply-typed lambda calculus. We type the body of the function $f$ under the added assumption that variable $V\ \textit{0}$ has type $t_1$. If we can subsequently judge that $f$ has type $t_2$, then $\lambda.\ f$ is of `value' type $t_1 \Rightarrow t_2$.
	\item {\sc lambdaTypeVar} is how $\Gamma$ is used to track the types of variables `in scope' when typing terms. The expression $xs `! n$ is the partial indexing function on lists: $xs\ `!\ n = Some\ x$ if $x$ is the $n$th element of $xs$, and $xs\ `!\ n = None$ otherwise. Thus, {\sc lambdaTypeVar} says $V\ n$ has the type of the $n$th type in $\Gamma$.
	\item {\sc lambdaTypePlus} forces the sub-terms of a $+$-term to have the same security level as the resulting term.
	\item Similarly, {\sc lambdaTypeApp} forces the result of applying an argument to a function to have both the same security level as the function itself, as well as that of the return type of the function.
	\item {\sc lambdaTypeLabel} allows us to \textit{forcibly upgrade} the security level of a term, preserving its value type.
\end{itemize}

\subsection{Subtyping}
\label{chap:infoflow_slc:subtyping}

Notice that the rules {\sc lambdaTypePlus} and {\sc lambdaTypeApp} force the security levels of some terms to be equal. We could have relaxed these constraints by instead using a rule like 

\begin{center}
	\exSlcPlusAlternative
\end{center}

However, not only would this needlessly complicate our rules in the simple cases, it wouldn't suffice for {\sc lambdaTypeApp} which requires a recursive definition: if we have $\Gamma \vdash_s f:\ (t_1 \Rightarrow t_2\ |\ l)$ we need to allow a similar form of flexibility both for the label $l$ as well as the arbitrary type $t_2$.

A common solution to this is to use \textit{subtyping}: we allow some type $t_1$ to be transformed or \textit{cast} into another type  $t_2$ when $t_1$ is a \textit{subtype} of $t_2$. An appropriate choice of this transitive, reflexive subtyping relation $\leq_s$ will capture this `security strengthening' definition that is hinted at by {\sc lambdaTypePlusAlternative}. 

The rules for subtyping used in the rule {\sc lambdaTypeSub} are given in 	\autoref{fig:infoflow_slc:subtyping_rules}. There are two cases:

\begin{itemize}
	\item {\sc lambdaSubLabel} handles when we wish to \textit{upgrade} a term to a higher security level. If we believe that $e$ will be indistinguishable at some level $l$, and is currently judged to be level $l^\prime$ with $l^\prime \leq l$, then we can soundly judge $e$ to be of level $l$.
	\item {\sc lambdaSubAbs} is an instance of a common pattern with function types, that of \textit{variance}. Consider a term $f$ of type $(t_1 \Rightarrow t_2\ |\ l)$:
	\begin{itemize}
		\item Once we call $f$ with an argument of type $t_1$, we get a result of type $t_2$. By the same reasoning as before, we can soundly increase the level of this resulting value, so if $t_2 \leq_s t_2^\prime$ we can soundly cast $f$ to the type $(t_1 \Rightarrow t_2^\prime\ |\ l)$. This is \textit{covariance}: subtyping in the codomain of a function type gets lifted into subtyping of the entire function type.
		\item We can soundly \textit{downgrade} the type of the domain: if $f$ produces a secure term with an argument of type $t_1$, then it can also produce it with a lower level argument with type $t_1^\prime \leq_s t_1$. This is \textit{contravariance}: \textit{supertyping} in the domain is lifted into subtyping of the entire function type. This doesn't have as convenient an explanation as the previous case, but we will prove that it is sound.
	\end{itemize}
\end{itemize}

\subsection{Type Property Results}

We have a mechanised proof\footnotemark of type preservation under suitable substitution of variables:

\footnotetext{Lemma {\sf subst\_preserves\_se\_typ} in theory file {\sf IFSTM}.}

\begin{center}
	\exSlcSubstPreservesType
\end{center}

This is reminiscent of `proof cut' theorems in proof theory. Here, $\textit{insert} \isacharunderscore \textit{at}\ xs\ n\ x$ is the result of inserting $x$ into the list $xs$ at index $n$. Hence, $\Gamma$ is a typing context including a type for the variable $V\ k$ and $\Gamma^\prime$ is $\Gamma$ with this type removed. The theorem then shows we can `cut' the typing assumption by substitutiong $V\ k$ with a term of the appropriate type.

The proof is a straightforward induction over the typing judgement that relies on the arithmetic in the definitions of substitution and lifting (as well as some properties about list insertion), and acts as an external check on the validity of those definitons.

Additionally, we prove\footnotemark that types are preserved under the evaluation relation $\slcstep$:

\footnotetext{Lemma {\sf cbvss\_preserves\_se\_typ} in theory file {\sf IFSTM}.}

\begin{center}
	\exSlcStepPreservesType
\end{center}

Although one might expect this to be proved by induction over the typing judgement, we will discuss in \autoref{chap:infoflow_slc:how_to_do_type_props} the issues encountered and techniques used to turn this into a straightforward induction over $\slcstep$ instead.

These two lemmas are standard requirements for a well-behaved type system over an evaluation relation, and are critical for the proof of the main result of this chapter.

\section{Information Flow}
\label{chap:infoflow_slc:infoflow}

In \autoref{ssec:information_flow:lambda_calculus} we discussed a formalisation of IFS for lambda calculi. However, most definitions are closer to those discussed in \autoref{sec:information_flow:smm} where we judge whether two program states can be distinguished by an observer at level $l$ and show that this distinguishability is preserved by execution.

\subsection{Indistinguishability}

There are many possible choices of an observational equivalence relation $\sim$ on terms. There is a continuum, ranging from the most restrictive case where $x \sim y$ iff $x = y$, to the most permissive case where $x \sim y$ for all $x$ and $y$. If we make our relation too refined, we risk preventing many terms from being related in the first place; if we make it too coarse, we won't meaningfully constrain program behaviour.

We define a combined typing and distinguishability equivalence relation $\Gamma \vdash_{se} a =(l) b:\ t$, which asserts that the terms $a$ and $b$ are indistinguishable to an observer at security level $l$. We include the type of the terms to assist with induction, by allowing us to capture the judgements $\Gamma \vdash_s a:\ t$ and $\Gamma \vdash_s b:\ t$.

The rules for this judgement are given in \autoref{fig:infoflow_slc:equivalence_rules}. Most of the rules consist of composing equivalence over sub-terms, as well as lifting typing judgements on equal natural number literals and variables. The one exception is the rule {\sc lambdaEqHide} which captures the censorious nature of our security property, in that it asserts all terms which are at a higher security level than the observer are indistinguishable to them.

\subsection{Non-Interference}

We are at last in a position to prove\footnotemark the major result of this chapter, a non-interference property that observational equivalence is preserved under execution:

\footnotetext{Lemma {\sf cbvss\_noninterference} in theory file {\sf IFSTM}.}

\begin{center}
	\exSlcNoninterference
\end{center}

One way to intuit the content of this result is to rephrase it as follows. Say Alice is an agent at security level $l$. We give her two terms, $a$ and $b$, of level $l^\prime$. Alice is allowed to execute the two terms indefinitely. If we gave her two highly-secure terms with $l^\prime > l$, then at no point will they suddenly become insecure, distinguishable terms. If the terms $a$ and $b$ \textit{contain} highly-secure sub-terms, then either the outer terms are distinguishable already or the sub-terms once again never cause the terms to suddenly become distinguishable to Alice (that is, they don't \textit{interfere} with observability).

\subsection{Implicit Information Flow}
\label{chap:infoflow_slc:implicit}

Interestingly, the way we formulate our original typing judgements on functions allows us to recreate an analog to the `program counter' concept described in \autoref{sec:information_flow:implicit}, designed to avoid \textit{implicit information flow}.

First, consider the Church encoding of booleans in the lambda calculus \cite{Pierce2002}. In this encoding we construct booleans as their own case-matching definitions, so that if $b$ is a boolean we have $\texttt{if}\ b\ \texttt{then}\ t\ \texttt{else}\ f \equiv b \circ t \circ f$. In our notation, we thus have $True = \lambda.\ \lambda.\ V\ \textit{1}$ since it returns the first, outer argument, and $False = \lambda.\ \lambda.\ V\ \textit{0}$ since it returns the second, inner one.

Second, consider the canonical example of implicit information flow: the above expression where $b$ is of some high security level $H$ and $t$ and $f$ are of some lower level $L < H$. We want the result of evaluating the term to be of level $H$ even though a naive typing rule would have the expression type be equal to that of the two $if$-branches.

For simplicity, assume $t$ and $f$ have type $(\exNat\ |\ L)$. Then the boolean $b$ is of (the unfortunately very verbose) type \exLambdaBoolType with outermost security label $H$. Notice that, in order to actually use $b$, we need to use subtyping to `upgrade' the inner $L$s to $H$s before we can use rule {\sc lambdaTypeApp}! Doing this for each argument $t$ and $f$, we deduce that $b \circ t \circ f$ is of type $(\exNat\ |\ H)$, as required.

\subsection{Lock-Step Execution}

Our stated non-interference property only considers cases where, when we are comparing terms, they are each evaluated one step at a time, and they are both evaluated at the same time. It is natural to consider the more general case where, with equivalence relation $\sim$ and execution relation $\rightarrow$, whether $a \sim b$ is preserved under the reflexive transitive closure $\rightarrow^\star$ (see \autoref{fig:app1:rtc}).

Unfortunately, this requires terms to be self-similar under evaluation: since $a \sim a$, if $a \rightarrow a^\prime$ then we need $a \sim a^\prime$. The result is a very coarse equivalence relation with little structure. However, at least one existing formalism manages to work with such a definition \cite{stefan_flexible_2011}.

\section{Encountered Obstacles}

\subsection{Proof Complexity}

Despite the minimalistic grammar and evaluation semantics of our language, the proof of {\sc lambdaNoninterference} becomes nontrivial in several places for entirely mundane reasons. In particular, whenever a term has both `structural' and `semantic' evaluation rules, we are forced to consider an excessive number of cases.

For example, consider showing preservation for the judgement $\Gamma \vdash_{se} f \circ x =(l) f^\prime \circ x^\prime:\ t$ inside of a greater inductive proof. We have the assumptions that both the left hand term $f \circ x$ and right hand term $f^\prime \circ x^\prime$ make progress under $\slcstep$. 

Through extensive case elimination, we can show that each side must either make progress `in lockstep' (that is, they both evaluate their left-hand sub-terms $f$ and $f^\prime$, or their right-hand ones, or they both apply {\sc lambdaStepAppAbs}), or both sides are indistinguishable under {\sc lambdaEqHide}. Unfortunately this process is sufficiently verbose that we feel the need to separate it out into a separate lemma. A similar outcome holds for terms like $a + b$. 

This high cost of each distinct term is the primary reason why our language is so feature-sparse; we will see potential methods to avoid this incidental complexity in \autoref{chap:infoflow_sstml}.

\subsection{Case Rules}
\label{chap:infoflow_slc:how_to_do_type_props}

When we define an inductive relation like the typing judgements in \autoref{fig:infoflow_slc:typing_rules}, Isabelle can produce what are called \textit{inductive case elimination rules}. These are used to \textit{destructure} instances of the inductive relation in the assumptions of a lemma, replacing them with the assumptions of the inductive predicate.

For example, if we were trying to prove the following lemma\footnotemark for some particular $P$:

\footnotetext{These depictions of proof state are styled somewhat differently than they appear in Isabelle, for readability.}

\begin{gather*}
	\Gamma \vdash_s f \circ x:\ (t\ |\ l) \Longrightarrow P
\end{gather*}

Then we can use the inductive case elimination rule on the assumption on the left hand side. This produces two new \textit{subgoals} which, if both proven, will in turn prove the above original goal:

\begin{gather*}
	\forall s.\ \Gamma \vdash_s f:\ (s \Rightarrow (t\ |\ l)\ |\ l)\ \wedge\ \Gamma \vdash_s x:\ s \Longrightarrow P \\
	\forall s.\ s \leq_s (t\ |\ l)\ \wedge\ \Gamma \vdash_s f \circ x:\ s \Longrightarrow P
\end{gather*}

It's easy to see how this method should be sound: there are two ways to construct the original typing judgement $\Gamma \vdash_s f \circ x:\ (t\ |\ l)$ (either directly by {\sc lambdaTypeApp}, or using subtyping by {\sc lambdaTypeSub}), so we just consider each of those cases. 

The first case looks very useful: we know the structure of the type of $f$, and we have a typing judgement for $x$. These could be applied to an inductive hypothesis or some auxillary lemma to make forward progress. Unfortunately, the second case is far less useful. Although the subtyping relation might have been helpful, the only other assumption we've gained is of the exact same form as the one we were trying to eliminate!

It is possible to instead `roll in' the subtyping, producing a new case elimination rule which produces the following, single subgoal:

\begin{align*}
\forall s\ t^\prime\ l^\prime\ l_f.
		&\phantom{\wedge}\ \Gamma \vdash_s f:\ (s \Rightarrow (t^\prime\ |\ l^\prime)\ |\ l_f) \\
		&\wedge\ \Gamma \vdash_s x: s \\
		&\wedge\ (t^\prime\ |\ l^\prime) \leq_s (t\ |\ l) \\
		&\wedge\ (t^\prime\ |\ l_f) \leq_s (t\ |\ l) \\
		&\Longrightarrow P
\end{align*}

This new elimination rule allows us to ergonomically perform inductions over other relations while still being able to productively use any typing judgements we may have held. We use this to good effect in order to prove {\sc lambdaStepPreservesType} while inducting over the simpler $\slcstep$ relation.

We can easily prove the soundness of this new case rule by induction over the typing judgement. The difficulty arises when figuring out what precisely the new subgoal should be. It \textit{looks} like the result of taking the `terminal' typing judgement, in this case {\sc lambdaTypeApp}, and `weakening' the assumptions of that judgement using the subtyping relation. This process might be automatable; if so, it would greatly improve our ability to prove properties about typing judgements. Unfortunately, this is out of scope of the project.

\section{Conclusion}

We outlined the issues that one encounters when representing functional program terms, and explored a solution in the form of De Bruijn indices. Using this representation we have defined a simple lambda calculus equipped with a way to annotate terms with security labels, along with a deterministic evaluation relation. 

We defined an inductive typing judgement over this language, as well as a term indistinguishability equivalence relation. We relate the concept of non-interference as an information flow security policy with respect to this relation. We have presented a proof in Isabelle of preservation of this relation under evaluation, discussing some limitations of our results and obstacles encountered during the proofs thereof.

%% FIGURES

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{align*}
		x & ::\ Variable \\
		e & ::\ Expr \equiv \\
		  &\ \phantom{|}\ \lambda x.\ e \\
		  &\ |\ e_1 \circ e_2 \\
		  &\ |\ x
		\end{align*}
	\end{minipage}	
	\caption[Grammar of the Classical Lambda Calculus]{Grammar of the classical minimalistic lambda calculus. Note the use of $\circ$ for application.}
	\label{fig:infoflow_slc:lambda_calculus_grammar}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{align*}
		l 	& ::\ SecLevel \\
		vt 	& ::\ SecLevel\ ValueType \equiv\\
			&\ \phantom{|}\ Nat \\
			&\ |\ st \Rightarrow st \\
		st	& ::\ SecLevel\ SecurityType \equiv\\
			&\ \phantom{|}\ (vt\ |\ l)
		\end{align*}
	\end{minipage}	
	\caption[Grammar of Security Types]{Mutually-recursive definition of value and security-labelled types in our secure lambda calculus.}
	\label{fig:infoflow_slc:sec_typ_grammar}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\inferrule{l \rightarrow_\beta l^\prime}{l \circ r \rightarrow_\beta l^\prime \circ r} {\textsc{appLeft}}\\
		\inferrule{r \rightarrow_\beta r^\prime}{l \circ r \rightarrow_\beta l \circ r^\prime} {\textsc{appLeft}}\\
		\inferrule{f \rightarrow_\beta f^\prime}{\lambda x.\ f \rightarrow_\beta \lambda x.\ f^\prime} {\textsc{abs}}\\
		\inferrule{ }{(\lambda x.\ a) \circ b \rightarrow_\beta a[x \mapsto b]} {\textsc{appAbs}}
		\end{gather*}
	\end{minipage}
	\caption[Beta-Reduction Rules]{The inductive relation for beta-reduction of lambda calculus terms.}
	\label{fig:infoflow_slc:lambda_calculus_beta}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{1\textwidth}
	\begin{gather*}
	\exSlcTypingRules
	\end{gather*}
\end{minipage}	
\caption[Typing Rules for a Simple Secure Lambda Calculus]{Typing judgement inference rules for our simple security-labelled lambda calculus.}
\label{fig:infoflow_slc:typing_rules}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exSlcSubTypingRules
		\end{gather*}
	\end{minipage}	
	\caption[Subtyping Rules for a Simple Secure Lambda Calculus]{Subtyping judgement inference rules for our simple security-labelled lambda calculus.}
	\label{fig:infoflow_slc:subtyping_rules}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{1\textwidth}
	\begin{gather*}
	\exSlcEquivalenceRules
	\end{gather*}
\end{minipage}	
\caption[Equivalence Rules for a Simple Secure Lambda Calculus]{Observational equivalence inference rules for our simple security-labelled lambda calculus.}
\label{fig:infoflow_slc:equivalence_rules}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{1\textwidth}
	\exSlcTerms
\end{minipage}	
\caption[Grammar of a Simple Secure Lambda Calculus]{Grammar of terms in a simple lambda calculus with security labels, natural number literals, and addition.}
\label{fig:infoflow_slc:slc_grammar}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{1\textwidth}
\begin{gather*}
\exSlcEval
\end{gather*}
\end{minipage}	
\caption[Evaluation Rules of a Simple Secure Lambda Calculus]{Derivation rules for the deterministic evaluation relation on terms in our security-labelled lambda calculus.}
\label{fig:infoflow_slc:eval_rules}
\end{figure}