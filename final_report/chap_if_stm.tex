\newcommand{\stmTVar}{\textit{TVar}}
\newcommand{\stmType}{\textit{Type}}
\newcommand{\stmSecLabel}{\textit{SecLevel}}
\newcommand{\stmExpr}{\textit{Expr}}
\newcommand{\stmList}{\textit{List}}
\newcommand{\threadsStep}{\rightsquigarrow_{SCH}}
\newcommand{\simpleEval}{\rightsquigarrow}
\newcommand{\simpleStep}{\rightarrow}
\newcommand{\lioStep}{\rightarrow_{LIO}}
\newcommand{\stmStep}{\rightarrow_{STM}}
\newcommand{\stmIntOp}{\textit{IntOp}}
\newcommand{\stmIntComp}{\textit{IntComp}}
\newcommand{\stmApp}{\textit{App}}
\newcommand{\stmStmBind}{\textit{StmBind}}
\newcommand{\stmLeaf}{\textit{Leaf}}
\newcommand{\stmNode}{\textit{Node}}
\newcommand{\stmNewTVar}{\textit{newTVar}}
\newcommand{\stmAddTVarMem}{\textit{addTVarMem}}
\newcommand{\stmReadTVar}{\textit{readTVar}}
\newcommand{\stmGetTVarMem}{\textit{getTVarMem}}
\newcommand{\stmWriteTVar}{\textit{writeTVar}}
\newcommand{\stmSetTVarMem}{\textit{setTVarMem}}
\newcommand{\stmStmReturnTree}{\textit{return}_\textit{STM}}
\newcommand{\stmStmBindTree}{\textit{bind}_\textit{STM}}
\newcommand{\stmRetry}{\textit{retry}}
\newcommand{\stmOrElse}{\textit{orElse}}
\newcommand{\stmZ}{\textit{stm}_0}
\newcommand{\stmIntOpTree}{\textit{intOp}}
\newcommand{\lioAtomically}{\textit{atomically}}
\newcommand{\stmLioReturnTree}{\textit{return}_\textit{LIO}}
\newcommand{\stmLioBindTree}{\textit{bind}_\textit{LIO}}
\newcommand{\stmStmType}[3]{\textit{Stm}\ (R:\ #1)\ (W:\ #2)\ #3}
\newcommand{\stmLioType}[2]{\textit{LIO}\ #1\ #2}


\chapter{A Simple STM Language}
\label{chap:infoflow_sstml}

In \autoref{chap:infoflow_slc}, we looked at information flow in a simple lambda calculus. Here, we will explore it in a much more expressive and interesting context: a concurrent language with shared software transactional memory.

\section{State Representation}

Most real computing environments have much more complicated states and state transitions than the lambda calculus. We won't be able to get away with just specifying a grammar of terms; we also have to describe an \textit{abstract machine} on which we evaluate those terms, hopefully having some effect.

\subsubsection{Threads and Concurrency}

We want to explore concurrency, and so the first step is to equip our machine with a thread pool. We model our pool as a list $T$ of expressions. Each expression is the currently evaluating term of a thread; if there are $n$ expressions in $T$ then there are $n$ threads running.

Once we have several threads, we need a way to model scheduling. We assume there is some external process (perhaps an operating system, perhaps a green threads scheduler) which dictates which of our threads is executing. For simplicity and flexibility, we say the scheduler has some datatype $\textit{Sched}$ that it uses to store state between choosing threads, and that it comes equipped with a function $\textit{sched} \isacharunderscore \textit{next}$ which takes the scheduler's current state $\textit{sch}$ and current thread pool $T$, and returns the index of the next thread to run as well as the new scheduler state.

Notice that we implicitly assume there is no true parallelism here: the scheduler chooses which thread runs next, and then only that thread makes progress. Alternatively, we can assume that there \textit{is} true parallelism, that the scheduler is choosing threads to run on, say, a collection of CPUs. In this case we are then assuming \textit{interleaving semantics}, which is a close relative of serializability as discussed in \autoref{sec:concurrency:semantics}.

\subsubsection{Shared Memory}
\label{chap:infoflow_sstml:heap}

If each of these expressions were running independently in their own thread and incapable of interaction with the others, there might still be interesting questions to ask such as how a scheduler might leak information to a thread (perhaps a thread can read the time?). However, we are more interested in enforcing security when threads interact while performing computations.

Thread interaction can be modelled, amongst other techniques, with synchronisation and message passing, or shared state. Since message passing can be modelled with shared memory \cite{harris_composable_2005}, and since STM explicitly requires a shared pool of transaction variables, we limit our focus to it.

For security reasons, when a thread creates a new transactional variable they must specify the security level $l$ of the variable. For simplicity of our definitions and to help ensure a consistent program state, we also require the thread to specify the type $t$ of the value held in the variable.

This suggests that we represent the shared heap $S$ as a mapping from pairs $(l, t)$ of security levels and types to lists of expressions. Then, we can define a transactional variable $t = \stmTVar\ n\ l\ t$ to be a variable referring to the $n$th expression of the list $S (l, t)$. We will see that this definition has additional benefits later on when discussing how to define a suitable security property on our system.

\subsubsection{Execution}
\label{chap:infoflow_sstml:execution}

With this infrastructure in place, we can now formalise a `top-level' execution relation $\threadsStep$ over thread pools $T$, shared memories $S$, and scheduler states $\textit{sch}$. The relation is defined by two rules. The first:

\begin{gather*}
\exStmThreadStep
\end{gather*}

Here, $xs[n := x]$ is $xs$ with the $n$th element replaced with $x$. We effectively delegate all `state changes' to the relation $\lioStep$, which will encode the semantics of our language proper. The second:

\begin{gather*}
\exStmThreadReturn
\end{gather*}

This rule is mostly book-keeping. It handles threads that are `stuck', and cannot make forward progress; we simply skip evaluating the thread, progressing the scheduler to its' next state. The expression $\stmLioReturnTree$ and the predicate $\textit{val}$ will be defined later. For now it suffices to consider them markers of completed computation.

\section{Term Representation}

Our reasons for using De Bruijn indices in our simple labelled lambda calculus (SLLC) are still valid here, so we use the technique again. However, there is one design choice we made in SLLC that would be quite costly here.

Recall from \autoref{fig:infoflow_slc:slc_grammar} that we had a top-level $+$-term in SLLC. We might consider having such a top-level term for each language term of interest - making transaction variables, reading and writing to them, or perhaps some more mundane features to increase the expressivity of our language, such as lists or if-expressions.

However, note the definitions of substitution and $\textit{lift}$ for SLLC in \autoref{chap:infoflow_slc:db_subst}, and their extensions in \autoref{chap:infoflow_slc:data_and_labels}. For each top-level term we would need to add another case to our definitions; this might sound trivial, but when iterating a language design it becomes an onerous chore quite quickly.

Similarly, recall the evaluation relation for SLLC in \autoref{fig:infoflow_slc:eval_rules}. Notice that there are separate {\sc left} and {\sc right} rules for and $+$, and a {\sc left} rule for $\circ$, yet both of these rules amount to the description `evaluate sub-terms from left to right'. Without finding a way to separate out this sort of definition, our evaluation rules would rapidly become cluttered with such `boilerplate'.

\subsection{Term Trees and Atoms}

One alternative representation is to have a generic, structural \textit{tree} of terms, with each node of the tree annotated with an \textit{atom} indicating what term the node is intended to represent. This grammar is given in 	\autoref{fig:infoflow_stm:tree_grammar}.

There are many program behaviours that we want to capture in our terms, and so there are many atoms used to mark nodes. A list of atoms is given in  	\autoref{fig:infoflow_stm:atom_grammar}. Some atoms such as $\stmIntOp$ and $\stmIntComp$ carry extra information, while others such as $\stmApp$ and $\stmStmBind$ are `bare', acting solely as markers.

We will discuss the semantics of these atom markers in \autoref{chap:infoflow_sstml:dynamic_semantics}. For now, we have already gained a more concise definition of substitution and $\textit{lift}$: 

\begin{center}
	\exStmSubstDef
	\exStmLiftDef
\end{center}

The cases for variables $V\ n$ and abstractions $\lambda.\ e$ are as before in SLLC, but now all the rest of our terms are handled in the $\stmLeaf$ and $\stmNode$ cases.

\subsection{Abbreviations}

Before we define evaluation semantics over these term trees, we define some abbreviations to avoid writing grotesque terms like $\stmNode\ (\textit{IntLit}\ 5)\ \stmLeaf\ \stmLeaf$ in our rules. Depending on the arity of an atom - that is, the number of sub-terms it expects - our abbreviations construct a $\stmNode$ with the appropriate sub-terms as branches. The abbreviations are listed in \autoref{fig:infoflow_stm:abbreviations}.

\section{Dynamic Semantics}
\label{chap:infoflow_sstml:dynamic_semantics}

Atoms starting with $\textit{Stm}$ describe terms used to construct software transactions. Those starting with $\textit{Lio}$ describe top-level labelled IO actions that deal with observable side-effects. The rest describe `pure computation' i.e. terms that have no side-effects in their evaluation; we begin with these.

\subsection{Pure Computation}

The rules in \autoref{fig:infoflow_stm:simple_eval} lay out the intuition behind evaluating pure terms. For the most part they are self-explanatory: $\circ$ reprises its' role as function application, $\textit{if}$ evaluates to one sub term or the other depending on the boolean literal being checked, $\textit{intOp}$ and $\textit{intComp}$ unwrap the function they're carrying along with their integer literal sub-terms, then wrap the result in another literal.

However, we still need to specify rules on how sub-terms evaluate. For instance, in $(\lambda. f) \circ a \circ b$ we clearly need to evaluate the inner application before the outer one. We \textit{could} add such bureaucratic rules to those in \autoref{fig:infoflow_stm:simple_eval}, or we could use our new tree description to delegate it all at once to the rules in 	\autoref{fig:infoflow_stm:simple_step}.

Our `boring' evaluation rules refer to a predicate $\textit{val}$ in order to determine if an expression is finished evaluating. In particular, we have that if $\textit{val}\ x$, then there is no $y$ with $x \rightarrow y$. The definition of $\textit{val}$ is rather straightforward, and can be found in \autoref{fig:infoflow_stm:val_def}.

\subsection{STM Transactions}

Our pure computation rules, having by definition no effect on global state, ignored the shared STM heap. All these STM terms are no use to us if we can't turn them into a transaction on memory; hence, we define the rules in \autoref{fig:infoflow_stm:stm_step} for a relation $\stmStep$  which relate pairs $(S, e)$ of shared heaps $S$ and terms $e$. Pairs are related by how terms evaluate, and how those evaluations change the heap.

The rule {\sc stmSimple} carries over pure computations our transaction evaluations, which lets us focus on fully-evaluated STM terms.  The intuitive semantics of STM terms are as follows:

\begin{itemize}
	\item When evaluated, $\stmNewTVar\ l\ t\ v$ creates a new transaction variable with security level $l$, value type $t$, and initial value $v$. The function $\stmAddTVarMem$ takes the original heap and the value to add, and returns the new heap as well as a $\stmTVar$ term referring to the newly created slot in the heap; this new term is wrapped in an administrative term $\stmStmReturnTree$ used to track that the result is within an STM transaction.
	\item Recalling the definiton of our heap from \autoref{chap:infoflow_sstml:heap}, the term $\stmReadTVar\ (\stmTVar\ n\ l\ t)$ reads the $n$th entry of the list of transaction variables with level $l$ and type $t$ using the function $\stmGetTVarMem$. Again, we wrap the result in $\stmStmReturnTree$.
	\item $\stmWriteTVar\ (\stmTVar\ n\ l\ t)\ v$ sets the value of the relevant transaction variable in the heap to $v$. The function $\stmSetTVarMem$ returns the updated memory. This time there is no result to wrap, however we still need to track that a transaction has occured. We instead use a dummy term $\textit{unit}$, again wrapped in $\stmStmReturnTree$.
	\item $\stmStmBindTree\ m\ f$ is the composition of an STM transaction $m$ with a continuation $f$. We require $f$ to also return a transaction.
	\begin{itemize}
		\item Rule {\sc stmBindStep} forces the inner transaction $m$ to evaluate fully, into either an $\stmStmReturnTree$ or $\stmRetry$.
		\item Rule {\sc stmBindReturn} handles when $m$ is `successfully complete' and has evaluated to $\stmStmReturnTree\ v$: it unwraps the result and passes $v$ onto the continuation $f$.
		\item Rule {\sc stmBindRetry} propagates failures. The term $\stmRetry$ allows the writer of a transaction program to indicate that the transaction has \textit{failed}, and so here the failure is propagated by skipping the continuation $f$ and passing on the $\stmRetry$.
	\end{itemize}
	\item $\stmOrElse\ l\ r$ composes transactions over failure. While $\stmStmBindTree$ unconditionally propagates failure, $\stmOrElse$ inspects the results of the transaction $l$. To \textit{inspect} a transaction result, we need to look at the reflexive transitive closure (see \autoref{fig:app1:rtc}) of $\stmStep$.
	\begin{itemize}
		\item Rule {\sc stmOrElseReturn} handles when the first transaction $l$ successfully evaluates to a wrapped $\stmStmReturnTree$ value. In this case there is no need to attempt $r$, and we propagate the successful return.
		\item Rule {\sc stmOrElseRetry} handles failure, when $l$ evaluates to $\stmRetry$. In this case, we simply start evaluating the second transaction $r$. Importantly, we start evaluating $r$ with the \textit{original} heap state of $l$, not the \textit{final} one: this is what makes transactions compose usefully.
	\end{itemize} 
\end{itemize}

The definitions of the utility functions $\stmAddTVarMem$, $\stmGetTVarMem$, and $\stmSetTVarMem$ are listed in 

Since the evaluation of terms in $\stmStep$ is more complicated than in $\simpleStep$, it is much harder to factor out `boring' rules like {\sc stmBindStep}. or to remove the many $\textit{val}$ predicates. These are used to ensure that STM terms are fully evaluated under $\simpleStep$ before applying any of the other rules, which in turn enforces determinism.

\subsubsection{A Bind Example}
\label{chap:infoflow_sstml:bind_example}

Let $\stmZ$ be an empty initial heap, which returns the empty list for all levels $l$ and types $t$. Then using function updating (see \autoref{fig:app1:fun_upd}) we can define a heap $\textit{stm}_1 = \stmZ((bot, Nat) := [I\ \textit{5}, I\ \textit{4}])$ containing the terms referred to by two transactional variables. 

We have that $v_0 = \stmTVar\ \textit{0}\ bot\ Nat$ is the term referring to the first element, an integer with value 5, and $v_1 = \stmTVar\ \textit{1}\ bot\ Nat$ refers to the second one with value 4.

Below is an example program $\textit{addWrite}$ that reads the value of $v_0$ and $v_1$, then writes their sum into $v_0$:

\begin{align*}
	\textit{addWrite} & =                  \stmStmBindTree\ (\stmReadTVar\ v_0)\ (\lambda. \\
					  & \phantomsection{=} \stmStmBindTree\ (\stmReadTVar\ v_1)\ (\lambda. \\
					  & \phantomsection{=} \stmWriteTVar\ v_0\ (\stmIntOpTree\ (+)\ (V\ \textit{0})\ (V\ \textit{1}))))
\end{align*}

Using our definition of $\stmStep$, we can show\footnotemark that this program, running on the initial heap $\textit{stm}_1$, will result in the expected heap $\textit{stm}_2 = \stmZ((bot, Nat) := [I\ \textit{9}, I\ \textit{4}])$:

\begin{gather*}
	(\textit{stm}_1, addWrite) \stmStep^\star (\textit{stm}_2, \stmStmReturnTree\ \textit{unit})
\end{gather*}

\footnotetext{Lemma {\sf stm\_example\_0} in theory file {\sf SimpleIO}.}

\subsubsection{A Retry Example}
\label{chap:infoflow_sstml:retry_example}

Consider the transaction terms $f = \stmStmBindTree\ (\stmWriteTVar\ v_0\ (I\ \textit{0})) (\lambda.\ \stmRetry)$ and $g = \stmWriteTVar\ v_1\ (I\ \textit{0})$. We see that $f$ writes 0 to $v_0$ and then aborts the transaction, while $g$ unconditionally successfully writes 0 to $v_1$. 

We expect that $\stmOrElse\ f\ g$ should ignore the effects of $f$ and only preserve the effects of $g$, that is, $v_0$ should remain unchanged and $v_1$ should be set to 0. Indeed, we can show\footnotemark that starting in $\textit{stm}_1$, the combined term evaluates to the expected heap $\textit{stm}_3 = \stmZ((bot, Nat) := [I\ \textit{5}, I\ \textit{0}])$:

\begin{gather*}
(\textit{stm}_1, \stmOrElse\ f\ g) \stmStep^\star (\textit{stm}_3, \stmStmReturnTree\ \textit{unit})
\end{gather*}

\footnotetext{Lemma {\sf stm\_example\_1} in theory file {\sf SimpleIO}.}

\subsection{IO and Side Effects}

We now have an operational evaluation semantics for pure computations and a way to describe how a transactional computation changes memory (including backtracking, via $\stmRetry$). However, real concurernt programs don't consist of a single transaction attempted once.

For example, a program might attempt a transaction in order to avoid race conditions during some important inter-thread communication such as transferring funds between bank accounts, then perform some device IO to inform a user of the result. It might do this in a loop, forever alternating between input and atomic transactions.

The lesson here is that we want to separate out our language terms and evaluation rules \textit{describing} of what a transaction does as per $\stmStep$, from our terms and rules \textit{performing} side-effectful termination, which we call $\lioStep$.

The attentive reader may recall that this is the relation described near the beginning of this chapter in \autoref{chap:infoflow_sstml:execution}: indeed, $\lioStep$ is the relation describing `real effects' that threads can perform. 

We describe a single term of interest, $\lioAtomically$, which performs an STM transaction atomically. Although it is out of scope for this project, this is the layer of reasoning where we would include things like thread forking and device IO.

The rules for the relation are given in \autoref{fig:infoflow_stm:lio_step}:

\begin{itemize}
	\item The rule {\sc lioSimple} once again lifts pure computations into side-effectful ones.
	\item The terms $\stmLioReturnTree$ and $\stmLioBindTree$ are exact analogues of their STM equivalents, except they mark a computation as performing side effects instead of being a transaction.
	\item Similarly the rules {\sc lioBindStep} and {\sc lioBindReturn} do the same job as before, evaluating side-effectful terms and forwarding the results to continuations that produce more side-effectful terms.
	\item The rule {\sc lioAtomicallyReturn} applies a successful transaction. Critically this is the only way to perform an STM transaction in the top-level relation $\threadsStep$, which is how we enforce atomicity.
	\item The rule {\sc lioAtomicallyRetry} abandons a failed transaction, leaving the program in the same state. If the relation $\threadsStep$ attempts to execute such a failing transaction, the scheduler will progress to its' next state, while the thread will remain blocked waiting until it can attempt the transaction again.
\end{itemize}

\subsubsection{Splitting Transactions}

At first glance the rules for $\lioStep$ in \autoref{fig:infoflow_stm:lio_step} are suspiciously similar to those of $\stmStep$ in \autoref{fig:infoflow_stm:stm_step}. However, having a separate layer allows us to genuinely split transactions.

Consider a thread $t_0$ executing our previous example transaction $\textit{addWrite}$ twice, in two separate transactions. That is, $t_0$ runs the term $\stmLioBindTree\ (\lioAtomically\ \textit{addWrite})\ (\lambda.\ \lioAtomically\ \textit{addWrite})$, where $\textit{addWrite}$ is defined in \autoref{chap:infoflow_sstml:bind_example}.

Consider a second thread $t_1$ executing our previous failing example transaction $\textit{failWrite = \stmOrElse\ f\ g}$, whre $f$ and $g$ are defined in \autoref{chap:infoflow_sstml:retry_example}.

Depending on our scheduler, $t_0$ could be run twice in a row, and so its' two atomic transactions go through with no interference from $t_1$. However our scheduler could \textit{also} choose to run $t_0$ until it completes its' first transaction, interleave $t_1$, then complete $t_0$'s second transaction. 

If the threads start with the shared heap $\textit{stm}_1$ defined previously, these will be clearly distinct traces. Hence, $\lioStep$ captures our intent of being able to describe programs with separate transactions as components.

\section{Static Semantics}

Currently, we haven't discussed how to enforce this layered atomic evaluation outlined in \autoref{chap:infoflow_sstml:dynamic_semantics}. For instance, it isn't obvious that the rules described so far will \textit{prevent} an STM transaction term from evaluating into an IO term which might violate the atomicity requirements of STM. Additionally, we haven't discussed the security properties of transactions or IO actions.

We address these concerns by defining typing judgements of the form $\Gamma \vdash_S e:\ t$. Here $\Gamma$ plays the same roll as in \autoref{chap:infoflow_slc} as a list of `in-scope' variables for determining function types, $e$ is a term, and $t$ is a type from the grammar defined in \autoref{fig:infoflow_stm:type_grammar}. The rules for pure terms are in \autoref{fig:infoflow_stm:simple_type}, 	for transaction terms in \autoref{fig:infoflow_stm:stm_type}, and side-effectful terms in \autoref{fig:infoflow_stm:lio_type}.

\subsection{Pure Computations}

The rules in \autoref{fig:infoflow_stm:simple_type} are used to type side-effect-free, non-transactional terms. Notice the absence of security labels in these definitions: any concerns about security have been `factored out' into the types $\textit{STM}$ and $\textit{LIO}$ which are discussed later. These rules are thus relatively straightforward, with little to discuss.

\subsection{Transactions}

The rules in \autoref{fig:infoflow_stm:stm_type} type transaction description terms.

The rule {\sc typeTVar} matches the security label and value type of data referred to by a transaction variable, and lifts it to the type.

The rest of these rules result in judgements of the form $\Gamma \vdash_S e:\ \stmStmType{r}{w}{t}$, with $t$ in turn a type. This indicates that $e$ is a transaction term which when evaluated atomically, will

\begin{itemize}
	\item Read from transaction variables of level at most $r$, and
	\item Write to transaction variables of level at least $w$, and
	\item Produce a value of type $t$ (or fail via $\textit{retry}$).
\end{itemize}

The rules {\sc typeReadTVar} and {\sc typeWriteTVar} capture this directly when performing the relevant operation. 

The rule {\sc typeNewTVar} might look a bit odd: surely when we create a new transactional variable of level $l$ with a starting value of $v$, we are performing a write of level $l$? 

However, we intend our $\textit{STM}$ types to capture externally observable writes to the shared transaction memory. Writing to a new variable will definitely change the \textit{heap}, but until the thread sends this new transactional variable to another thread there is no way for any other thread to see this new, written value.

The terms $\textit{retry}$ and $\stmStmReturnTree$ are typed as one might expect: the former as a transaction with no reads or writes that `returns' any type of value, and the latter as a similarly uneventful transaction that indeed returns the wrapped type.

The rules {\sc typeStmBind} and {\sc typeStmReturn} indicate that our $\textit{STM}$ type is `monadic in the last parameter'. The use of monadic types is a common trick in strongly-typed functional languages for the kind of `semantic separation' of terms that we wish to enforce between our transaction terms and our IO side-effect terms.

Since $\stmOrElse$ might evaluate either of its' two sub-transactions, it forces them to have the same type.

The most interesting term from a security standpoint is {\sc typeStmSub}, which is what allows us to weaken $\textit{STM}$ types to conform to the requirements of rules like {\sc typeStmBind}. In detail, {\sc typeStmSub} says:

\begin{itemize}
	\item If we have a transaction that reads from variables of level at most $rd$, and
	\item If we have a transaction that writes to variables of level at least $wr$, then
	\item We can soundly \textit{pretend} that the variable reads from higher levels of variables, because we are claiming the transaction is more of a security risk: it might read higher-security information that it shouldn't be privy to. Also,
	\item We can soundly \textit{pretend} that the variable writes to lower levels of variables, because we are claiming the transaction is more of a security risk: it might write write to a lower-security variable, leaking informaton to that lower level.
\end{itemize}

In short, a transaction is in a sense \textit{security sound} if it only reads variables that are lower than or equal to the level of the variables that it writes to. In this sense {\sc typeStmSub} allows us to narrow this range, or even invert it to make the transaction \textit{unsound}. Ensuring we don't execute unsound transactions is the responsibility of the outermost security type, $\textit{LIO}$.

\subsection{IO and Side-Effects}

The typing rules for side-effectful computations is given in \autoref{fig:infoflow_stm:lio_type}. These rules produce judgements of the form $\Gamma \vdash_S e:\ \stmLioType{l}{t}$, with $t$ in turn a type. We will require that the top-level term of a thread always be a term with a type of this form and with constant $l$, and we say that the security level $l$ is the level of the thread.

\begin{itemize}
	\item The rules {\sc typeLioBind} and {\sc typeLioReturn} show that $\textit{LIO}$ is also a monad, and thus has the separation property we want between transaction and IO terms.
	\item The rule {\sc typeLioSub} seems unsound: can a thread simply upgrade their level to perform something terribly insecure? However, our final goal is to show that a particular set of typed threads is secure: if we say that thread $t_A$ belongs to Alice and Alice has security level $l$, we will reject any initial thread state where $t_A$ has level greater than $l$. In this case, using {\sc typeLioSub} can only upgrade a thread to the level of the thread's owner.
	\item The rule {\sc typeLioAtomically} is what ensures that only sound transactions are run by a thread. By checking that the level of reads of the transaction are below the thread's level, and that the writes are above it, we prevent the thread from reading information above its' level and writing it to its own (or a lower) level.
\end{itemize}

\section{Results}

We have a proof of type preservation for the simple evaluation relation $\rightarrow$, and a proof of determinism for the transactional relation $\stmStep$.

\section{Future Work}

\subsection{Type Preservation}
We have work in-progress for defining what `type preservation' means for STM terms. This is non-trivial because some terms, such as $\stmReadTVar$, read from the shared heap. This necessitates a sort of `general transactional variable consistency' invariant, where every $\stmTVar$ in a term must refer to an expression of the appropriate type.

\subsection{Security Properties}

Although we have alluded to an intuitive sense of security above, where a transaction is barred from writing information `above' its' level to below, we haven't formalised this to the degree we have in \autoref{chap:infoflow_slc:infoflow}. This is largely due to lack of progress on intermediate results, such as type preservation and determinism.

We are however in a position to discuss techniques that avoid the issues discussed in \autoref{chap:infoflow_slc:infoflow}, and how we have structured our language in a way that lays the groundwork to use such techniques.

Recall that in \autoref{chap:infoflow_slc:infoflow} we defined our non-interference property with respect to an equivalence relation over terms, and that the flexibility of this relation greaty increased if not the complexity, then the workload of our proofs of this property. 

Prior work \cite{harris_composable_2005} in the field has used an alternative way of proving non-interference called \textit{erasure}. While a security equivalence relation $\sim_l$ attempts to relate all pairs of terms $a$ and $b$ such that an observer at level $l$ should be unable to distinguish them, the erasure technique defines a function $\epsilon_l$ on terms which \textit{removes} all parts of the term that should be unobservable to an agent at level $l$.

While previously we would show that indistinguishability is preserved by execution (that is, if $a \sim_l b$, $a \rightarrow a^\prime$, and $b \rightarrow b^\prime$, then $a^\prime \sim_l b^\prime$), we would instead show that a term $a$ and its' erasure are indistinguishable under evaluation, i.e. if $a \rightarrow b$, and $\epsilon_l\ a \rightarrow b^\prime$, then $b^\prime = \epsilon_l\ b$. 

By separating out our shared heap and indexing its' contents by security level, we enable a concise definition for $\epsilon_l$ over our shared thread state. Additionally, by phrasing the scope of our security policy in terms of assigning each active thread with a security level, we also simplify $\epsilon_l$ over our thread pool. The result is promising ground for further formalisation and verification of security properties.

\section{Conclusion}

We have outlined the representation and semantics of a language with concurrency, transactional terms, and side effects. We present an alternative method of structuring terms which provides benefits when defining these semantics.

We have presented a typing system designed to enforce a 'read below your writes' security policy, and have discussed how to define a non-interference property to capture this enforcement.

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\exStmExpr
	\end{minipage}	
	\caption[Grammar of term trees for an STM language]{Grammar of term trees for our STM language.}
	\label{fig:infoflow_stm:tree_grammar}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\exStmTypes
	\end{minipage}	
	\caption[Grammar of types for a secure STM language]{Grammar of types for our labelled-transaction STM language.}
	\label{fig:infoflow_stm:type_grammar}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\exStmAtoms
	\end{minipage}	
	\caption[Terminal Atoms in an STM language]{Grammar of node atoms in term trees for our STM language. Note that some atoms contain types: see \autoref{fig:infoflow_stm:type_grammar}.}
	\label{fig:infoflow_stm:atom_grammar}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\exStmAbbreviations
	\end{minipage}	
	\caption[Abbreviations for Term Trees in an STM language]{Abbreviations for combinations of atoms and term trees, for use in rules and relations on our STM language.}
	\label{fig:infoflow_stm:abbreviations}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exStmSimpleEval
		\end{gather*}
	\end{minipage}	
	\caption[Evaluation rules for pure terms in an STM language]{Evaluation rules for the `interesting cases' of pure terms in our STM language, involving the actual semantics of terms.}
	\label{fig:infoflow_stm:simple_eval}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exStmSimpleStep
		\end{gather*}
	\end{minipage}	
	\caption[Evaluation rules for sub-terms in an STM language]{Evaluation rules for the `boring cases' of pure terms in our STM language, involving structural evaluation rules.}
	\label{fig:infoflow_stm:simple_step}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\exStmValDef
	\end{minipage}	
	\caption[Valuehood predicate on terms of an STM language]{A predicate to determine if a term has no further evaluations under the rules in \autoref{fig:infoflow_stm:simple_step}.}
	\label{fig:infoflow_stm:val_def}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
	\begin{gather*}
		\exStmStmStep
	\end{gather*}
	\end{minipage}	
	\caption[Transaction evaluation rules in an STM language]{Evaluation rules for the effect of an STM transaction term on the shared STM heap.}
	\label{fig:infoflow_stm:stm_step}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exStmLioStep
		\end{gather*}
	\end{minipage}	
	\caption[Side-effect evaluation rules in an STM language]{Evaluation rules for performing side-effectful computations in the context of a multithreaded program.}
	\label{fig:infoflow_stm:lio_step}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exStmSimpleType
		\end{gather*}
	\end{minipage}	
	\caption[Pure term typing rules in a secure STM language]{Typing judgement rules for pure computation terms in our secure STM language with security labels.}
	\label{fig:infoflow_stm:simple_type}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exStmStmType
		\end{gather*}
	\end{minipage}	
	\caption[Transaction term typing rules in a secure STM language]{Typing judgement rules for transactional terms in our secure STM language with security labels.}
	\label{fig:infoflow_stm:stm_type}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\begin{gather*}
		\exStmLioType
		\end{gather*}
	\end{minipage}	
	\caption[Side-effectful term typing rules in a secure STM language]{Typing judgement rules for side-effectful IO terms in our secure STM language with security labels.}
	\label{fig:infoflow_stm:lio_type}
\end{figure}

\begin{figure}[pt]
	\centering
	\begin{minipage}{1\textwidth}
		\exStmGetTVarMemDef
		\exStmSetTVarMemDef
		\exStmAddTVarMemDef
	\end{minipage}	
	\caption[Utility functions for transaction semantics]{Utility functions to increase readability of the STM transaction evaluation rules. These definitions use the function update syntax defined in \autoref{fig:app1:fun_upd}.}
	\label{fig:infoflow_stm:stm_step_funcs}
\end{figure}