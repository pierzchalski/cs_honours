\chapter{Secure Transactional Haskell}
\label{chap:language}

\section{Previous Work}

Exploring IFS in Haskell or Haskell-like languages is not a new concept. \cite{li_encoding_2006} implement a Haskell library able to annotate pure (non-side-effectful) expressions with a domain in a lattice policy using an `arrows' abstraction, and validate that the policy is followed at run-time. 

\cite{tsai_library_2007} extend \cite{li_encoding_2006} to work with side effects and multithreading. They introduce co-operative round-robin scheduling and restrictions on threads running computations that branch on secret variables, which are potentially unrealistic features for systems running on modern non-cooperative kernel schedulers \cite{yun_deterministic_2011}.

\cite{russo_library_2008} also extend \cite{li_encoding_2006}, instead focusing on replacing the `arrows' abstraction with a more general monadic one. They also discuss the implementation of declassification policies. However, their declassification process is ad hoc: they provide an unchecked `escape hatch' with which developers are expected to enforce policy.

\cite{stefan_addressing_2012} independently provide a Haskell library focusing on addressing implicit information flow. Their implementation focuses on the `program counter' trick discussed in \autoref{sec:information_flow:implicit}. The counter is tracked at the type level, while the library uses dynamic checks to ensure that domain-labelled values respect the constraints of the counter. One of their techniques for avoiding implicit IF involves running high-domain behaviour in separate threads and forcing low-domain observers to synchronise with them: it would be interesting to see if this technique can be translated to STM.

\subsection{Other Languages}

Jif is a language implementation in the Java family focusing on IFS. Policy enforcement is done partially at compile time using type-level domain annotations on terms, as well as dynamically using domain-representing labels \cite{zheng_dynamic_2007}. Jif has support for multithreaded programming, and applies restrictions on its use as a way to deal with implicit flows.

FlowCaml is a variant of Caml adding support for a security lattice to the type system, which is verified to guarantee non-interference \cite{simonet2003flow}.

Notably, the majority of prior work has dealt with bare mulitithreading as the primary model of concurrency. Additionally, most projects deal with a security lattice policy. Given the productivity and reasoning improvements provided by STM \cite{pankratius_study_2011}, the utility of IFS as outlined in \autoref{chap:intro}, and the expressive power of non-lattice policies discussed in \autoref{sec:information_flow:non_lattice_policies}, there is clear motivation to investigate how well these concepts work together.

\section{A Toy Language}

To facilitate our investigation, we consider a minimal fragment of Haskell equipped with the STM and IO monads, with syntax and semantics described in \autoref{chap:app1:seth}. We call this toy variant \Seth\ for ‘Secure Transactional Haskell’. \Seth\ captures enough of the semantics of STM to be a non-trivial and pragmatic execution model. The majority of the work of this project will be using \Seth\ as the setting in which to analyse models of IFS. The goals of these analyses is given in \autoref{chap:thesis_plan}.

The semantics outlined in \autoref{chap:app1:seth} are incomplete. They constitute a \textit{functional specification} in that they are an abstracted version of what it means for evaluations to occur transactionally: for instance, the lack of a rule for evaluating an STM expression that doesn't sucessfully return forces the evaluation semantics to choose another expression to evaluate. 

However, a full description of STM must include a precise method for ensuring transaction semantics. The original Haskell implementation described in \cite{harris_composable_2005} uses logging; it would be interesting to see if the choice of implementation technique influenced the IFS properties of the language. 