#!/bin/sh

#changing these to just delete instead of replacing with comments
#might break something in the future
sed "s/\\\begin{isabellebody}//" $1 | sed "s/\\\end{isabellebody}//" | \
sed "s/\\\begin{isamarkuptext}//" | sed "s/\\\end{isamarkuptext}//" | \
sed "s/\\\isadelimML//" | sed "s/\\\endisadelimML//" | \
sed "s/\\\isatagML//" | sed "s/\\\endisatagML//" | \
sed "s/{\\\isasymacute}//g" | \
sed -e ':a' -e 'N' -e '$!ba' -e 's/%*\(\n\)*%*\(\n\)*\\\ignorespacesafterend/\\\ignorespacesafterend/g' | \
sed "s/\\\isa{\\\ /\\\isa{/g" > $2
