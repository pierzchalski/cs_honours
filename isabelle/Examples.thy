theory Examples
imports Main
begin

setup \<open>
    Thy_Output.antiquotation @{binding example} (Scan.lift Args.name)
      (fn {context,source,state} => (fn exnm =>
      (if String.isSubstring  "_" exnm then 
          error ("Underscores may not be used in latex macros") else ();
      "\\newcommand{\\ex" ^ exnm ^ "}{\\ignorespaces ")
      ))
\<close>

setup \<open>Thy_Output.antiquotation @{binding endexample} (Scan.succeed ())
        (fn {context,source,state} => K ("\\ignorespacesafterend}"))
\<close>

end