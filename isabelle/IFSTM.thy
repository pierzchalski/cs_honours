(*<*)
theory IFSTM
imports 
  Main 
  ListUtils
  Examples
  "~~/src/HOL/Library/LaTeXsugar"
  "~~/src/HOL/Library/OptionalSugar"
begin

datatype ('L) Expr =
  Nat nat ("N _")
| Plus "'L Expr" "'L Expr" ("_ + _" [80, 81] 80)
| Var nat ("V _" [101])
| App "'L Expr" "'L Expr" ("_ \<circ> _" [70, 71] 70)
| Abs "'L Expr" ("\<lambda>. _" [60] 60)
| Label "'L Expr" 'L ("L'(_ | _')")

datatype SecLevel = DUMMY
type_synonym SecExpr = "SecLevel Expr"

(*>*)
text_raw \<open>@{example SlcTerms}\<close>
text \<open>
  %\begin{spreadlines}{-1.5\baselineskip}
  \begin{align*}
  @{term n} & :: \mathbb{N} & \\
  @{term l} & :: @{type SecLevel} \\
  @{term c} & :: @{type SecLevel}\ @{type Expr}\ @{text "\<equiv>"} \\
    &\ \hphantom{@{text "|"}}\ @{term "Var n"} \\
    &\ @{text "|"}\ @{term "\<lambda>. c"} \\
    &\ @{text "|"}\ @{term "(c\<^sub>1 :: SecLevel Expr) \<circ> c\<^sub>2"} \\
    &\ @{text "|"}\ @{term "L(c | l)"} \\
    &\ @{text "|"}\ @{term "Nat n"} \\
    &\ @{text "|"}\ @{term "(c\<^sub>1 :: SecLevel Expr) + c\<^sub>2"}
  \end{align*}
  %\end{spreadlines}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

lemma "V 1 \<circ> V 2 \<circ> V 3 = App (App (Var 1) (Var 2)) (Var 3)"
by simp

lemma "\<lambda>. \<lambda>. V 1 \<circ> V 2 = Abs (Abs (App (Var 1) (Var 2)))"
by simp

lemma "V 1 + V 2 + V 3 = Plus (Plus (Var 1) (Var 2)) (Var 3)"
by simp

lemma "\<lambda>. V 0 + N 1 = Abs (Plus (Var 0) (Nat 1))"
by simp

(* 
  "lift k e" leaves each variable fewer than k abstractions deep alone, but 
  bumps up each variable more than k levels deep by 1.
  
  Used for substituting a term with bound variables, for when the term is substituted
  "past" an abstraction.
  
  In "lift k", imagine "k" is a "depth" parameter. In the base case, k=0 and we bump up
  all variables. When we enter a lambda term, we want to bump up all terms other than
  the new, bound term, so k=1. If we encounter another lambda term, there are two bound variables
  (V 0 and V 1), and again every other term we want to bump up.
*)
primrec lift :: "nat \<Rightarrow> 'L Expr \<Rightarrow> 'L Expr" 
where
  "lift k (Var x) = (if x < k then Var x else Var (x + 1))"
| "lift k (l \<circ> r) = (lift k l) \<circ> (lift k r)"
| "lift k (\<lambda>. f) = \<lambda>. lift (k + 1) f"
| "lift k (l + r) = (lift k l) + (lift k r)"
| "lift _ N n = N n"
| "lift k L(e | l) = L(lift k e | l)"

(*>*)
text_raw \<open>@{example LiftDef}\<close>
text \<open>
  \begin{align*}
  @{term "lift k (Var n)"} &= @{term "if n < k then Var n else Var (n + 1)"} \\
  @{term "lift k (l \<circ> r)"} &= @{term "(lift k l) \<circ> (lift k r)"} \\
  @{term "lift k (\<lambda>. f)"} &= @{term "\<lambda>. lift (k + 1) f"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(*>*)
text_raw \<open>@{example LiftDefExt}\<close>
text \<open>
  \begin{align*}
  @{term "lift k (N n)"} &= @{term "N n"} \\
  @{term "lift k (l + r)"} &= @{term "(lift k l) + (lift k r)"} \\
  @{term "lift k (L(e | l))"} &= @{term "L(lift k e | l)"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

primrec subst :: "'L Expr \<Rightarrow> nat \<Rightarrow> 'L Expr \<Rightarrow> 'L Expr" 
  ("_[_ \<mapsto> _]" [100, 0, 0] 60) 
where
  "(V n)[k \<mapsto> e] = (if k = n then e else if n > k then Var (n - 1) else  Var n)"
| "(l + r)[k \<mapsto> e] = (l[k \<mapsto> e]) + (r[k \<mapsto> e])"
| "(N n)[_ \<mapsto> _] = N n"
| "((l :: 'L Expr) \<circ> r)[k \<mapsto> e] = (l[k \<mapsto> e]) \<circ> (r[k \<mapsto> e])"
(*
  In the "outer" term, "V n" refers to the nth bound term. In the "inner term" f, we need to refer
  to V (n + 1) instead. k + 1 adjusts the variables we replace, and lift 0 adjusts the variables
  inside e for where they will be.
*)
| "(\<lambda>. f)[k \<mapsto> e] = \<lambda>. (f[k + 1 \<mapsto> lift 0 e])"
| "L(e | l)[k \<mapsto> e'] = L(e[k \<mapsto> e'] | l)"
print_theorems

(*>*)
text_raw \<open>@{example SubstDef}\<close>
text \<open>
  \begin{align*}
  @{term "(Var n)[k \<mapsto> e]"} &= @{term "if n = k then e else if n > k then Var (n - 1) else  Var n"} \\
  @{term "((l :: 'L Expr) \<circ> r)[k \<mapsto> e]"} &= @{term "(((l :: 'L Expr)[k \<mapsto> e]) :: 'L Expr) \<circ> ((r :: 'L Expr)[k \<mapsto> e])"} \\
  @{term "(\<lambda>. f)[k \<mapsto> e]"} &= @{term "\<lambda>. ((f :: 'L Expr)[k + 1 \<mapsto> lift 0 e])"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(*>*)
text_raw \<open>@{example SubstDefExt}\<close>
text \<open>
  \begin{align*}
  @{term "(N n)[k \<mapsto> e]"} &= @{term "N n"} \\
  @{term "((l :: 'L Expr) + r)[k \<mapsto> e]"} &= @{term "(((l :: 'L Expr)[k \<mapsto> e]) :: 'L Expr) + ((r :: 'L Expr)[k \<mapsto> e])"} \\
  @{term "(L(e | l))[k \<mapsto> e]"} &= @{term "L(e[k \<mapsto> e] | l)"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

schematic_goal "(\<lambda>. (V 2 \<circ> V 0) + V 3)[1 \<mapsto> \<lambda>. V 1 + V 0] = ?x"
by simp

datatype Type = Nat | Fun Type Type ("_ \<Rightarrow> _" [61, 60] 60)

lemma "Nat \<Rightarrow> Nat \<Rightarrow> Nat = Nat \<Rightarrow> (Nat \<Rightarrow> Nat)"
by simp

inductive beta :: "'L Expr \<Rightarrow> 'L Expr \<Rightarrow> bool" 
  ("_ \<rightarrow>\<^sub>\<beta> _" [51, 50] 50) 
where
  plusNat[simp, intro!]: "(N n1) + (N n2) \<rightarrow>\<^sub>\<beta> N (n1 + n2)"
| plusL[simp, intro!]: "l \<rightarrow>\<^sub>\<beta> l' \<Longrightarrow> l + r \<rightarrow>\<^sub>\<beta> l' + r"
| plusR[simp, intro!]: "r \<rightarrow>\<^sub>\<beta> r' \<Longrightarrow> l + r \<rightarrow>\<^sub>\<beta> l + r'"
| appL[simp, intro!]: "l \<rightarrow>\<^sub>\<beta> l' \<Longrightarrow> l \<circ> r \<rightarrow>\<^sub>\<beta> l' \<circ> r"
| appR[simp, intro!]: "r \<rightarrow>\<^sub>\<beta> r' \<Longrightarrow> l \<circ> r \<rightarrow>\<^sub>\<beta> l \<circ> r'"
| beta[simp, intro!]: "fx = f[0 \<mapsto> x] \<Longrightarrow> (\<lambda>.f) \<circ> x \<rightarrow>\<^sub>\<beta> fx"
| abs[simp, intro!]: "f \<rightarrow>\<^sub>\<beta> f' \<Longrightarrow> \<lambda>. f \<rightarrow>\<^sub>\<beta> \<lambda>. f'"
| label[simp, intro]: "e \<rightarrow>\<^sub>\<beta> e' \<Longrightarrow> L(e | l) \<rightarrow>\<^sub>\<beta> e'"

abbreviation beta_reds :: "'L Expr \<Rightarrow> 'L Expr \<Rightarrow> bool"  
  (infixl "\<rightarrow>\<^sub>\<beta>*" 50) 
where
  "s \<rightarrow>\<^sub>\<beta>* t == beta^** s t"
  
lemma beta_refl[intro!]: "e \<rightarrow>\<^sub>\<beta>* e"
using rtrancl.intros by blast

lemma beta_trancl[intro!]: "e \<rightarrow>\<^sub>\<beta> f \<Longrightarrow> f \<rightarrow>\<^sub>\<beta>* g \<Longrightarrow> e \<rightarrow>\<^sub>\<beta>* g"
using rtrancl.intros by simp

lemma "(\<lambda>. \<lambda>. V 1 \<circ> V 0 \<circ> N 3) \<circ> (\<lambda>. \<lambda>. V 1 + V 0) \<circ> N 2 \<rightarrow>\<^sub>\<beta>* (N 5)"
apply (
  simp
| rule beta_trancl
| rule beta.plusNat
| rule beta.beta
| rule beta.appL)+
done

lemma "(\<lambda>. (\<lambda>. \<lambda>. V 1 \<circ> V 0 \<circ> V 2) \<circ> (\<lambda>. \<lambda>. V 1 + V 0 + V 2)) \<circ> N 1 \<circ> N 2 \<rightarrow>\<^sub>\<beta>* N 4"
apply (
  simp
| rule beta_trancl
| rule beta_refl
| rule beta.plusNat
| rule beta.beta
| rule beta.appL
| rule beta.plusL
| rule beta.plusNat)+
done

inductive expr_typing :: "Type list \<Rightarrow> 'L Expr \<Rightarrow> Type \<Rightarrow> bool" 
  ("_ \<turnstile>\<^sub>e _ : _")
where
  nat[simp, intro]: "\<Gamma> \<turnstile>\<^sub>e N n: Nat"
| plus[simp, intro]: "
    \<Gamma> \<turnstile>\<^sub>e lhs: Nat \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>e rhs: Nat \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>e lhs + rhs: Nat"
| var[simp, intro]: "
    \<Gamma> `! n = Some t \<Longrightarrow> 
    \<Gamma> \<turnstile>\<^sub>e V n: t"
| abs[simp, intro]: "
    t1 # \<Gamma> \<turnstile>\<^sub>e fx: t2 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>e \<lambda>. fx: t1 \<Rightarrow> t2"
| label[simp, intro]: "
    \<Gamma> \<turnstile>\<^sub>e e: t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>e L(e | l): t"
| app[simp, intro]: "
    \<Gamma> \<turnstile>\<^sub>e l: t1 \<Rightarrow> t2 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>e r: t1 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>e l \<circ> r: t2"
    
inductive_cases expr_typing_app[dest]: "\<Gamma> \<turnstile>\<^sub>e l \<circ> r: t"
inductive_cases expr_typing_abs[dest]: "\<Gamma> \<turnstile>\<^sub>e \<lambda>. f: t1 \<Rightarrow> t2"
inductive_cases expr_typing_plus[dest]: "\<Gamma> \<turnstile>\<^sub>e x + y: t"
inductive_cases expr_typing_label[dest]: "\<Gamma> \<turnstile>\<^sub>e L(e | l): t"
    
lemma lift_preserves_expr_typing: "
  \<Gamma> \<turnstile>\<^sub>e e: t \<Longrightarrow>
  k \<le> length \<Gamma> \<Longrightarrow>
  insert_at \<Gamma> k t' \<turnstile>\<^sub>e lift k e: t"
apply (induct arbitrary: k t' set: expr_typing)
apply simp_all
apply (metis Suc_leI Suc_le_lessD diff_Suc_1 insert_at_shift_nth le_eq_less_or_eq nth'_some_le var)
apply (metis expr_typing.abs insert_at_cons le_imp_less_Suc less_eq_Suc_le)
apply blast
done

lemma subst_preserves_expr_typing: "
  \<Gamma> \<turnstile>\<^sub>e f: t \<Longrightarrow>
  \<Gamma> = insert_at \<Gamma>' k t' \<Longrightarrow>
  k \<le> length \<Gamma>' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>e x: t' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>e f[k \<mapsto> x]: t"
apply (induct arbitrary: \<Gamma>' x k t' set: expr_typing)
apply simp+
apply safe
apply (subgoal_tac "insert_at \<Gamma>' k t' `! n = \<Gamma>' `! (n - 1)", simp)
apply (rule insert_at_ge, simp+)
using insert_at_length nth'_some_le
apply (metis Suc_eq_plus1 Suc_leI add.commute leD leI)
apply simp
apply (simp add: insert_at_eq)
apply (subgoal_tac "insert_at \<Gamma>' k t' `! n = \<Gamma>' `! n", simp)
apply (rule insert_at_le, simp+)
apply (metis Suc_le_mono expr_typing.abs insert_at.simps le0 length_Cons lift_preserves_expr_typing)
apply simp
by fastforce

(* call-by-name big-step *)
inductive cbnbs :: "'L Expr \<Rightarrow> 'L Expr \<Rightarrow> bool" 
  ("_ \<Down>\<^sub>n _")
where
  abs: "\<lambda>. e \<Down>\<^sub>n \<lambda>. e"
| app: "
    f \<Down>\<^sub>n \<lambda>. e \<Longrightarrow>
    e' = e[0 \<mapsto> x] \<Longrightarrow>
    e' \<Down>\<^sub>n fx \<Longrightarrow>
    f \<circ> x \<Down>\<^sub>n fx"
| nat: "N n \<Down>\<^sub>n N n"
| plus: "
    e1 \<Down>\<^sub>n N n1 \<Longrightarrow> 
    e2 \<Down>\<^sub>n N n2 \<Longrightarrow> 
    n = n1 + n2 \<Longrightarrow>
    e1 + e2 \<Down>\<^sub>n N n"
| label: "
    e \<Down>\<^sub>n e' \<Longrightarrow>
    L(e | l) \<Down>\<^sub>n e'"

inductive_cases cbnbs_abs: "\<lambda>. fx \<Down>\<^sub>n e"
inductive_cases cbnbs_app: "f \<circ> x \<Down>\<^sub>n e"
inductive_cases cbnbs_nat: "N n \<Down>\<^sub>n e"
inductive_cases cbnbs_plus: "e1 + e2 \<Down>\<^sub>n e"
inductive_cases cbnbs_label: "L(e | l) \<Down>\<^sub>n e'"
   
lemma "(\<lambda>. (\<lambda>. \<lambda>. V 1 \<circ> V 0 \<circ> V 2) \<circ> (\<lambda>. \<lambda>. V 1 + V 0 + V 2)) \<circ> N 1 \<circ> N 2 \<Down>\<^sub>n N 4"
by (rule cbnbs.intros, simp?)+

lemma "(\<lambda>. (\<lambda>. \<lambda>. V 1 \<circ> L(V 0 | l) \<circ> V 2) \<circ> (\<lambda>. \<lambda>. V 1 + V 0 + V 2)) \<circ> N 1 \<circ> N 2 \<Down>\<^sub>n N 4"
by (rule cbnbs.intros, simp?)+

lemma cbnbs_preserves_typing: "
  e \<Down>\<^sub>n e' \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>e e: t \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>e e': t"
apply (induct arbitrary: \<Gamma> t set: cbnbs)
apply simp
apply (erule expr_typing_app)
using expr_typing_abs subst_preserves_expr_typing
apply (metis (full_types) insert_at.simps(1) le0)
apply simp
apply (erule expr_typing_plus, simp)
by blast

(* call-by-value big step *)
inductive cbvbs :: "'L Expr \<Rightarrow> 'L Expr \<Rightarrow> bool" 
  ("_ \<Down>\<^sub>v _")
where
  abs: "\<lambda>. e \<Down>\<^sub>v \<lambda>. e"
| app: "
    f \<Down>\<^sub>v \<lambda>. e \<Longrightarrow>
    x \<Down>\<^sub>v x' \<Longrightarrow>
    e' = e[0 \<mapsto> x'] \<Longrightarrow>
    e' \<Down>\<^sub>v fx' \<Longrightarrow>
    f \<circ> x \<Down>\<^sub>v fx'"
| nat: "N n \<Down>\<^sub>v N n"
| plus: "
    e1 \<Down>\<^sub>v N n1 \<Longrightarrow> 
    e2 \<Down>\<^sub>v N n2 \<Longrightarrow> 
    n = n1 + n2 \<Longrightarrow>
    e1 + e2 \<Down>\<^sub>v N n"
| label: "
    e \<Down>\<^sub>v e' \<Longrightarrow>
    L(e | l) \<Down>\<^sub>v e'"
    
inductive_cases cbvbs_abs: "\<lambda>. fx \<Down>\<^sub>v e"
inductive_cases cbvbs_app: "f \<circ> x \<Down>\<^sub>v e"
inductive_cases cbvbs_nat: "N n \<Down>\<^sub>v e"
inductive_cases cbvbs_plus: "e1 + e2 \<Down>\<^sub>v e"
inductive_cases cbvbs_label: "L(e | l) \<Down>\<^sub>v e'"
inductive_cases cbvbs_var: "V n \<Down>\<^sub>v e"

lemma cbvbs_deterministic: "
  e \<Down>\<^sub>v e1 \<Longrightarrow>
  e \<Down>\<^sub>v e2 \<Longrightarrow>
  e1 = e2"
apply (induct arbitrary: e2 set: cbvbs)
using cbvbs_abs apply blast
apply (metis Expr.inject(5) cbvbs_app)
using cbvbs_nat apply blast
apply (metis Expr.inject(1) cbvbs_plus)
by (meson cbvbs_label)    

(* call-by-value small step *)
inductive cbvss :: "'L Expr \<Rightarrow> 'L Expr \<Rightarrow> bool" 
  ("_ \<leadsto>\<^sub>v _") 
where
  plus_nat: "
    N n\<^sub>1 + N n\<^sub>2 \<leadsto>\<^sub>v N n\<^sub>1 + n\<^sub>2"
| plus_left: "
    l \<leadsto>\<^sub>v l' \<Longrightarrow>
    l + r \<leadsto>\<^sub>v l' + r"
| plus_right: "
    r \<leadsto>\<^sub>v r' \<Longrightarrow>
    N n + r \<leadsto>\<^sub>v N n + r'"
| app_left: "
    l \<leadsto>\<^sub>v l' \<Longrightarrow>
    l \<circ> r \<leadsto>\<^sub>v l' \<circ> r"
| app_abs: "
    (\<lambda>. f) \<circ> x \<leadsto>\<^sub>v f[0 \<mapsto> x] "
    
(*>*)
text_raw \<open>@{example SlcEval}\<close>
text \<open>
  @{thm [mode=Axiom] cbvss.app_abs} {\textsc{lambdaStepAppAbs}} \\[1ex]
  @{thm [mode=Rule] cbvss.app_left} {\textsc{lambdaStepAppLeft}} \\[1ex]
  @{thm [mode=Axiom] cbvss.plus_nat} {\textsc{lambdaStepPlusNat}} \\[1ex]
  @{thm [mode=Rule] cbvss.plus_left} {\textsc{lambdaStepPlusLeft}} \\[1ex]
  @{thm [mode=Rule] cbvss.plus_right} {\textsc{lambdaStepPlusRight}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

    
inductive_cases cbvss_plus_cases: "x + y \<leadsto>\<^sub>v e"
inductive_cases cbvss_app_cases: "f \<circ> x \<leadsto>\<^sub>v e"

lemma cbvss_nat_no_step[elim!, intro]: "
  N n \<leadsto>\<^sub>v e \<Longrightarrow>
  False"
using Expr.distinct(5) cbvss.cases by blast

lemma cbvss_abs_no_step[elim!, intro]: "
  \<lambda>. f \<leadsto>\<^sub>v e \<Longrightarrow>
  False"
using Expr.distinct(5) cbvss.cases by blast

definition expr_if :: "'L Expr \<Rightarrow> 'L Expr \<Rightarrow> 'L Expr \<Rightarrow> 'L Expr" 
  ("if {_} then {_} else {_}") 
where
  "if {c} then {t} else {f} \<equiv> c \<circ> t \<circ> f"
  
definition expr_true :: "'L Expr" ("tt") where
  "tt \<equiv> \<lambda>. \<lambda>. V 1"

definition expr_false :: "'L Expr" ("ff") where
  "ff \<equiv> \<lambda>. \<lambda>. V 0"
    
datatype 'L SecType =
  Label "'L ValueType" "'L" ("'(_ | _')")
and 'L ValueType =
  Nat
| Fun "'L SecType" "'L SecType" ("_ \<Rightarrow> _" [61, 60] 60)

notation (latex output)
  ValueType.Nat  ("Nat" 10)

(* security type subtyping *)
inductive st_subtyp :: "('L :: bounded_lattice) SecType \<Rightarrow> 'L SecType \<Rightarrow> bool" 
  ("_ \<le>\<^sub>s _") 
where
  label: "
    l \<le> l' \<Longrightarrow>
    (t | l) \<le>\<^sub>s (t | l')"
| abs: "
    s' \<le>\<^sub>s s \<Longrightarrow>
    t \<le>\<^sub>s t' \<Longrightarrow>
    l \<le> l' \<Longrightarrow>
    (s \<Rightarrow> t | l) \<le>\<^sub>s (s' \<Rightarrow> t' | l')"
    
(*>*)
text_raw \<open>@{example SlcSubTypingRules}\<close>
text \<open>
  @{thm [mode=Rule] st_subtyp.label} {\textsc{lambdaSubLabel}} \\[1ex]
  @{thm [mode=Rule] st_subtyp.abs} {\textsc{lambdaSubAbs}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
    
inductive_cases st_subtyp_abs_cases_left:
  "(s \<Rightarrow> t | l) \<le>\<^sub>s st"
  
inductive_cases st_subtyp_abs_cases_right:
  "st \<le>\<^sub>s (s \<Rightarrow> t | l)"
    
(* security expression typing *)
inductive se_typ :: "('L :: bounded_lattice) SecType list \<Rightarrow> 'L Expr \<Rightarrow> 'L SecType \<Rightarrow> bool"
  ("_ \<turnstile>\<^sub>s _ : _")
where
  nat[simp, intro]: "\<Gamma> \<turnstile>\<^sub>s N n: (Nat | bot)"
| plus[simp, intro]: "
    \<Gamma> \<turnstile>\<^sub>s lhs: (Nat | l) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s rhs: (Nat | l) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s lhs + rhs: (Nat | l)"
| app[simp, intro]: "
    \<Gamma> \<turnstile>\<^sub>s lhs: (t\<^sub>1 \<Rightarrow> (t\<^sub>2 | l) | l) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s rhs: t\<^sub>1 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s lhs \<circ> rhs: (t\<^sub>2 | l)"
| label[simp, intro]: "
    \<Gamma> \<turnstile>\<^sub>s e: (t | l) \<Longrightarrow>
    l \<le> l' \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s L(e | l'): (t | l')"
| var[simp, intro]: "
    \<Gamma> `! n = Some t \<Longrightarrow> 
    \<Gamma> \<turnstile>\<^sub>s V n: t"
| abs[simp, intro]: "
    t\<^sub>1 # \<Gamma> \<turnstile>\<^sub>s f: t\<^sub>2 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s \<lambda>. f: (t\<^sub>1 \<Rightarrow> t\<^sub>2 | bot)"
| sub[simp, intro]: "
    t\<^sub>1 \<le>\<^sub>s t\<^sub>2 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s e: t\<^sub>1 \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s e: t\<^sub>2"
    
(*>*)
text_raw \<open>@{example SlcTypingJudgement}\<close>
text \<open>
  @{term "\<Gamma> \<turnstile>\<^sub>s e: (t | l)"}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(*>*)
text_raw \<open>@{example SlcTypingRules}\<close>
text \<open>
  @{thm [mode=Axiom] se_typ.nat} {\textsc{lambdaTypeNat}} \\[1ex]
  @{thm [mode=Rule] se_typ.plus} {\textsc{lambdaTypePlus}} \\[1ex]
  @{thm [mode=Rule] se_typ.app} {\textsc{lambdaTypeApp}} \\[1ex]
  @{thm [mode=Rule] se_typ.label} {\textsc{lambdaTypeLabel}} \\[1ex]
  @{thm [mode=Rule] se_typ.var} {\textsc{lambdaTypeVar}} \\[1ex]
  @{thm [mode=Rule] se_typ.abs} {\textsc{lambdaTypeAbs}} \\[1ex]
  @{thm [mode=Rule] se_typ.sub} {\textsc{lambdaTypeSub}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
 
inductive_cases se_typ_abs[dest]: "\<Gamma> \<turnstile>\<^sub>s \<lambda>. f: (t | l)"
inductive_cases se_typ_plus[dest]: "\<Gamma> \<turnstile>\<^sub>s x + y: (t | l)"
inductive_cases se_typ_plus'[dest]: "\<Gamma> \<turnstile>\<^sub>s x + y: st"

inductive_simps se_typ_abs_simps: "\<Gamma> \<turnstile>\<^sub>s \<lambda>. f: (t | l)"
inductive_simps se_typ_plus_simps: "\<Gamma> \<turnstile>\<^sub>s x + y: (t | l)"

lemma bool_example: "[] \<turnstile>\<^sub>s L(\<lambda>. \<lambda>. V 0 | top): ((Nat | bot) \<Rightarrow> ((Nat | bot) \<Rightarrow> (Nat | bot) | bot) | top)"
apply (rule se_typ.label)
thm se_typ.abs
apply (rule se_typ.abs)
apply (rule se_typ.abs)
apply (rule se_typ.var)
apply simp+
done

(*>*)
text_raw \<open>@{example LambdaBoolType}\<close>
text \<open>
  @{term "((Nat | L) \<Rightarrow> ((Nat | L) \<Rightarrow> (Nat | L) | L) | H)"}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

lemma lift_preserves_se_typ: "
  \<Gamma> \<turnstile>\<^sub>s e: t \<Longrightarrow>
  k \<le> length \<Gamma> \<Longrightarrow>
  insert_at \<Gamma> k t' \<turnstile>\<^sub>s lift k e: t"
apply (induct arbitrary: k t' set: se_typ)
apply simp+
apply (meson se_typ.app)
apply fastforce
apply (metis One_nat_def add.right_neutral add_Suc_right diff_Suc_1 insert_at_shift_nth le_eq_less_or_eq less_eq_Suc_le lift.simps(1) nth'_some_le se_typ.var)
apply simp
apply (rule se_typ.abs)
using insert_at_cons
apply (metis Suc_n_not_le_n lift_Suc_mono_le nat_le_linear)
by blast
    
lemma subst_preserves_se_typ: "
  \<Gamma> \<turnstile>\<^sub>s f: t \<Longrightarrow>
  \<Gamma> = insert_at \<Gamma>' k t' \<Longrightarrow>
  k \<le> length \<Gamma>' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>s x: t' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>s f[k \<mapsto> x]: t"
apply (induct arbitrary: \<Gamma>' x k t' set: se_typ)
apply simp_all
apply safe
apply (rule_tac ?t\<^sub>1=t\<^sub>1 in se_typ.app)
apply blast
apply simp
apply blast
apply (subgoal_tac "insert_at \<Gamma>' k t' `! n = \<Gamma>' `! (n - 1)", simp)
apply (rule insert_at_ge, simp+)
apply (subgoal_tac "n < length (insert_at \<Gamma>' k t')")
using insert_at_length apply simp
using nth'_some_le insert_at_length apply blast
apply (simp add: insert_at_eq)
apply (simp add: insert_at_le)
apply (rule se_typ.abs)
apply (subst (asm) insert_at_cons)+
using lift_preserves_se_typ
by (metis Suc_leI insert_at.simps(1) le0 le_less_trans length_Cons lessI) 

(*>*)
text_raw \<open>@{example SlcSubstPreservesType}\<close>
text \<open>
  @{thm [mode=Rule] subst_preserves_se_typ} {\textsc{lambdaSubstPreservesType}}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(* just confirming that high things in the environment don't doom something to be high *)
lemma "
  (Nat | bot) # (Nat | top) # [] \<turnstile>\<^sub>s \<lambda>. V 0 + V 1: ((Nat | bot) \<Rightarrow> (Nat | bot) | bot)"
apply (simp add: se_typ_abs_simps)
done

lemma st_subtyp_trans': "
  t1 \<le>\<^sub>s t2 \<Longrightarrow>
  (\<forall> t3. 
    t2 \<le>\<^sub>s t3 \<longrightarrow> 
    t1 \<le>\<^sub>s t3) \<and>
  (\<forall> t0.
    t0 \<le>\<^sub>s t1 \<longrightarrow>
    t0 \<le>\<^sub>s t2)"
apply (induct arbitrary: set: st_subtyp)
apply safe
apply (erule st_subtyp.cases, simp_all)
apply (meson order.trans st_subtyp.label)
apply (meson order.trans st_subtyp.abs)
apply (erule st_subtyp.cases, simp_all)
apply (meson order.trans st_subtyp.label)
apply (meson order.trans st_subtyp.abs)
apply (erule st_subtyp_abs_cases_left, simp_all)
apply (meson order.trans st_subtyp.abs)
apply (meson order.trans st_subtyp.abs)
apply (erule st_subtyp_abs_cases_right, simp_all)
apply (meson order.trans st_subtyp.abs)
apply (meson order.trans st_subtyp.abs)
done

lemma st_subtyp_trans[intro!]: "
  t1 \<le>\<^sub>s t2 \<Longrightarrow>
  t2 \<le>\<^sub>s t3 \<Longrightarrow>
  t1 \<le>\<^sub>s t3"
using st_subtyp_trans' by blast

lemma st_subtyp_label_le: "
  st \<le>\<^sub>s st' \<Longrightarrow>
  st = (t | l) \<Longrightarrow>
  st' = (t' | l') \<Longrightarrow>
  l \<le> l'"
apply (induct set: st_subtyp)
apply simp
by simp

lemma st_subtyp_plus_elim': "
  \<Gamma> \<turnstile>\<^sub>s e: ts \<Longrightarrow>
  ts = (t | l) \<Longrightarrow>
  e = x + y \<Longrightarrow>
  ts \<le>\<^sub>s (t' | l') \<Longrightarrow>
  t = Nat \<and> t' = Nat \<and> l \<le> l'"
apply (induct arbitrary: x y t l t' l' set: se_typ)
apply simp_all
apply (erule st_subtyp.cases, simp_all)
by (metis SecType.exhaust st_subtyp_label_le st_subtyp_trans')

lemma st_subtyp_plus_elim: "
  \<Gamma> \<turnstile>\<^sub>s x + y: (t | l) \<Longrightarrow>
  (t | l) \<le>\<^sub>s (t' | l') \<Longrightarrow>
  t = Nat \<and> t' = Nat \<and> l \<le> l'"
using st_subtyp_plus_elim' by fastforce

lemma se_typ_plus_cases_impl: "
  \<Gamma> \<turnstile>\<^sub>s e: ts \<Longrightarrow>
  e = x + y \<Longrightarrow>
  ts \<le>\<^sub>s (t | l) \<Longrightarrow>
  (\<And> l1 l2.
    \<Gamma> \<turnstile>\<^sub>s x: (Nat | l1) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s y: (Nat | l2) \<Longrightarrow>
    l1 \<le> l \<Longrightarrow>
    l2 \<le> l \<Longrightarrow>
    t = Nat \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_typ)
apply simp_all
apply (meson le_sup_iff se_typ.plus st_subtyp_plus_elim')
by blast

lemma se_typ_plus_cases[elim]: "
  \<Gamma> \<turnstile>\<^sub>s x + y: (t | l) \<Longrightarrow>
  (\<And> l1 l2.
    \<Gamma> \<turnstile>\<^sub>s x: (Nat | l1) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s y: (Nat | l2) \<Longrightarrow>
    l1 \<le> l \<Longrightarrow>
    l2 \<le> l \<Longrightarrow>
    t = Nat \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson order_refl se_typ_plus_cases_impl st_subtyp.label)

lemma se_typ_plus_intro_present: "
  \<Gamma> \<turnstile>\<^sub>s lhs: (Nat | l\<^sub>1) \<Longrightarrow>
  l\<^sub>1 \<le> l \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s rhs: (Nat | l\<^sub>2) \<Longrightarrow>
  l\<^sub>2 \<le> l \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s lhs + rhs: (Nat | l)"
by (meson se_typ.plus st_subtyp.label sub)

(*>*)
text_raw \<open>@{example Nat}\<close>
text \<open>
  @{term ValueType.Nat}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(*>*)
text_raw \<open>@{example SlcPlusAlternative}\<close>
text \<open>
  @{thm [mode=Rule] se_typ_plus_intro_present} {\textsc{lambdaTypePlusAlternative}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

inductive_cases demo_cases: "\<Gamma> \<turnstile>\<^sub>s f \<circ> x: (t | l)"
thm demo_cases
(*>*)
text_raw \<open>@{example SlcInductiveCases}\<close>
text \<open>
  @{thm [mode=Rule] demo_cases} {\textsc{lambdaTypeAppCases}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

lemma se_typ_app_cases_impl: "
  \<Gamma> \<turnstile>\<^sub>s e: st \<Longrightarrow>
  e = f \<circ> x \<Longrightarrow>
  st \<le>\<^sub>s (t2 | l) \<Longrightarrow>
  (\<And> t1 lf l2 t2'.
    \<Gamma> \<turnstile>\<^sub>s f: (t1 \<Rightarrow> (t2' | l2) | lf) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s x: t1 \<Longrightarrow>
    (t2' | l2) \<le>\<^sub>s (t2 | l) \<Longrightarrow>
    (t2' | lf) \<le>\<^sub>s (t2 | l) \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_typ)
apply simp_all
by blast

thm se_typ.cases

lemma se_typ_app_cases[elim]: "
  \<Gamma> \<turnstile>\<^sub>s f \<circ> x: (t2 | l) \<Longrightarrow>
  (\<And> t1 lf l2 t2'.
    \<Gamma> \<turnstile>\<^sub>s f: (t1 \<Rightarrow> (t2' | l2) | lf) \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s x: t1 \<Longrightarrow>
    (t2' | l2) \<le>\<^sub>s (t2 | l) \<Longrightarrow>
    (t2' | lf) \<le>\<^sub>s (t2 | l) \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson order_refl se_typ_app_cases_impl st_subtyp.label)

lemma se_typ_abs_cases_impl: "
  \<Gamma> \<turnstile>\<^sub>s e: st \<Longrightarrow>
  e = \<lambda>. f \<Longrightarrow>
  st \<le>\<^sub>s (s \<Rightarrow> t | l) \<Longrightarrow>
  (\<And> s' t'.
    s \<le>\<^sub>s s' \<Longrightarrow>
    t' \<le>\<^sub>s t \<Longrightarrow>
    s' # \<Gamma> \<turnstile>\<^sub>s f: t' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_typ)
apply simp_all
apply (erule st_subtyp.cases)
apply (metis SecType.exhaust SecType.inject ValueType.inject order_refl st_subtyp.label)
apply simp
by blast

lemma se_typ_abs_cases[elim]: "
  \<Gamma> \<turnstile>\<^sub>s \<lambda>. f: (s \<Rightarrow> t | l) \<Longrightarrow>
  (\<And> s' t'.
    s \<le>\<^sub>s s' \<Longrightarrow>
    t' \<le>\<^sub>s t \<Longrightarrow>
    s' # \<Gamma> \<turnstile>\<^sub>s f: t' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson order_refl se_typ_abs_cases_impl st_subtyp.label)

lemma se_typ_nat_cases_impl: "
  \<Gamma> \<turnstile>\<^sub>s e: st \<Longrightarrow>
  e = N n \<Longrightarrow>
  st \<le>\<^sub>s (t | l) \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l) \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_typ)
apply simp_all
apply blast
done

lemma se_typ_nat_cases: "
  \<Gamma> \<turnstile>\<^sub>s N n: (t | l) \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l) \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson order_refl se_typ_nat_cases_impl st_subtyp.label)

lemma se_typ_var_cases_impl: "
  \<Gamma> \<turnstile>\<^sub>s e: st \<Longrightarrow>
  e = V n \<Longrightarrow>
  st \<le>\<^sub>s (t | l) \<Longrightarrow>
  (\<And> st'.
    \<Gamma> `! n = Some st' \<Longrightarrow>
    st' \<le>\<^sub>s (t | l) \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_typ)
apply simp_all
by blast

lemma se_typ_var_cases[elim]: "
  \<Gamma> \<turnstile>\<^sub>s V n: (t | l) \<Longrightarrow>
  (\<And> st'.
    \<Gamma> `! n = Some st' \<Longrightarrow>
    st' \<le>\<^sub>s (t | l) \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson order_refl se_typ_var_cases_impl st_subtyp.label)

lemma st_subtyp_refl[intro]: "
  st \<le>\<^sub>s st"
by (metis SecType.exhaust order_refl st_subtyp.label)

lemma cbvss_preserves_se_typ: "
  e \<leadsto>\<^sub>v e' \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s e: t \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s e': t"
apply (induct arbitrary: \<Gamma> t set: cbvss)
apply (case_tac t, simp)
apply (erule se_typ_plus, simp)
apply (meson bot.extremum se_typ.nat st_subtyp.label sub)
apply (case_tac t\<^sub>1, simp)
using st_subtyp_plus_elim apply (metis bot.extremum se_typ.nat st_subtyp.label sub)
apply (case_tac t, simp)
apply (erule se_typ_plus_cases)
apply (meson le_sup_iff se_typ.plus st_subtyp.label sub)
apply (case_tac t, simp)
apply (erule se_typ_plus_cases)
apply (metis se_typ.plus st_subtyp.label sub)
apply (case_tac t, simp)
apply (erule se_typ_app_cases)
apply (meson se_typ.app st_subtyp.abs st_subtyp_label_le st_subtyp_refl sub)
apply (case_tac t, simp)
apply (erule se_typ_app_cases)
apply (meson se_typ.app st_subtyp.abs st_subtyp_label_le st_subtyp_refl sub)
apply (erule se_typ_abs_cases)
by (metis (full_types) insert_at.simps(1) le0 sub subst_preserves_se_typ)

(*>*)
text_raw \<open>@{example SlcStepPreservesType}\<close>
text \<open>
  @{thm [mode=Rule] cbvss_preserves_se_typ} {\textsc{lambdaStepPreservesType}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(* 
  security expression equivalence
  for term indistinguishability / non-interference, we need some way to say
  "these two terms are indistinguishable, up to level l in the lattice".
  Pulled from <dependent type theory thing>.
*)
inductive se_equiv :: 
  "('L :: bounded_lattice) SecType list \<Rightarrow> 'L Expr \<Rightarrow> 'L \<Rightarrow> 'L Expr \<Rightarrow> 'L SecType \<Rightarrow> bool" 
  ("_ \<turnstile>\<^sub>s\<^sub>e _ ='(_') _: _") 
where
  nat[intro!]: "
    \<Gamma> \<turnstile>\<^sub>s N n: st \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e N n =(l) N n: st"
| var[intro!]: "
    \<Gamma> \<turnstile>\<^sub>s V n: st \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e V n =(l) V n: st"
| plus[intro!]: "
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) x': (Nat | l') \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e y =(l) y': (Nat | l') \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e x + y =(l) x' + y': (Nat | l')"
| app[intro]: "
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) f': (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) x': s \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e f \<circ> x =(l) f' \<circ> x': (t | l')"
| abs[intro]: "
    s # \<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) f': t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e \<lambda>. f =(l) \<lambda>. f': (s \<Rightarrow> t | bot)"
| sub[intro]: "
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1 =(l) e\<^sub>2: st \<Longrightarrow>
    st \<le>\<^sub>s st' \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1 =(l) e\<^sub>2: st'"
| hide[intro]: "
    \<Gamma> \<turnstile>\<^sub>s e\<^sub>1: (t | l') \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s e\<^sub>2: (t | l') \<Longrightarrow>
    l \<le> l' \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1 =(l) e\<^sub>2: (t | l')"
| label[intro]: "
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1 =(l) e\<^sub>2: (t | l') \<Longrightarrow>
    l' \<le> l'' \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e L(e\<^sub>1 | l'') =(l) L(e\<^sub>2 | l''): (t | l'')"
    
(*>*)
text_raw \<open>@{example SlcEquivalenceRules}\<close>
text \<open>
  @{thm [mode=Rule] se_equiv.nat} {\textsc{lambdaEqNat}} \\[1ex]
  @{thm [mode=Rule] se_equiv.plus} {\textsc{lambdaEqPlus}} \\[1ex]
  @{thm [mode=Rule] se_equiv.app} {\textsc{lambdaEqApp}} \\[1ex]
  @{thm [mode=Rule] se_equiv.label} {\textsc{lambdaEqLabel}} \\[1ex]
  @{thm [mode=Rule] se_equiv.var} {\textsc{lambdaEqVar}} \\[1ex]
  @{thm [mode=Rule] se_equiv.abs} {\textsc{lambdaEqAbs}} \\[1ex]
  @{thm [mode=Rule] se_equiv.sub} {\textsc{lambdaEqSub}} \\[1ex]
  @{thm [mode=Rule] se_equiv.hide} {\textsc{lambdaEqHide}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
    
inductive_cases se_equiv_nat_cases_left: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e N n =(l) r: st"
inductive_cases se_equiv_app_cases_left: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e f \<circ> x =(l) r: st"
inductive_cases se_equiv_abs_cases: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e \<lambda>. f =(l) \<lambda>. f': (t | l')"
    
lemma se_equiv_implies_se_typ_impl: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s e1: st \<and> \<Gamma> \<turnstile>\<^sub>s e2: st"
apply (induct arbitrary: set: se_equiv)
apply simp
apply simp
apply simp
apply (meson cbvss.plus_nat cbvss_preserves_se_typ)
apply (meson se_typ.intros)+
done

lemma se_equiv_implies_se_typ[intro]:
  assumes equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st"
  shows "\<Gamma> \<turnstile>\<^sub>s e1: st" and "\<Gamma> \<turnstile>\<^sub>s e2: st"
proof -
  show "\<Gamma> \<turnstile>\<^sub>s e1: st"
    using equiv se_equiv_implies_se_typ_impl by force
  show "\<Gamma> \<turnstile>\<^sub>s e2: st"
    using equiv se_equiv_implies_se_typ_impl by force
qed
    
lemma lift_preserves_se_equiv: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st \<Longrightarrow>
  k \<le> length \<Gamma> \<Longrightarrow>
  insert_at \<Gamma> k st' \<turnstile>\<^sub>s\<^sub>e lift k e1 =(l) lift k e2: st"
apply (induct arbitrary: k st' set: se_equiv)
apply (metis lift.simps(5) lift_preserves_se_typ se_equiv.nat)
apply (metis lift.simps(1) lift_preserves_se_typ se_equiv.var)
apply (simp add: se_equiv.plus)
apply (metis (no_types, lifting) lift.simps(2) se_equiv.app)
apply simp
apply (metis Suc_n_not_le_n insert_at.simps(2) lift_Suc_mono_le nat_le_linear se_equiv.abs)
apply (meson sub)
apply (simp add: hide lift_preserves_se_typ)
apply fastforce
done

lemma subst_preserves_se_equiv: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st \<Longrightarrow>
  k \<le> length \<Gamma>' \<Longrightarrow>
  \<Gamma> = insert_at \<Gamma>' k st' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>s\<^sub>e v1 =(l) v2: st' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>s\<^sub>e e1[k \<mapsto> v1] =(l) e2[k \<mapsto> v2]: st"
apply (induct arbitrary: \<Gamma>' k st' v1 v2 set: se_equiv)
apply simp
apply (case_tac st, simp, erule se_typ_nat_cases)
apply (simp add: se_equiv.nat)
apply (case_tac st, simp, erule se_typ_var_cases)
apply safe
apply (rule_tac ?t\<^sub>1=st'a in se_typ.sub, simp)
apply (rule se_typ.var)
apply (metis One_nat_def insert_at_le')
apply (simp add: insert_at_eq sub)
apply (simp add: insert_at_le se_equiv.var)
apply (simp add: se_equiv.plus)
apply simp
apply (metis se_equiv.app)
apply simp
apply (rule se_equiv.abs)
apply (subgoal_tac "s # \<Gamma>' \<turnstile>\<^sub>s\<^sub>e lift 0 v1 =(l) lift 0 v2: st'")
using insert_at_cons apply simp
using lift_preserves_se_equiv apply (metis (full_types) insert_at.simps(1) le0)
apply (metis sub)
using subst_preserves_se_typ se_equiv_implies_se_typ apply (metis hide)
apply fastforce
done
    
lemma se_equiv_sym: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e2 =(l) e1: st"
apply (induct set: se_equiv)
apply blast+
done

lemma se_equiv_cbvss_hide_preserves_se_equiv[intro]: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: (t | l') \<Longrightarrow>
  l \<le> l' \<Longrightarrow>
  e1 \<leadsto>\<^sub>v e1' \<Longrightarrow>
  e2 \<leadsto>\<^sub>v e2' \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': (t | l')"
using cbvss_preserves_se_typ se_equiv_implies_se_typ se_equiv.hide by metis

lemma se_equiv_nat_cases_left_impl: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st \<Longrightarrow>
  e1 = N n \<Longrightarrow>
  st \<le>\<^sub>s (t | l') \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l') \<Longrightarrow>
    e2 = N n \<Longrightarrow>
    P) \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l') \<Longrightarrow>
    l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_equiv)
apply simp_all
apply (case_tac st, simp)
apply (erule se_typ_nat_cases)
apply blast
apply blast
apply (erule se_typ_nat_cases)
by (meson order_trans st_subtyp_label_le st_subtyp_trans)

lemma se_equiv_nat_cases_left': "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e N n =(l) e2: (t | l') \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l') \<Longrightarrow>
    e2 = N n \<Longrightarrow>
    P) \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l') \<Longrightarrow>
    l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson se_equiv_nat_cases_left_impl st_subtyp_refl)

lemma se_equiv_nat_cases_right': "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) N n: (t | l') \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l') \<Longrightarrow>
    e1 = N n \<Longrightarrow>
    P) \<Longrightarrow>
  ((Nat | bot) \<le>\<^sub>s (t | l') \<Longrightarrow>
    l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson se_equiv_nat_cases_left' se_equiv_sym)

(* there _must_ be a way to make this simpler. This is absurd! *)
lemma cbvss_noninterference_nat:
  assumes x_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) x': (Nat | l')"
  assumes y_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e y =(l) y': (Nat | l')"
  assumes x_step_equiv: "\<And> e1' e2'. x \<leadsto>\<^sub>v e1' \<Longrightarrow> x' \<leadsto>\<^sub>v e2' \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': (Nat | l')"
  assumes y_step_equiv: "\<And> e1' e2'. y \<leadsto>\<^sub>v e1' \<Longrightarrow> y' \<leadsto>\<^sub>v e2' \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': (Nat | l')"
  assumes lhs_step: "x + y \<leadsto>\<^sub>v e1'"
  assumes rhs_step: "x' + y' \<leadsto>\<^sub>v e2'"
  shows "\<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': (Nat | l')"
proof -
from lhs_step show ?thesis 
proof (rule cbvss_plus_cases)
next
  fix n1 n2
  assume nat_eqs: "e1' = N (n1 + n2)" "x = N n1" "y = N n2"
  from x_equiv nat_eqs(2) have x'_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e N n1 =(l) x': (ValueType.Nat | l')"
  by simp
  from y_equiv nat_eqs(3) have y'_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e N n2 =(l) y': (ValueType.Nat | l')"
  by simp
  from x'_equiv show ?thesis 
  proof (rule se_equiv_nat_cases_left')
  next assume x'_eq: "x' = N n1"
    from y'_equiv show ?thesis
    proof (rule se_equiv_nat_cases_left')
    next assume y'_eq: "y' = N n2"
      from x'_eq y'_eq rhs_step have e2'_eq: "e2' = N n1 + n2"
      using cbvss_plus_cases by (metis Expr.inject(1) cbvss_nat_no_step)
      from nat_eqs(1) e2'_eq show ?thesis
      by (meson se_equiv.nat se_equiv_nat_cases_left_impl se_typ.nat st_subtyp_refl sub x'_equiv)
    next assume level: "l \<le> l'"
      from level lhs_step rhs_step x_equiv y_equiv
      show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.plus by smt 
    qed
  next assume level: "l \<le> l'"
    from level lhs_step rhs_step x_equiv y_equiv
    show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.plus by smt
  qed
next
  fix x1'
  assume e1'_eq: "e1' = x1' + y" 
  assume x_step': "x \<leadsto>\<^sub>v x1'"
  from rhs_step show ?thesis
  proof (rule cbvss_plus_cases)
  next
    fix n1 n2
    assume eqs: "e2' = N (n1 + n2)" "x' = N n1" "y' = N n2"
    from eqs(2) x_equiv have x_equiv_nat: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) N n1: (ValueType.Nat | l')"
    by simp
    from x_equiv_nat show ?thesis
    proof (rule se_equiv_nat_cases_right')
    next
      assume x_nat: "x = N n1"
      from x_nat x_step' show ?thesis using cbvss_nat_no_step by force
    next
      assume level: "l \<le> l'"
      from level lhs_step rhs_step x_equiv y_equiv
      show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.plus by smt
    qed
  next
    fix x2'
    assume e2'_eq: "e2' = x2' + y'" 
    assume x'_step': "x' \<leadsto>\<^sub>v x2'"
    from x_step_equiv x_step' x'_step' have x1'x2'_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e x1' =(l) x2': (ValueType.Nat | l')"
    by simp
    from y_equiv x1'x2'_equiv e2'_eq e1'_eq show ?thesis
    using se_equiv.plus by blast
  next
    fix y2' n
    assume e2'_eq: "e2' = (N n) + y2'"
    assume x'_eq: "x' = N n"
    assume y'_step': "y' \<leadsto>\<^sub>v y2'"
    from x'_eq x_equiv have x_equiv_nat: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) N n: (ValueType.Nat | l')" by simp
    from x_equiv_nat show ?thesis
    proof (rule se_equiv_nat_cases_right')
    next
      assume x_eq: "x = N n"
      from x_eq x_step' cbvss_nat_no_step show ?thesis by fastforce
    next
      assume level: "l \<le> l'"
      from level lhs_step rhs_step x_equiv y_equiv
      show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.plus by smt
    qed
  qed
next
  fix y1' n1
  assume e1'_eq: "e1' = (N n1) + y1'"
  assume y_step': "y \<leadsto>\<^sub>v y1'"
  assume x_eq: "x = N n1"
  from x_eq x_equiv have x'_equiv_nat: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e N n1 =(l) x': (ValueType.Nat | l')" by simp
  from x'_equiv_nat show ?thesis
  proof (rule se_equiv_nat_cases_left')
  next
    assume x'_eq: "x' = N n1"
    from rhs_step show ?thesis
    proof (rule cbvss_plus_cases)
    next
      fix n2
      assume y'_nat: "y' = N n2"
      from y'_nat y_equiv have y_equiv_nat: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e y =(l) N n2: (ValueType.Nat | l')" by simp
      from y_equiv_nat show ?thesis
      proof (rule se_equiv_nat_cases_right')
      next
        assume y_eq: "y = N n2"
        from y_step' y_eq cbvss_nat_no_step show ?thesis by fastforce
      next
        assume level: "l \<le> l'"
        from level lhs_step rhs_step x_equiv y_equiv
        show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.plus by smt
      qed
    next
      fix x2'
      assume x'_step': "x' \<leadsto>\<^sub>v x2'"
      from x'_eq x'_step' cbvss_nat_no_step show ?thesis by fastforce
    next
      fix n2 y2'
      assume x'_eq': "x' = N n2"
      assume e2'_eq: "e2' = (N n2) + y2'"
      assume y'_step': "y' \<leadsto>\<^sub>v y2'"
      from x'_eq x'_eq' have n_eq: "n1 = n2" by simp
      from y_step' y'_step' y_step_equiv have y_equiv': "\<Gamma> \<turnstile>\<^sub>s\<^sub>e y1' =(l) y2': (ValueType.Nat | l')"
      by simp
      show ?thesis using e1'_eq e2'_eq x'_eq' x'_equiv_nat y_equiv' by blast
    qed
  next
    assume level: "l \<le> l'"
    from level lhs_step rhs_step x_equiv y_equiv 
    have typs: "\<Gamma> \<turnstile>\<^sub>s e1': (Nat | l')" "\<Gamma> \<turnstile>\<^sub>s e2': (Nat | l')"
    using cbvss_preserves_se_typ se_equiv_implies_se_typ se_equiv.hide se_typ.plus
    by metis+
    from typs level show ?thesis 
    using hide by blast
  qed
qed
qed

lemma se_equiv_abs_cases_left_impl: "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) e2: st \<Longrightarrow>
  e1 = \<lambda>. f \<Longrightarrow>
  st \<le>\<^sub>s (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
  (\<And> s' t' f'.
    (s' \<Rightarrow> t' | bot) \<le>\<^sub>s (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
    e2 = \<lambda>. f' \<Longrightarrow>
    s' # \<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) f': t' \<Longrightarrow>
    P) \<Longrightarrow>
  (l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
apply (induct set: se_equiv)
apply simp_all
apply blast
by (meson order_trans st_subtyp_label_le)

lemma se_equiv_abs_cases_left': "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e \<lambda>. f =(l) e2: (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
  (\<And> s' t' f'.
    (s' \<Rightarrow> t' | bot) \<le>\<^sub>s (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
    e2 = \<lambda>. f' \<Longrightarrow>
    s' # \<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) f': t' \<Longrightarrow>
    P) \<Longrightarrow>
  (l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson se_equiv_abs_cases_left_impl st_subtyp_refl)

lemma se_equiv_abs_cases_right': "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1 =(l) \<lambda>. f: (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
  (\<And> s' t' f'.
    (s' \<Rightarrow> t' | bot) \<le>\<^sub>s (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
    e1 = \<lambda>. f' \<Longrightarrow>
    s' # \<Gamma> \<turnstile>\<^sub>s\<^sub>e f' =(l) f: t' \<Longrightarrow>
    P) \<Longrightarrow>
  (l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (meson se_equiv_abs_cases_left' se_equiv_sym)

lemma se_equiv_abs_cases': "
  \<Gamma> \<turnstile>\<^sub>s\<^sub>e \<lambda>. f =(l) \<lambda>. f': (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
  (\<And> s' t'.
    (s' \<Rightarrow> t' | bot) \<le>\<^sub>s (s \<Rightarrow> (t | l') | l') \<Longrightarrow>
    s' # \<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) f': t' \<Longrightarrow>
    P) \<Longrightarrow>
  (l \<le> l' \<Longrightarrow>
    P) \<Longrightarrow>
  P"
by (metis Expr.inject(5) se_equiv_abs_cases_left')

lemma cbvss_noninterference_app:
  assumes f_f'_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) f': (s \<Rightarrow> (t | l') | l')"
  assumes f_f'_step_induct: "\<And> e1' e2'. 
    f \<leadsto>\<^sub>v e1' \<Longrightarrow> 
    f' \<leadsto>\<^sub>v e2' \<Longrightarrow> 
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': (s \<Rightarrow> (t | l') | l')"
  assumes x_x'_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) x': s"
  assumes x_x'_step_induct: "\<And> e1' e2'. 
    x \<leadsto>\<^sub>v e1' \<Longrightarrow> 
    x' \<leadsto>\<^sub>v e2' \<Longrightarrow> 
    \<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': s"
  assumes lhs_step: "f \<circ> x \<leadsto>\<^sub>v e1'"
  assumes rhs_step: "f' \<circ> x' \<leadsto>\<^sub>v e2'"
  shows "\<Gamma> \<turnstile>\<^sub>s\<^sub>e e1' =(l) e2': (t | l')"
proof -
from lhs_step show ?thesis
proof (rule cbvss_app_cases)
next
  fix f1
  assume e1'_eq: "e1' = f1 \<circ> x"
  assume f_step: "f \<leadsto>\<^sub>v f1"
  from rhs_step show ?thesis
  proof (rule cbvss_app_cases)
  next
    fix f2'
    assume e2'_eq: "e2' = f2' \<circ> x'"
    assume f'_step: "f' \<leadsto>\<^sub>v f2'"
    from f_step f'_step f_f'_step_induct 
    have f_f'_equiv': "\<Gamma> \<turnstile>\<^sub>s\<^sub>e f1 =(l) f2': (s \<Rightarrow> (t | l') | l')"
    by simp
    from f_f'_equiv' x_x'_equiv e1'_eq e2'_eq show ?thesis
    apply (simp)
    apply (rule se_equiv.app, simp+)
    done
  next
    fix f'f
    assume eqs: "f' = \<lambda>. f'f" 
    from eqs f_f'_equiv have f_f'_equiv': "\<Gamma> \<turnstile>\<^sub>s\<^sub>e f =(l) \<lambda>. f'f: (s \<Rightarrow> (t | l') | l')" by simp
    from f_f'_equiv' show ?thesis
    proof (rule se_equiv_abs_cases_right')
    next
      fix ff'
      assume f_eq: "f = \<lambda>. ff'"
      from f_eq f_step cbvss_abs_no_step show ?thesis by fastforce
    next
      assume level: "l \<le> l'"
      from level lhs_step rhs_step f_f'_equiv x_x'_equiv
      show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.app by smt
    qed
  qed
next
  fix ff'
  assume lhs_eqs: "f = \<lambda>. ff'" "e1' = (ff' :: 'a Expr)[0 \<mapsto> x]"
  from rhs_step show ?thesis
  proof (rule cbvss_app_cases)
  next
    fix f2'
    assume f'_step: "f' \<leadsto>\<^sub>v f2'"
    from lhs_eqs f_f'_equiv have f_f'_equiv': "\<Gamma> \<turnstile>\<^sub>s\<^sub>e  \<lambda>. ff' =(l) f': (s \<Rightarrow> (t | l') | l')" by simp
    from f_f'_equiv' show ?thesis
    proof (rule se_equiv_abs_cases_left')
    next
      fix f'f
      assume f'_eq: "f' = \<lambda>. f'f"
      from f'_eq f'_step cbvss_abs_no_step show ?thesis by fastforce
    next
      assume level: "l \<le> l'"
      from level lhs_step rhs_step f_f'_equiv x_x'_equiv
      show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.app by smt
    qed
  next
    fix x2' f'f
    assume rhs_eqs: "e2' = (f'f :: 'a Expr)[0 \<mapsto> x']" "f' = \<lambda>. f'f"
    from lhs_eqs rhs_eqs f_f'_equiv 
    have f_f'_equiv': "\<Gamma> \<turnstile>\<^sub>s\<^sub>e  \<lambda>. ff' =(l) \<lambda>. f'f: (s \<Rightarrow> (t | l') | l')" by simp
    from f_f'_equiv' show ?thesis
    proof (rule se_equiv_abs_cases')
    next
      fix s' t'
      assume sub: "(s' \<Rightarrow> t' | bot) \<le>\<^sub>s (s \<Rightarrow> (t | l') | l')"
      assume ff'_f'f_equiv: "s' # \<Gamma> \<turnstile>\<^sub>s\<^sub>e ff' =(l) f'f: t'"
      from sub have s_s'_sub: "s \<le>\<^sub>s s'"
      apply (rule st_subtyp.cases)
      apply simp
      apply (metis ValueType.inject st_subtyp_refl)
      by simp
      from s_s'_sub x_x'_equiv have x_x'_equiv': "\<Gamma> \<turnstile>\<^sub>s\<^sub>e x =(l) x': s'"
      using se_equiv.sub by blast
      from x_x'_equiv' ff'_f'f_equiv have e1'_e2'_equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e ff'[0 \<mapsto> x] =(l) f'f[0 \<mapsto> x']: t'"
      using subst_preserves_se_equiv insert_at.simps(1) le0 by force
      from sub have t'_t_sub: "t' \<le>\<^sub>s (t | l')"
      apply (rule st_subtyp.cases)
      apply (metis SecType.inject ValueType.inject st_subtyp_refl)
      by auto[1]
      from e1'_e2'_equiv t'_t_sub lhs_eqs rhs_eqs show ?thesis using se_equiv.sub by blast
    next
      assume level: "l \<le> l'"
      from level lhs_step rhs_step f_f'_equiv x_x'_equiv
      show ?thesis using se_equiv_cbvss_hide_preserves_se_equiv se_equiv.app by smt
    qed
  qed
qed
qed

lemma cbvss_noninterference:
  fixes \<Gamma> e\<^sub>1 e\<^sub>1' e\<^sub>2 e\<^sub>2' st l
  assumes equiv: "\<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1 =(l) e\<^sub>2: st"
  assumes step1: "e\<^sub>1 \<leadsto>\<^sub>v e\<^sub>1'"
  assumes step2: "e\<^sub>2 \<leadsto>\<^sub>v e\<^sub>2'"
  shows "\<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1' =(l) e\<^sub>2': st"
proof -
from equiv step1 step2 show "\<Gamma> \<turnstile>\<^sub>s\<^sub>e e\<^sub>1' =(l) e\<^sub>2': st"
proof (induct arbitrary: e\<^sub>1' e\<^sub>2' set: se_equiv)
next case nat
  from this show ?case
  using Expr.distinct(1) Expr.distinct(5) cbvss.cases by fastforce
next case var
  from this show ?case
  using Expr.distinct(1) Expr.distinct(5) cbvss.cases by fastforce
next case (plus \<Gamma> x l x' l' y y')
  show ?case
  apply (rule cbvss_noninterference_nat)
  using plus.hyps(1) apply simp
  using plus.hyps(3) apply simp
  using plus.hyps apply simp+
  using plus.prems apply simp+
  done
next case (app \<Gamma> f l f' s t l' x x')
  show ?case
  apply (rule cbvss_noninterference_app)
  using app.hyps app.prems apply simp+
  done
next case (abs s \<Gamma> f l f' t)
  assume f_step: "\<lambda>. f \<leadsto>\<^sub>v e\<^sub>1'"
  from f_step cbvss_abs_no_step show ?case by fastforce
next case (sub \<Gamma> e1 l e2 st st')
  from sub.prems sub.hyps show ?case
  using se_equiv.sub by blast
next case (hide \<Gamma> e1 t l' e2 l)
  from hide.prems hide.hyps show ?case
  by (simp add: cbvss_preserves_se_typ se_equiv.hide)
next case (label \<Gamma> e1 l e2 t l' l'')
  from label.prems label.hyps show ?case
  using cbvss.cases by fastforce
qed
qed

(*>*)
text_raw \<open>@{example SlcNoninterference}\<close>
text \<open>
  @{thm [mode=Rule] cbvss_noninterference} {\textsc{lambdaNoninterference}}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

end
(*>*)