theory LambdaTests
imports Main "Nominal/Nominal2"
begin


datatype Lit = 
  Nat nat
| Bool bool
| NatOp "nat \<Rightarrow> nat \<Rightarrow> nat"
| BoolOp "bool \<Rightarrow> bool \<Rightarrow> bool"

atom_decl name

instantiation Lit :: pt
begin  
  definition "p \<bullet> (l::Lit) = l"
  
  instance
  apply standard
  using permute_Lit_def by simp+
end

instance Lit :: fs
apply standard
unfolding supp_def using permute_Lit_def by simp

lemma lit_supp_nil[simp]: "(supp (l :: Lit)) = {}"
unfolding supp_def using permute_Lit_def
by (simp add: not_finite_existsD)

nominal_datatype Expr =
  Var name ("`_`")
| Lit "Lit"
| App f::"Expr" x::"Expr" ("_ of _" [70, 71] 70)
| Abs x::"name" fx::"Expr" binds x in fx ("\<lambda>[_]. _" [60, 60] 60)
print_theorems

lemma "\<lambda>[x]. \<lambda>[y]. `y` of `x` = \<lambda>[x]. (\<lambda>[y]. (`y` of `x`))"
by simp

lemma "atom x \<sharp> \<lambda>[x]. `x`"
by simp

lemma "x \<noteq> y \<Longrightarrow> \<not> atom y \<sharp> \<lambda>[x]. `x` of `y`"
apply simp
using fresh_atom_at_base fresh_ineq_at_base fresh_at_base by blast

nominal_function subst :: "Expr \<Rightarrow> name \<Rightarrow> Expr \<Rightarrow> Expr" ("_[_ \<mapsto> _]") where
  "(Var x)[y \<mapsto> e] = (if x = y then e else Var x)"
| "(Lit l)[_ \<mapsto> _] = Lit l"
| "(f of x)[y \<mapsto> e] = (f[y \<mapsto> e]) of (x[y \<mapsto> e])"
| "atom x \<sharp> (y, e) \<Longrightarrow> (\<lambda>[x]. fx)[y \<mapsto> e] = \<lambda>[x]. (fx[y \<mapsto> e])"
using eqvt_def subst_graph_aux_def apply force
using subst_graph_def apply blast
apply (clarsimp split: prod.splits)
apply (rename_tac P expr y e)
thm Expr.strong_exhaust
apply (rule_tac y=expr and c="(y, e)" in Expr.strong_exhaust)
unfolding fresh_star_def
apply blast+
apply simp+
apply clarsimp
apply (subgoal_tac "(x \<leftrightarrow> c) \<bullet> fx = (xa \<leftrightarrow> c) \<bullet> fxa") defer
  apply simp
unfolding eqvt_at_def
using flip_fresh_fresh fresh_Pair eqvt_apply Pair_eqvt
by smt
nominal_termination by lexicographic_order
print_theorems

lemma ffs: "distinct [x, y, z] \<Longrightarrow> \<lambda>[x]. `y` of `x` = \<lambda>[z]. `y` of `z`"
by (simp, force)

lemma  "distinct [x, y, z] \<Longrightarrow> (\<lambda>[x]. `y` of `x`)[y \<mapsto> `x`] = \<lambda>[z]. `x` of `z`"
apply (subst ffs[where z=z], simp)
apply (subgoal_tac "atom z \<sharp> (y, `x`)")
  apply simp
by auto

datatype PrimType = Nat | Bool

datatype Type = 
  Prim PrimType
| Fun Type Type ("_ \<rightarrow> _" [70, 71] 70)

inductive \<beta>_step :: "Expr \<Rightarrow> Expr \<Rightarrow> bool" ("_ \<rightarrow>\<^sub>\<beta> _") where
  "(Var x) \<rightarrow>\<^sub>\<beta> (Var x)"
| "(Lit l) \<rightarrow>\<^sub>\<beta> (Lit l)"
| lhs: "lhs \<rightarrow>\<^sub>\<beta> lhs' \<Longrightarrow> lhs of rhs \<rightarrow>\<^sub>\<beta> lhs' of rhs"
| rhs: "rhs \<rightarrow>\<^sub>\<beta> rhs' \<Longrightarrow> lhs of rhs \<rightarrow>\<^sub>\<beta> lhs of rhs'"
| lam: "fx \<rightarrow>\<^sub>\<beta> fx' \<Longrightarrow> \<lambda>[x]. fx \<rightarrow>\<^sub>\<beta> \<lambda>[x]. fx'"
| lam_app: "atom x \<sharp> y \<Longrightarrow> (\<lambda>[x]. fx) of y \<rightarrow>\<^sub>\<beta> fx[x \<mapsto> y]"

lemma subst: "atom x \<sharp> y \<Longrightarrow> atom x \<sharp> subst fx x y"
apply (nominal_induct fx avoiding: x rule: Expr.strong_induct)
apply clarsimp+
using lit_supp_nil apply (simp add: fresh_def)
apply simp
sledgehammer

nominal_inductive \<beta>_step
  avoids lam_app: "x"
apply safe
unfolding fresh_star_def apply (clarsimp simp: fresh_Pair)

end