theory ListUtils
imports 
  Main
begin

fun insert_at :: "'a list \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> 'a list" where
  "insert_at xs 0 x = x # xs"
| "insert_at (x' # xs) (Suc n) x = x' # insert_at xs n x"
| "insert_at [] n x = [x]"

primrec nth' :: "'a list \<Rightarrow> nat \<Rightarrow> 'a option" (infixl "`!" 100) where
  "(x # xs) `! n = (case n of 0 \<Rightarrow> Some x | Suc n \<Rightarrow> xs `! n)"
| "[] `! n = None"

lemma nth'_some_le[intro!]: "xs `! i = Some x \<Longrightarrow> i < length xs"
apply (induct i arbitrary: xs x)
apply simp
by (case_tac xs, simp+)+

lemma nth'_none_ge[intro!]: "xs `! i = None \<Longrightarrow> i \<ge> length xs"
apply (induct i arbitrary: xs)
using list.exhaust list.size(3) apply force
apply (case_tac xs, simp+)
done

lemma nth'_ge_none[intro!]: "i \<ge> length xs \<Longrightarrow> xs `! i = None"
apply (induct i arbitrary: xs, simp)
by (case_tac xs, simp+)

lemma nth'_le_some[intro!]: "i < length xs \<Longrightarrow> xs `! i = Some (xs ! i)"
apply (induct i arbitrary: xs)
apply simp
using list.exhaust apply force
apply (case_tac xs)
by simp+

lemma nth'_take_le[simp]: "i < n \<Longrightarrow> take n xs `! i = xs `! i"
apply (case_tac "n < length xs")
using nth_take length_take nth'_le_some
apply (metis (no_types, lifting) min.commute min.strict_order_iff order.strict_trans)
using take_all by simp

lemma nth'_drop[simp]: "drop n xs `! i = xs `! (n + i)"
apply (induct n arbitrary: i xs)
  apply simp
by (case_tac xs, simp+)

lemma insert_at_append: "
  (insert_at xs n x) = (take n xs) @ [x] @ (drop n xs)"
apply (induct xs arbitrary: n x)
apply simp
by (case_tac n, simp+)+

lemma append_index: "(xs @ xs') `! i = (if i < length xs then xs `! i else xs' `! (i - length xs))"
apply (induct xs arbitrary: i)
apply simp
apply (case_tac i)
by simp+

lemma insert_at_shift_nth: "
  i \<le> length xs \<Longrightarrow>
  n \<le> length xs \<Longrightarrow>
  (insert_at xs n x) `! i = (if i < n then xs `! i else if i = n then Some x else xs `! (i - 1))"
apply (subst insert_at_append)
apply (subst append_index)
apply (subst length_take)
apply (clarsimp)
by (simp add: Nitpick.case_nat_unfold min_def)

lemma insert_at_cons: "
  x # insert_at xs n x' = insert_at (x # xs) (Suc n) x'"
by (induct xs, simp+)

lemma insert_at_le[intro!]: "
  i < n \<Longrightarrow>
  n \<le> length xs \<Longrightarrow>
  (insert_at xs n x) `! i = xs `! i"
by (simp add: insert_at_shift_nth)

lemma insert_at_eq[intro!]: "
  i = n \<Longrightarrow>
  n \<le> length xs \<Longrightarrow>
  (insert_at xs n x) `! i = Some x"
by (simp add: insert_at_shift_nth)

lemma insert_at_ge[intro!]: "
  n < i \<Longrightarrow>
  i \<le> length xs \<Longrightarrow>
  (insert_at xs n x) `! i = xs `! (i - 1)"
by (simp add: insert_at_shift_nth)

lemma insert_at_le'[intro!]: "
  k \<le> length xs \<Longrightarrow>
  insert_at xs k x `! n = Some x' \<Longrightarrow>
  k < n \<Longrightarrow>
  xs `! (n - 1) = Some x'"
by (metis add_Suc_right append_Cons append_Nil append_take_drop_id insert_at_append insert_at_ge length_Cons length_append not_le not_less_eq_eq nth'_some_le)

lemma insert_at_eq'[intro!]: "
  k \<le> length xs \<Longrightarrow>
  insert_at xs k x `! n = Some x' \<Longrightarrow>
  k > n \<Longrightarrow>
  xs `! n = Some x'"
by (simp add: insert_at_le)

lemma insert_at_length[intro!, simp]: "
  length (insert_at xs n x) = Suc (length xs)"
apply (induct xs, simp)
apply (simp add: insert_at_append)
by (metis (no_types, lifting) Suc_eq_plus1_left add.left_commute append_take_drop_id insert_at_append length_Cons length_append)

lemma nth'_lift_map[intro!]: "xs `! n = Some x \<Longrightarrow> (map f xs) `! n = Some (f x)"
apply (induct xs arbitrary: f n x, simp)
by (metis length_map nth'_le_some nth'_some_le nth_map option.inject)

end