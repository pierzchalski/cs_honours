theory Parsing
imports Main "./IFSTM.thy"
begin

ML {*
  val s = "Abs (Abs (Plus (Var 0) (Var 0)))"
  val IamInIsabelle_not_really = Syntax.read_term @{context} s;
  *}
  ML{*
  val isabelle_term_in_ml_world = @{term "Abs (Abs (Plus (Var 0) (Var 0)))"} : term;
  (* This is an ML value that expresses an Isabelle term. It is still in the ML world.*)
  *}ML{*
  Local_Theory.notes;
  Local_Theory.define (*read The Isabelle/Isar Implementation Manual page 136*)  
*}

local_setup{*
(* You need to write a function here.
   The type of this function has to be "local_theory \<Rightarrow> local_theory".
   What is local_theory? It is similar to Proof.context but was developed later.
   In Isabelle you have multiple "context":
    Proof.context, theory, local_theory, general_something.*)
*}

definition x :: Expr where
  "x \<equiv> "

ML{* Parse_Spec.constdef;Parse.prop; Parse.inner_syntax; --; 
Scan.ahead;
Token.inner_syntax_of;
Proof.theorem*}
definition "foo \<equiv> (\<lambda> x . x)"

function my_magic_functon :: " string \<Rightarrow> Expr"

my_magic_function "\<lambda>x. \<lambda>x. x + x" = "Abs (Abs (Plus (V 0) (V 0)))" 


end