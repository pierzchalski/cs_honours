(*<*)
theory SimpleIO
imports 
  Main 
  ListUtils
  Examples
  "~~/src/HOL/Library/LaTeXsugar"
  "~~/src/HOL/Library/OptionalSugar"
  (*"../PSL-master/PSL"*)
begin

datatype secLevel = DUMMY

datatype 'l type =
  Int
| Bool
| Unit
| Fn "'l type" "'l type" ("_ \<Rightarrow> _" [61, 60] 60)
| Stm 'l 'l "'l type" ("Stm '(R: _, W: _') _")
| StmTVar 'l "'l type"
| IO 'l "'l type"

notation (latex output) Int ("Int")
notation (latex output) Bool ("Bool")
notation (latex output) Unit ("Unit")
notation (latex output) StmTVar ("TVar")
notation (latex output) IO ("LIO")

(*>*)
text_raw \<open>@{example StmTypes}\<close>
text \<open>
  %\begin{spreadlines}{-1.5\baselineskip}
  \begin{align*}
  @{term l} & :: @{type secLevel} \\
  @{term t} & :: \textit{type}\ @{text "\<equiv>"} \\
    &\ \hphantom{|}\ @{term "type.Int"} \\
    &\ |\ @{term "type.Bool"} \\
    &\ |\ @{term "type.Unit"} \\
    &\ |\ @{term "t\<^sub>1 \<Rightarrow> t\<^sub>2"} \\
    &\ |\ @{term "StmTVar l t"} \\
    &\ |\ @{term "Stm (R: l\<^sub>1, W: l\<^sub>2) t"} \\
    &\ |\ @{term "IO l t"}
  \end{align*}
  %\end{spreadlines}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

datatype 'l atom =
  Unit
| App
| Pair
| IntLit int
| IntOp "int \<Rightarrow> int \<Rightarrow> int"
| IntComp "int \<Rightarrow> int \<Rightarrow> bool"
| BoolLit bool
| If
| StmTVar nat 'l "'l type"
| StmNewTVar 'l "'l type"
| StmReturn
| StmBind
| StmReadTVar
| StmWriteTVar
| StmRetry
| StmOrElse
| LioAtomically
| LioReturn
| LioBind

notation (latex output) Unit ("Unit")
notation (latex output) Pair ("Pair")
notation (latex output) If ("If")
notation (latex output) StmTVar ("StmTVar")

(*>*)
text_raw \<open>@{example StmAtoms}\<close>
text \<open>
  %\begin{spreadlines}{-1.5\baselineskip}
  \begin{align*}
  @{term n} & :: \mathbb{N} \\
  @{term i} & :: \mathbb{Z} \\
  @{term b} & :: \mathbb{B} \\
  @{term intOp} & :: \mathbb{Z} \Rightarrow \mathbb{Z} \Rightarrow \mathbb{Z} \\
  @{term intComp} & :: \mathbb{Z} \Rightarrow \mathbb{Z} \Rightarrow \mathbb{B} \\
  @{term l} & :: @{type secLevel} \\
  @{term t} & :: \textit{type} \\
  @{term a} & :: @{type atom}\ @{text "\<equiv>"} \\
    &\ \hphantom{|}\ @{term "Unit"} 
     \ |\ @{term "App"} 
     \ |\ @{term "Pair"} 
     \ |\ @{term "If"} \\
    &\ |\ @{term "IntLit i"}
     \ |\ @{term "BoolLit b"} \\
    &\ |\ @{term "IntOp intOp"}
     \ |\ @{term "IntComp intComp"} \\
    &\ |\ @{term "StmReturn"} 
     \ |\ @{term "StmBind"} \\
    &\ |\ @{term "StmTVar n l t"}
     \ |\ @{term "StmNewTVar l t"} \\
    &\ |\ @{term "StmReadTVar"}
     \ |\ @{term "StmWriteTVar"} \\
    &\ |\ @{term "StmRetry"}
     \ |\ @{term "StmOrElse"} \\
    &\ |\ @{term "LioAtomically"}
     \ |\ @{term "LioReturn"}
     \ |\ @{term "LioBind"}
  \end{align*}
  %\end{spreadlines}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

datatype 'l expr = 
  Var nat ("V _" [101])
| Abs "'l expr" ("\<lambda>. _" [40] 40)
| Leaf
| Node "'l atom" "'l expr" "'l expr"

(*>*)
text_raw \<open>@{example StmExpr}\<close>
text \<open>
  %\begin{spreadlines}{-1.5\baselineskip}
  \begin{align*}
  @{term n} & :: \mathbb{N} & \\
  @{term a} & :: @{type atom} \\
  @{term e} & :: @{type expr}\ @{text "\<equiv>"} \\
    &\ \hphantom{@{text "|"}}\ @{term "Var n"} \\
    &\ @{text "|"}\ @{term "\<lambda>. e"} \\
    &\ @{text "|"}\ @{term "Leaf"} \\
    &\ @{text "|"}\ @{term "Node a e\<^sub>1 e\<^sub>2"}
  \end{align*}
  %\end{spreadlines}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

abbreviation app ("_ \<circ> _" [50, 51] 50) where
  "app f x \<equiv> Node App f x"
abbreviation "if' b l r \<equiv> Node If b (Node Pair l r)" text \<open>\\\<close>

(*>*)
text_raw \<open>@{example StmAbbreviations}\<close>
text \<open>\begin{gather*}\<close>
abbreviation "unit \<equiv> Node Unit Leaf Leaf" text \<open>\\\<close>
text \<open>\textbf{\textrm{abbreviation}}\ f \circ x \equiv \textit{Node}\ \textit{App}\ f\ x \\\<close>
text \<open>\textbf{\textrm{abbreviation}}\ \textit{if}\ b\ t\ f \equiv \textit{Node}\ \textit{If}\ b\ (\textit{Node}\ \textit{Pair}\ t\ f) \\\<close>
abbreviation "I i \<equiv> Node (IntLit i) Leaf Leaf" text \<open>\\\<close>
abbreviation "B i \<equiv> Node (BoolLit i) Leaf Leaf" text \<open>\\\<close>
abbreviation "intOp f i\<^sub>1 i\<^sub>2 \<equiv> Node (IntOp f) i\<^sub>1 i\<^sub>2" text \<open>\\\<close>
abbreviation "intComp f i\<^sub>1 i\<^sub>2 \<equiv> Node (IntComp f) i\<^sub>1 i\<^sub>2"  text \<open>\\\<close>
abbreviation "return\<^sub>S\<^sub>T\<^sub>M v \<equiv> Node StmReturn v Leaf" text \<open>\\\<close>
abbreviation "bind\<^sub>S\<^sub>T\<^sub>M m n \<equiv> Node StmBind m n" text \<open>\\\<close>
abbreviation "TVar n l t \<equiv> Node (StmTVar n l t) Leaf Leaf" text \<open>\\\<close>
abbreviation "newTVar l t v \<equiv> Node (StmNewTVar l t) v Leaf" text \<open>\\\<close>
abbreviation "readTVar v \<equiv> Node StmReadTVar v Leaf" text \<open>\\\<close>
abbreviation "writeTVar v x \<equiv> Node StmWriteTVar v x" text \<open>\\\<close>
abbreviation "retry \<equiv> Node StmRetry Leaf Leaf" text \<open>\\\<close>
abbreviation "orElse l r \<equiv> Node StmOrElse l r" text \<open>\\\<close>
abbreviation "return\<^sub>L\<^sub>I\<^sub>O v \<equiv> Node LioReturn v Leaf" text \<open>\\\<close>
abbreviation "bind\<^sub>L\<^sub>I\<^sub>O m n \<equiv> Node LioBind m n" text \<open>\\\<close>
abbreviation "atomically v \<equiv> Node LioAtomically v Leaf"
text \<open>\end{gather*}\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

notation (latex output) if' ("if (_) (_) (_)")
  
(* 
  "lift k e" leaves each variable fewer than k abstractions deep alone, but 
  bumps up each variable more than k levels deep by 1.
  
  Used for substituting a term with bound variables, for when the term is substituted
  "past" an abstraction.
  
  In "lift k", imagine "k" is a "depth" parameter. In the base case, k=0 and we bump up
  all variables. When we enter a lambda term, we want to bump up all terms other than
  the new, bound term, so k=1. If we encounter another lambda term, there are two bound variables
  (V 0 and V 1), and again every other term we want to bump up.
*)
fun lift :: "nat \<Rightarrow> 'l expr \<Rightarrow> 'l expr" where
  "lift k (Var x) = (if x < k then Var x else Var (x + 1))"
| "lift k (Abs f) = Abs (lift (k + 1) f)"
| "lift k Leaf = Leaf"
| "lift k (Node atom left right) = Node atom (lift k left) (lift k right)"

(*>*)
text_raw \<open>@{example StmLiftDef}\<close>
text \<open>
  \begin{align*}
  @{term "lift k (Var n)"} &= @{term "if n < k then Var n else Var (n + 1)"} \\
  @{term "lift k (Abs f)"} &= @{term "Abs (lift (k + 1) f)"} \\
  @{term "lift k (Leaf)"} &= @{term "Leaf"} \\
  @{term "lift k (Node a l r)"} &= @{term "Node a (lift k l) (lift k r)"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)


fun subst :: "'l expr \<Rightarrow> nat \<Rightarrow> 'l expr \<Rightarrow> 'l expr" ("_[_ \<mapsto> _]") where
  "subst (Var n) k e = (if k = n then e else if n > k then Var (n - 1) else  Var n)"
| "subst (Abs f) k e = Abs (subst f (k + 1) (lift 0 e))"
| "subst Leaf k e = Leaf"
| "subst (Node atom left right) k e = Node atom (subst left k e) (subst right k e)" 

(*>*)
text_raw \<open>@{example StmSubstDef}\<close>
text \<open>
  \begin{align*}
  @{term "(Var n)[k \<mapsto> e]"} &= @{term "if n < k then Var n else Var (n + 1)"} \\
  @{term "(Abs f)[k \<mapsto> e]"} &= @{term "Abs ((f :: 'l expr)[k + 1 \<mapsto> lift 0 e])"} \\
  @{term "(Leaf)[k \<mapsto> e]"} &= @{term "Leaf"} \\
  @{term "(Node a l r)[k \<mapsto> e]"} &= @{term "Node a ((l :: 'l expr)[k \<mapsto> e]) ((r :: 'l expr)[k \<mapsto> e])"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

type_synonym 'l threads = "'l expr list"
(* 
  an STM shared heap, using labels and types as keys, mapping to a list which will be indexed by
  the contents of the relevant TVar
*)
type_synonym 'l stm = "('l * 'l type) \<Rightarrow> 'l expr list"
type_synonym ('l, 's) env = "'l threads * 'l stm * 's"

fun val :: "'l expr \<Rightarrow> bool" where
  "val (Abs f) = True"
| "val (Var n) = False"
| "val Leaf = True"
| "val (Node atom l r) = (case atom of
      App \<Rightarrow> False
    | IntOp f \<Rightarrow> False
    | IntComp f \<Rightarrow> False
    | If \<Rightarrow> False
    | other \<Rightarrow> val l \<and> val r)"
    
(*>*)
text_raw \<open>@{example StmValDef}\<close>
text \<open>
  \begin{align*}
  @{term "val (Leaf)"} &= @{term "True"} \\
  @{term "val (Abs e)"} &= @{term "False"} \\
  @{term "val (Var e)"} &= @{term "False"} \\
  @{term "val (Node App l r)"} &= @{term "False"} \\
  @{term "val (Node (IntOp f) l r)"} &= @{term "False"} \\
  @{term "val (Node (IntComp f) l r)"} &= @{term "False"} \\
  @{term "val (Node If l r)"} &= @{term "False"} \\
  @{term "val (Node other l r)"} &= @{term "val l \<and> val r"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
    
locale scheduler =
  fixes sched_step :: "'s \<Rightarrow> ('l :: bounded_lattice) expr list \<Rightarrow> (nat * 's)"
  assumes s_step_range: "length xs > 0 \<Longrightarrow> fst (sched_step s xs) < length xs"

context scheduler
begin

inductive simple_eval :: "'l expr \<Rightarrow> 'l expr \<Rightarrow> bool" ("_ \<leadsto> _") where
  app: "(\<lambda>. f) \<circ> x \<leadsto> f[0 \<mapsto> x]"
| intOp: "intOp f (I i\<^sub>1) (I i\<^sub>2) \<leadsto> I (f i\<^sub>1 i\<^sub>2)"
| intComp: "intComp f (I i\<^sub>1) (I i\<^sub>2) \<leadsto> B (f i\<^sub>1 i\<^sub>2)"
| if': "if' (B b) l r \<leadsto> if b then l else r"

(*>*)
text_raw \<open>@{example StmSimpleEval}\<close>
text \<open>
  @{thm [mode=Axiom] simple_eval.app} {\textsc{simpleEvalApp}} \\[1ex]
  @{thm [mode=Axiom] simple_eval.intOp} {\textsc{simpleEvalIntOp}} \\[1ex]
  @{thm [mode=Axiom] simple_eval.intComp} {\textsc{simpleEvalIntComp}} \\[1ex]
  @{thm [mode=Axiom] simple_eval.if'} {\textsc{simpleEvalIf}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

inductive_set simple :: "'l expr rel" and
  simple_abv :: "'l expr \<Rightarrow> 'l expr \<Rightarrow> bool" ("_ \<rightarrow> _")
where
  "a \<rightarrow> b \<equiv> (a, b) \<in> simple"
| left[intro]: "
    l \<rightarrow> l' \<Longrightarrow>
    Node atom l r \<rightarrow> Node atom l' r"
| right[intro]: "
    val l \<Longrightarrow>
    r \<rightarrow> r' \<Longrightarrow>
    Node atom l r \<rightarrow> Node atom l r'"
| eval[intro]: "
    val l \<Longrightarrow>
    val r \<Longrightarrow>
    (Node atom l r) \<leadsto> y \<Longrightarrow>
    Node atom l r \<rightarrow> y"

(*>*)
text_raw \<open>@{example StmSimpleStep}\<close>
text \<open>
  @{thm [mode=Rule] simple.left} {\textsc{simpleStepLeft}} \\[1ex]
  @{thm [mode=Rule] simple.right} {\textsc{simpleStepRight}} \\[1ex]
  @{thm [mode=Rule] simple.eval} {\textsc{simpleStepEval}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

abbreviation simple_star ("_ \<rightarrow>^* _") where
  "x \<rightarrow>^*y \<equiv> (x, y) \<in> simple^*"
 
notation (latex output) simple_star ("(_) \<^raw:\rightarrow^\star> (_)")
    
inductive_cases simple_app: "(Abs f) \<circ> x \<rightarrow> y"
inductive_cases simple_node: " Node atom l r \<rightarrow> y'"
    
lemma val_no_simple: "
  x \<rightarrow> y \<Longrightarrow>
  val x \<Longrightarrow>
  False"
apply (induct arbitrary: y rule: simple.induct)
apply simp
apply (case_tac atom, simp_all)
apply (case_tac atom, simp_all)
apply (erule simple_eval.cases, simp_all)
done

lemma val_no_simple': " 
  val x \<Longrightarrow>
  x \<rightarrow> y \<Longrightarrow>
  False"
using val_no_simple by auto

lemma simple_eval_no_val: "
  x \<leadsto> y \<Longrightarrow>
  \<not>val x"
apply (erule simple_eval.cases, simp_all)
done

lemma simple_eval_deterministic: "
  x \<leadsto> a \<Longrightarrow>
  x \<leadsto> b \<Longrightarrow>
  a = b"
apply (erule simple_eval.cases, simp_all)+
done

lemma simple_deterministic: " 
  x \<rightarrow> a \<Longrightarrow>
  x \<rightarrow> b \<Longrightarrow>
  a = b"
apply (induct arbitrary: b rule: simple.induct)
apply (erule simple_node)
apply simp
using val_no_simple apply fast+
apply (erule simple_node)
using val_no_simple apply fast+
apply (erule simple_node)
using val_no_simple apply fast
using val_no_simple apply fast
apply simp
using simple_eval_deterministic apply simp
done

lemma simple_example_0: "
  (\<lambda>. \<lambda>. (intOp (op -) (V 0) (V 1))) \<circ> (I 3) \<circ> (I 2) \<rightarrow>^* I (-1)"
apply ((
  rule converse_rtrancl_into_rtrancl,
  rule left,
  rule eval, simp_all,
  rule simple_eval.intros, simp_all
) | (
  rule converse_rtrancl_into_rtrancl,
  rule eval, simp_all,
  rule simple_eval.intros, simp_all
))+
done

lemma simple_example_1: "
  let f = (\<lambda>. \<lambda>. (intOp (op -) (V 1) (app (V 0) (I 5)))) ;
  g = (\<lambda>. (intOp (op *) (I (-1)) (V 0))) in
  (f \<circ> (I 4) \<circ> g \<rightarrow>^* I (9))"
apply clarsimp
apply ((
  rule converse_rtrancl_into_rtrancl,
  rule left,
  rule eval, simp_all,
  rule simple_eval.intros, simp_all)
| (
  rule converse_rtrancl_into_rtrancl,
  rule eval, simp_all,
  rule simple_eval.intros, simp_all)
| (
  rule converse_rtrancl_into_rtrancl,
  rule right, simp_all,
  rule eval, simp_all,
  rule simple_eval.intros, simp_all))+
done

inductive simple_sub :: "
  'l type \<Rightarrow>
  'l type \<Rightarrow>
  bool" ("_ <: _")
where
  stm: "
    r' \<le> r \<Longrightarrow>
    w \<le> w' \<Longrightarrow>
    Stm (R: r, W: w) t <: Stm (R: r', W: w') t"

(* \<Sigma>, \<Gamma> \<turnstile>\<^sub>S e: t, \<Sigma>' means "with variable bindings of type \<Gamma>, and an STM heap of type \<Sigma>,
 the expression e has type t, and results in an STM heap of type \<Sigma>'.*)
inductive simple_type :: "
  'l type list \<Rightarrow> 
  'l expr \<Rightarrow> 
  'l type \<Rightarrow> 
  bool" 
  ("_ \<turnstile>\<^sub>S _: _")
where
  var: "
    \<Gamma> `! n = Some t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S V n: t"
| abs: "
    s # \<Gamma> \<turnstile>\<^sub>S f: t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S \<lambda>.f : s \<Rightarrow> t"
| app: "
    \<Gamma> \<turnstile>\<^sub>S x: s \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S f: s \<Rightarrow> t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S f \<circ> x: t"
| intLit: "\<Gamma> \<turnstile>\<^sub>S I i: type.Int"
| boolLit: "\<Gamma> \<turnstile>\<^sub>S B b: type.Bool"
| if': "
    \<Gamma> \<turnstile>\<^sub>S b: type.Bool \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S l: s \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S r: s \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S if' b l r: s"
| intOp: "
    \<Gamma> \<turnstile>\<^sub>S l: type.Int \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S r: type.Int \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S intOp f l r: type.Int"
| intComp: "
    \<Gamma> \<turnstile>\<^sub>S l: type.Int \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S r: type.Int \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S intComp f l r: type.Bool"
| stmNewTVar: "
    \<Gamma> \<turnstile>\<^sub>S v: t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S newTVar l t v: Stm (R: bot, W: top) (type.StmTVar l t)"
| stmReturn: "
    \<Gamma> \<turnstile>\<^sub>S v: t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S return\<^sub>S\<^sub>T\<^sub>M v: Stm (R: bot, W: top) t"
| stmRetry: "
    \<Gamma> \<turnstile>\<^sub>S retry: Stm (R: bot, W: top) t"
| stmBind: "
    \<Gamma> \<turnstile>\<^sub>S m: Stm (R: r, W: w) s \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S f: s \<Rightarrow> Stm (R: r, W: w) t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S bind\<^sub>S\<^sub>T\<^sub>M m f: Stm (R: r, W: w) t"
| stmReadTVar: "
    \<Gamma> \<turnstile>\<^sub>S v: type.StmTVar l t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S readTVar v: Stm (R: l, W: top) t"
| stmWriteTVar: "
    \<Gamma> \<turnstile>\<^sub>S v: type.StmTVar l t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S x: t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S writeTVar v x: Stm (R: bot, W: l) type.Unit"
| stmOrElse: "
    \<Gamma> \<turnstile>\<^sub>S l: Stm (R: rd, W: wr) t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S r: Stm (R: rd, W: wr) t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S orElse l r: Stm (R: rd, W: wr) t"
| stmTVar: "
    \<Gamma> \<turnstile>\<^sub>S TVar n l t: type.StmTVar l t"
| stmSub: "
    rd \<le> rd' \<Longrightarrow>
    wr' \<le> wr \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S e: Stm (R: rd, W: wr) t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S e: Stm (R: rd', W: wr') t"
| atomically: "
    rd \<le> l \<Longrightarrow>
    l \<le> wr \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S e: Stm (R: rd, W: wr) t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S atomically e: IO l t"
| lioBind: "
    \<Gamma> \<turnstile>\<^sub>S m: IO l s \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S n: s \<Rightarrow> IO l t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S bind\<^sub>L\<^sub>I\<^sub>O m n: IO l t"
| lioReturn: "
    \<Gamma> \<turnstile>\<^sub>S v: t \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S return\<^sub>L\<^sub>I\<^sub>O v: IO bot t"
(* 
  Say a thread is "targeting" security level l'. Then if that thread runs an expression of type
  IO l t, with l \<le> l', then clearly the thread can upgrade the term to its own level.
  We rely on preservation and a top-level constraint on the thread pool to encode our security
  guarantees
*)
| lioSub: "
    \<Gamma> \<turnstile>\<^sub>S e: IO l t \<Longrightarrow>
    l' \<ge> l \<Longrightarrow>
    \<Gamma> \<turnstile>\<^sub>S e: IO l' t"
    
(*>*)
text_raw \<open>@{example StmSimpleType}\<close>
text \<open>
  @{thm [mode=Rule] simple_type.var} {\textsc{typeVar}} \\[1ex]
  @{thm [mode=Rule] simple_type.abs} {\textsc{typeAbs}} \\[1ex]
  @{thm [mode=Rule] simple_type.app} {\textsc{typeApp}} \\[1ex]
  @{thm [mode=Axiom] simple_type.intLit} {\textsc{typeIntLit}} \\[1ex]
  @{thm [mode=Axiom] simple_type.boolLit} {\textsc{typeBoolLit}} \\[1ex]
  @{thm [mode=Rule] simple_type.if'} {\textsc{typeIf}} \\[1ex]
  @{thm [mode=Rule] simple_type.intOp} {\textsc{typeIntOp}} \\[1ex]
  @{thm [mode=Rule] simple_type.intComp} {\textsc{typeIntComp}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>

text_raw \<open>@{example StmStmType}\<close>
text \<open>
  @{thm [mode=Axiom] simple_type.stmTVar} {\textsc{typeTVar}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmNewTVar} {\textsc{typeNewTVar}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmReturn} {\textsc{typeStmReturn}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmBind} {\textsc{typeStmBind}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmReadTVar} {\textsc{typeReadTVar}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmWriteTVar} {\textsc{typeWriteTVar}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmOrElse} {\textsc{typeOrElse}} \\[1ex]
  @{thm [mode=Axiom] simple_type.stmRetry} {\textsc{typeRetry}} \\[1ex]
  @{thm [mode=Rule] simple_type.stmSub} {\textsc{typeStmSub}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>

text_raw \<open>@{example StmLioType}\<close>
text \<open>
  @{thm [mode=Rule] simple_type.atomically} {\textsc{typeAtomically}} \\[1ex]
  @{thm [mode=Rule] simple_type.lioReturn} {\textsc{typeLioReturn}} \\[1ex]
  @{thm [mode=Rule] simple_type.lioBind} {\textsc{typeLioBind}} \\[1ex]
  @{thm [mode=Rule] simple_type.lioSub} {\textsc{typeLioSub}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
    
inductive_cases simple_type_app_abs: "\<Gamma> \<turnstile>\<^sub>S (\<lambda>. f) \<circ> x: t"
inductive_cases simple_type_abs: "\<Gamma> \<turnstile>\<^sub>S \<lambda>. f: t"
inductive_cases simple_type_newTVar: "\<Gamma> \<turnstile>\<^sub>S newTVar l tt v: t"
    
lemma lift_preserves_simple_type: "
  \<Gamma> \<turnstile>\<^sub>S e: t \<Longrightarrow>
  k \<le> length \<Gamma> \<Longrightarrow>
  insert_at \<Gamma> k t' \<turnstile>\<^sub>S lift k e: t"
apply (induct arbitrary: k t' rule: simple_type.induct)
apply simp_all
apply (rule conjI)
apply (rule impI, rule simple_type.var, simp add: insert_at_le)
apply (rule impI, rule simple_type.var)
apply (metis Suc_leI Suc_le_lessD diff_Suc_1 insert_at_shift_nth lessI less_imp_le_nat nth'_some_le)
apply (metis Suc_leI abs insert_at_cons le_imp_less_Suc)
using simple_type.intros apply meson+
done

lemma subst_preserves_simple_type: "
  \<Gamma> \<turnstile>\<^sub>S a: t \<Longrightarrow>
  \<Gamma> = insert_at \<Gamma>' k t' \<Longrightarrow>
  k \<le> length \<Gamma>' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>S b: t' \<Longrightarrow>
  \<Gamma>' \<turnstile>\<^sub>S a[k \<mapsto> b]: t"
apply (induct arbitrary: \<Gamma>' k t' b rule: simple_type.induct)
apply simp_all
apply (rule conjI, rule impI, rule simple_type.var)
apply (metis One_nat_def insert_at_le')
apply (rule impI, rule conjI, rule impI, simp add: insert_at_eq)
apply (rule impI, rule simple_type.var, simp add: insert_at_le)
apply (rule simple_type.abs)
subgoal premises asms
  apply (rule asms(2))
  apply simp
  using asms apply simp
  using asms lift_preserves_simple_type
  by (metis (full_types) insert_at.simps(1) le0)
apply (metis simple_type.intros | simp add:simple_type.stmRetry)+
done

(*>*)
text_raw \<open>@{example StmSubstPreservesSimpleType}\<close>
text \<open>
  @{thm [mode=Rule] subst_preserves_simple_type} {\textsc{substPreservesType}}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

lemma subst_preserves_simple_type': "
  s # \<Gamma> \<turnstile>\<^sub>S a: t \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>S b: s \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>S a[0 \<mapsto> b]: t"
using subst_preserves_simple_type by auto

lemma return_value_has_simple_type: "
  \<Gamma> \<turnstile>\<^sub>S e: s \<Longrightarrow>
  e = return\<^sub>S\<^sub>T\<^sub>M v \<Longrightarrow>
  s = Stm (R: r, W: w) t \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>S v: t"
apply (induct arbitrary: v r w t rule: simple_type.induct)
apply simp_all
done

lemma return_value_has_simple_type': "
  \<Gamma> \<turnstile>\<^sub>S return\<^sub>S\<^sub>T\<^sub>M v: Stm (R: r, W: w) t \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>S v: t"
using return_value_has_simple_type by blast

lemma simple_preserves_simple_type: "
  \<Gamma> \<turnstile>\<^sub>S a: t \<Longrightarrow>
  a \<rightarrow> b \<Longrightarrow> 
  \<Gamma> \<turnstile>\<^sub>S b: t"
apply (induct arbitrary: b rule: simple_type.induct)
using simple.cases apply blast
using simple.cases apply blast
apply (erule simple.cases, simp_all)
using simple_type.intros apply fastforce
using simple_type.intros apply fastforce
apply (erule simple_eval.cases, simp_all)
apply (erule simple_type_abs, simp_all)
using subst_preserves_simple_type' apply simp
using val_no_simple' apply force
using val_no_simple' apply force
apply (erule simple.cases, simp_all)
apply (fastforce intro: simple_type.intros)
subgoal
  apply (erule simple.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  apply (fastforce intro: simple_type.intros)
  apply (erule simple_eval.cases, simp_all)
  done
subgoal
  apply (erule simple_eval.cases, simp_all)
  done
subgoal
  apply (erule simple.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  apply (fastforce intro: simple_type.intros)
  apply (erule simple_eval.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  done
subgoal
  apply (erule simple.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  apply (fastforce intro: simple_type.intros)
  apply (erule simple_eval.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  done
subgoal
  apply (erule simple.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  using val_no_simple' apply force
  apply (erule simple_eval.cases, simp_all)
  done
subgoal
  apply (erule simple.cases, simp_all)
  apply (fastforce intro: simple_type.intros)
  using val_no_simple' apply force
  apply (erule simple_eval.cases, simp_all)
  done
subgoal
  apply (erule simple.cases, simp_all)
  using val_no_simple' apply force+
  done
apply (erule simple.cases, simp_all)
apply (fastforce intro: simple_type.intros)
apply (fastforce intro: simple_type.intros)
apply (erule simple_eval.cases, simp_all)
apply (erule simple.cases, simp_all)
apply (fastforce intro: simple_type.intros)
using val_no_simple' apply force
apply (erule simple_eval.cases, simp_all)
apply (erule simple.cases, simp_all)
apply (fastforce intro: simple_type.intros)
apply (fastforce intro: simple_type.intros)
apply (erule simple_eval.cases, simp_all)
apply (erule simple.cases, simp_all)
apply (fastforce intro: simple_type.intros)
apply (fastforce intro: simple_type.intros)
apply (erule simple_eval.cases, simp_all)
using val_no_simple' apply force
apply (fastforce intro: simple_type.intros)
apply (erule simple.cases, simp_all)
apply (simp add: atomically)
using val_no_simple' apply force
apply (erule simple_eval.cases, simp_all)
apply (erule simple.cases, simp_all)
using lioBind apply blast
using lioBind apply blast
apply (erule simple_eval.cases, simp_all)
apply (erule simple.cases, simp_all)
using lioReturn apply blast
using val_no_simple' apply force
apply (erule simple_eval.cases, simp_all)
using lioSub apply blast
done

(*>*)
text_raw \<open>@{example StmSimpleStepPreservesType}\<close>
text \<open>
  @{thm [mode=Rule] simple_preserves_simple_type} {\textsc{simpleStepPreservesType}}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

fun stmVal :: "'l expr \<Rightarrow> bool" where
  "stmVal (Node atom l r) = (case atom of
      StmReturn \<Rightarrow> True
    | StmRetry \<Rightarrow> True
    | other \<Rightarrow> False)"
| "stmVal other = False"
    
(* returns the value at TVar n l t, if it exists *)
fun getTVarMem :: "'l stm \<Rightarrow> nat \<Rightarrow> 'l \<Rightarrow> 'l type \<Rightarrow> 'l expr option" where
  "getTVarMem S n l t = S (l, t) `! n"
  
(*>*)
text_raw \<open>@{example StmGetTVarMemDef}\<close>
text \<open>
  \begin{align*}
  @{term "getTVarMem"} &:: \textit{stmHeap} \Rightarrow 
      \mathbb{N} \Rightarrow 
      \textit{secLevel} \Rightarrow 
      \textit{type} \Rightarrow
      \textit{expr}\ \textit{option} \\
  @{term "getTVarMem S n l t"} &= @{term "S (l, t) `! n"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
  
(* sets the value at TVar n l t, if it exists *)
fun setTVarMem :: "'l stm \<Rightarrow> nat \<Rightarrow> 'l \<Rightarrow> 'l type \<Rightarrow> 'l expr \<Rightarrow> 'l stm option" where
  "setTVarMem S n l t e = (
    let old = S (l, t) in
    if n < length old 
    then (Some (S((l, t) := old[n := e])))
    else None)"
    
(*>*)
text_raw \<open>@{example StmSetTVarMemDef}\<close>
text \<open>
  \begin{align*}
  @{term "setTVarMem"} &:: \textit{stmHeap} \Rightarrow 
      \mathbb{N} \Rightarrow 
      \textit{secLevel} \Rightarrow 
      \textit{type} \Rightarrow
      \textit{expr} \Rightarrow 
      \textit{stmHeap}\ \textit{option} \\
  @{term "setTVarMem S n l t e"} &= \textsf{let}\ @{term "old = S (l, t)"}\ \textsf{in}\\
      &\phantom{=} \textsf{if}\ @{term "n < length old" } \\ 
      &\phantom{=} \textsf{then}\  @{term "Some (S((l, t) := old[n := e]))"} \\
      &\phantom{=} \textsf{else}\ @{term None}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
 
(* adds a new slot to the sub-heap (l, t), returning the resulting memory and tvar *)
fun addTVarMem :: "'l stm \<Rightarrow> 'l \<Rightarrow> 'l type \<Rightarrow> 'l expr \<Rightarrow> 'l stm * 'l expr" where
  "addTVarMem S l t e = (
    let old = S (l, t) ;
       S' = S((l, t) := old @ [e]) ;
       tvar = TVar (length old) l t in
    (S', tvar))"
    
(*>*)
text_raw \<open>@{example StmAddTVarMemDef}\<close>
text \<open>
  \begin{align*}
  @{term "addTVarMem"} &:: 
      \textit{stmHeap} \Rightarrow 
      \textit{secLevel} \Rightarrow 
      \textit{type} \Rightarrow
      \textit{expr} \Rightarrow 
      (\textit{stmHeap}, \textit{expr}) \\
  @{term "addTVarMem S l t e"} &= \textsf{let}\ @{term "old = S (l, t)"}\ \textsf{in}\\
       &\phantom{=} \textsf{let}\ @{term "tvar = TVar (length old) l t"}\ \textsf{in}\\
       &\phantom{=} @{term "(S', tvar)"}
  \end{align*}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

inductive_set stm :: "('l stm * 'l expr) rel"
  and stm_abbrev ("_ \<rightarrow>\<^sub>S\<^sub>T\<^sub>M _")
  and stm_star ("_ \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* _")
where
  "x \<rightarrow>\<^sub>S\<^sub>T\<^sub>M y \<equiv> (x, y) \<in> stm"
| "x \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* y \<equiv> (x, y) \<in> stm^*"
| simple: "
    a \<rightarrow> b \<Longrightarrow> 
    (S, a) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S, b)"
| newTVar: "
    val v \<Longrightarrow>
    (S', tvar) = addTVarMem S l t v \<Longrightarrow>
    (S, newTVar l t v) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', return\<^sub>S\<^sub>T\<^sub>M tvar)"
| readTVar: "
    getTVarMem S n l t = Some v \<Longrightarrow> 
    (S, readTVar (TVar n l t)) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S, return\<^sub>S\<^sub>T\<^sub>M v)"
| writeTVar: "
    val v \<Longrightarrow>
    setTVarMem S n l t v = Some S' \<Longrightarrow>
    (S, writeTVar (TVar n l t) v) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', return\<^sub>S\<^sub>T\<^sub>M unit)"
| bindStep: "
    val m \<Longrightarrow>
    val f \<Longrightarrow>
    (S, m) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', m') \<Longrightarrow>
    (S, bind\<^sub>S\<^sub>T\<^sub>M m f) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', bind\<^sub>S\<^sub>T\<^sub>M m' f)"
| bindReturn: "
    val v \<Longrightarrow>
    val f \<Longrightarrow>
    (S, bind\<^sub>S\<^sub>T\<^sub>M (return\<^sub>S\<^sub>T\<^sub>M v) f) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S, f \<circ> v)"
| bindRetry: "
    val f \<Longrightarrow>
    (S, bind\<^sub>S\<^sub>T\<^sub>M retry f) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S, retry)"
| orElseReturn: "
    val l \<Longrightarrow>
    val r \<Longrightarrow>
    (S, l) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* (S', return\<^sub>S\<^sub>T\<^sub>M v) \<Longrightarrow>
    val v \<Longrightarrow>
    (S, orElse l r) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', return\<^sub>S\<^sub>T\<^sub>M v)"
| orElseRetry: "
    val l \<Longrightarrow>
    val r \<Longrightarrow>
    (S, l) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* (S', retry) \<Longrightarrow>
    (S, orElse l r) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S, r)"

notation (latex output) stm_star ("((_) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M\<^sup>\<star> (_))")

(*>*)
text_raw \<open>@{example StmStmStep}\<close>
text \<open>
  @{thm [mode=Rule] stm.simple} {\textsc{stmSimple}} \\
  @{thm [mode=Rule] stm.newTVar} {\textsc{stmNewTVar}} \\
  @{thm [mode=Rule] stm.readTVar} {\textsc{stmReadTVar}} \\
  @{thm [mode=Rule] stm.writeTVar} {\textsc{stmWriteTVar}} \\
  @{thm [mode=Rule] stm.bindStep} {\textsc{stmBindStep}} \\
  @{thm [mode=Rule] stm.bindReturn} {\textsc{stmBindReturn}} \\
  @{thm [mode=Rule] stm.bindRetry} {\textsc{stmBindRetry}} \\
  @{thm [mode=Rule] stm.orElseReturn} {\textsc{stmOrElseReturn}} \\
  @{thm [mode=Rule] stm.orElseRetry} {\textsc{stmOrElseRetry}} \\
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

lemma stmVal_no_stm: "
  stmVal a \<Longrightarrow>
  val a \<Longrightarrow>
  (S, a) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', b) \<Longrightarrow>
  False"
apply (erule stm.cases)
using val_no_simple apply blast
using val_no_simple apply fastforce
apply simp+
done

lemma stm_return_no_stm: "
  val v \<Longrightarrow>
  (S, return\<^sub>S\<^sub>T\<^sub>M v) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', x) \<Longrightarrow>
  False"
apply (erule stm.cases, simp_all)
using val_no_simple apply fastforce
done

lemma stm_retry_no_stm: "
  (S, retry) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (S', x) \<Longrightarrow>
  False"
apply (erule stm.cases, simp_all)
using val_no_simple apply fastforce
done

lemma converse_rtrancl_cases: "
  (a, b) \<in> r\<^sup>* \<Longrightarrow>
  (\<And>c. 
    a = c \<Longrightarrow> 
    b = c \<Longrightarrow> P) \<Longrightarrow>
  (\<And>a' b' c.
    a = a' \<Longrightarrow>
    b = b' \<Longrightarrow> 
    (a', c) \<in> r \<Longrightarrow>
    (c, b') \<in> r\<^sup>* \<Longrightarrow> 
    P) \<Longrightarrow>
  P"
apply (induct arbitrary: rule: converse_rtrancl_induct)
apply simp
by blast

lemma stm_termination_star_deterministic: "
  ((Sa, a), (Sc, c)) \<in> (stm \<inter> {(b, a). \<forall>x xa. b \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (x, xa) \<longrightarrow> a = (x, xa)})\<^sup>* \<Longrightarrow>
  (Sa, a) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* (Sb, b) \<Longrightarrow>
  (\<And>Sd d. (Sb, b) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (Sd, d) \<Longrightarrow> False) \<Longrightarrow>
  (\<And>Sd d. (Sc, c) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (Sd, d) \<Longrightarrow> False) \<Longrightarrow>
  (Sb, b) = (Sc, c)"
apply (induct rule: converse_rtrancl_induct)
apply (induct rule: converse_rtrancl_induct)
apply simp
apply auto[1]
apply (erule converse_rtrancl_cases) back
apply auto[1]
apply (subgoal_tac "ca = z")
apply fastforce
apply force
done

(* how disgusting is this? _very disgusting_. *)
lemma stm_deterministic: "
  (S, x) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (Sa, a) \<Longrightarrow>
  (S, x) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (Sb, b) \<Longrightarrow>
  (Sa, a) = (Sb, b)"
apply (induct arbitrary: Sb b rule: stm.induct)
apply (erule stm.cases)
subgoal using simple_deterministic by simp
using val_no_simple apply fastforce
using val_no_simple apply fastforce
using val_no_simple apply fastforce
using val_no_simple apply fastforce
using val_no_simple apply fastforce
using val_no_simple apply fastforce
using val_no_simple apply fastforce
using val_no_simple apply fastforce
apply (erule stm.cases)
subgoal using val_no_simple by fastforce
subgoal by (metis Pair_inject atom.inject expr.inject)
apply fastforce+
apply (erule stm.cases)
using val_no_simple apply fastforce
apply fastforce+
apply (erule stm.cases)
using val_no_simple apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply (erule stm.cases) back
apply (subgoal_tac "val (bind\<^sub>S\<^sub>T\<^sub>M m f)")
using val_no_simple prod.inject apply blast
apply simp
apply auto[1]
apply fastforce
apply fastforce
apply fastforce
apply fastforce
apply (thin_tac "(\<And>Sb b. (S, m) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (Sb, b) \<Longrightarrow> (S', m') = (Sb, b))")
using stm_return_no_stm prod.inject apply blast
apply (thin_tac "(\<And>Sb b. (S, m) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M (Sb, b) \<Longrightarrow> (S', m') = (Sb, b))")
using stm_retry_no_stm prod.inject apply blast
apply fastforce
apply fastforce
apply (erule stm.cases)
using val_no_simple apply fastforce
apply simp+
apply (erule stm.cases)
using val_no_simple apply fastforce
apply simp+
apply (erule stm.cases)
using val_no_simple apply fastforce
apply simp+
apply (erule stm.cases)
using val_no_simple apply fastforce
apply fastforce+
apply (erule stm.cases)
using val_no_simple apply fastforce
apply fastforce+
using stm_return_no_stm stm_termination_star_deterministic apply (metis (no_types, lifting) Pair_inject expr.inject(3))
using stm_return_no_stm stm_termination_star_deterministic apply (metis (no_types, lifting) Pair_inject atom.distinct(277) expr.inject(3) stm_retry_no_stm)
apply (erule stm.cases)
using val_no_simple apply fastforce
apply fastforce+
using stm_return_no_stm stm_termination_star_deterministic apply (metis (no_types, lifting) Pair_inject atom.distinct(277) expr.inject(3) stm_retry_no_stm)
using stm_return_no_stm stm_termination_star_deterministic apply simp
done

fun tvars_in :: "'l expr \<Rightarrow> (nat * 'l * 'l type) set" where
  "tvars_in (Var n) = {}"
| "tvars_in (Abs f) = tvars_in f"
| "tvars_in Leaf = {}"
| "tvars_in (Node atom l r) = tvars_in l \<union> tvars_in r \<union> (case atom of
      StmTVar n l t \<Rightarrow> {(n, l, t)}
    | other \<Rightarrow> {})"
    
definition tvars_heap_sound :: "'l type list \<Rightarrow> 'l expr \<Rightarrow> 'l stm \<Rightarrow> bool" ("_ \<turnstile> _ sound in _") where
  "\<Gamma> \<turnstile> e sound in S \<equiv> \<forall>(n, l, t) \<in> tvars_in e. \<exists>e'. S (l, t) `! n = Some e' \<and> \<Gamma> \<turnstile>\<^sub>S e': t"
  
lemma node_sound_elims: "
  \<Gamma> \<turnstile> Node atom l r sound in S \<Longrightarrow>
  \<Gamma> \<turnstile> l sound in S \<and> \<Gamma> \<turnstile> r sound in S \<and> (case atom of 
      StmTVar n ll t \<Rightarrow> (\<exists>v. S (ll, t) `! n = Some v \<and> \<Gamma> \<turnstile>\<^sub>S v: t) 
    | other \<Rightarrow> True)"
unfolding tvars_heap_sound_def apply simp
apply (case_tac atom, simp_all)
done

lemma node_sound_intros: "
  \<Gamma> \<turnstile> l sound in S \<Longrightarrow>
  \<Gamma> \<turnstile> r sound in S \<Longrightarrow>
  (case atom of 
      StmTVar n ll t \<Rightarrow> (\<exists>v. S (ll, t) `! n = Some v \<and> \<Gamma> \<turnstile>\<^sub>S v: t) 
    | other \<Rightarrow> True) \<Longrightarrow>
  \<Gamma> \<turnstile> Node atom l r sound in S"
unfolding tvars_heap_sound_def apply simp
apply (subst ball_Un)+
apply (rule conjI)
apply blast
apply (case_tac atom, simp_all)
done

thm lift.induct
find_theorems name: lift 

thm lift.induct[where P="\<lambda>k v. tvars_in v = tvars_in (lift k v)"]

lemma lift_preserves_tvars: "
  tvars_in x = tvars_in (lift k x)"
apply (rule lift.induct[where P="\<lambda>k v. tvars_in v = tvars_in (lift k v)"])
apply simp_all
done

lemma subst_mono_tvars: "
  tvars_in (a[k \<mapsto> b]) \<subseteq> tvars_in a \<union> tvars_in b"
apply (rule subst.induct[where P="\<lambda>a k b. tvars_in (a[k \<mapsto> b]) \<subseteq> tvars_in a \<union> tvars_in b"])
using lift_preserves_tvars apply simp_all
apply blast
done

lemma simple_mono_tvars_in: "
  a \<rightarrow> b \<Longrightarrow>
  tvars_in a \<supseteq> tvars_in b"
apply (induct rule: simple.induct)
apply simp
apply blast
apply simp
apply blast
apply (erule simple_eval.cases, simp_all)
using subst_mono_tvars apply blast
done

lemma tvars_in_mono_heap_sound: "
  tvars_in a \<supseteq> tvars_in b \<Longrightarrow>
  \<Gamma> \<turnstile> a sound in S \<Longrightarrow>
  \<Gamma> \<turnstile> b sound in S"
unfolding tvars_heap_sound_def by (meson subsetCE)

lemma simple_preserves_heap_sound: "
  a \<rightarrow> b \<Longrightarrow>
  \<Gamma> \<turnstile> a sound in S \<Longrightarrow>
  \<Gamma> \<turnstile> b sound in S"
using simple_mono_tvars_in tvars_in_mono_heap_sound by blast
  
lemma stm_preserves_heap_sound_simple_type: "
  aa \<rightarrow>\<^sub>S\<^sub>T\<^sub>M bb \<Longrightarrow>
  aa = (Sa, a) \<Longrightarrow>
  bb = (Sb, b) \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>S a: t \<Longrightarrow>
  \<Gamma> \<turnstile> a sound in Sa \<Longrightarrow>
  \<Gamma> \<turnstile> b sound in Sb \<and> \<Gamma> \<turnstile>\<^sub>S b: t"
thm stm.induct
apply (induct arbitrary: \<Gamma> t Sa a Sb b rule: stm.induct)
using simple_preserves_heap_sound simple_preserves_simple_type apply blast
apply clarsimp
apply (subgoal_tac "\<Gamma> \<turnstile>\<^sub>S return\<^sub>S\<^sub>T\<^sub>M (TVar (length (Sa (l, t))) l t): ta")
unfolding tvars_heap_sound_def apply simp
apply (erule simple_type_newTVar)
oops

abbreviation stm\<^sub>0 :: "'l stm" where
  "stm\<^sub>0 \<equiv> \<lambda>(l, t). []"

lemma stm_example_0: "
  (stm\<^sub>0((bot, type.Int) := [I 5, I 4]), 
    bind\<^sub>S\<^sub>T\<^sub>M (readTVar (TVar 0 bot type.Int)) (\<lambda>.
    bind\<^sub>S\<^sub>T\<^sub>M (readTVar (TVar 1 bot type.Int)) (\<lambda>.
    writeTVar (TVar 0 bot type.Int) (intOp (op +) (V 0) (V 1)))))
  \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^*
  (stm\<^sub>0((bot, type.Int) := [I 9, I 4]), return\<^sub>S\<^sub>T\<^sub>M unit)"
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.bindStep, simp_all+)
apply (rule stm.readTVar, simp_all+)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.bindReturn, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.simple)
apply (rule simple.eval, simp_all)
apply (rule simple_eval.app, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.bindStep, simp_all)
apply (rule stm.readTVar, simp_all+)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.bindReturn, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.simple)
apply (rule simple.eval, simp_all)
apply (rule simple_eval.app, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.simple)
apply (rule simple.right, simp_all)
apply (rule simple.eval, simp_all)
apply (rule simple_eval.intOp)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.writeTVar, simp_all+)
done

lemma stm_example_1: "
  let f = bind\<^sub>S\<^sub>T\<^sub>M (writeTVar (TVar 0 bot type.Int) (I 0)) (\<lambda>. retry);
      g = writeTVar (TVar 1 bot type.Int) (I 0) in
      ((stm\<^sub>0((bot, type.Int) := [I 5, I 4]), orElse f g) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* 
      (stm\<^sub>0((bot, type.Int) := [I 5, I 0]), return\<^sub>S\<^sub>T\<^sub>M unit))"
apply simp
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.orElseRetry, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.bindStep, simp_all)
apply (rule stm.writeTVar, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.bindReturn, simp_all)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.simple)
apply (rule simple.eval, simp_all)
apply (rule simple_eval.intros, simp_all)
apply (rule rtrancl.intros)
apply (rule converse_rtrancl_into_rtrancl)
apply (rule stm.writeTVar, simp_all)
apply simp
done

(*>*)
text_raw \<open>@{example StmBindExample}\<close>
text \<open>
  @{term "addWrite = 
    bind\<^sub>S\<^sub>T\<^sub>M (readTVar v_0) (\<lambda>.
    bind\<^sub>S\<^sub>T\<^sub>M (readTVar v_1) (\<lambda>.
    writeTVar v_0 (intOp (op +) (V 0) (V 1))))"
}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
    
inductive lio :: "('l stm * 'l expr) \<Rightarrow> ('l stm * 'l expr) \<Rightarrow> bool" ("_ \<rightarrow>\<^sub>L\<^sub>I\<^sub>O _")
where
  simple: "
    e \<rightarrow> e' \<Longrightarrow>
    (S, e) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S, e')" 
| atomicallyReturn: "
    val e \<Longrightarrow>
    (S, e) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* (S', return\<^sub>S\<^sub>T\<^sub>M v) \<Longrightarrow>
    (S, atomically e) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S', return\<^sub>L\<^sub>I\<^sub>O v)"
| atomicallyRetry: "
    val e \<Longrightarrow>
    (S, e) \<rightarrow>\<^sub>S\<^sub>T\<^sub>M^* (S', retry) \<Longrightarrow>
    (S, atomically e) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S, atomically e)"
| bindStep: "
    val m \<Longrightarrow>
    val f \<Longrightarrow>
    (S, m) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S', m') \<Longrightarrow>
    (S, bind\<^sub>L\<^sub>I\<^sub>O m f) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S', bind\<^sub>L\<^sub>I\<^sub>O m' f)"
| bindReturn: "
    val v \<Longrightarrow>
    val f \<Longrightarrow>
    (S, bind\<^sub>L\<^sub>I\<^sub>O (return\<^sub>L\<^sub>I\<^sub>O v) f) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S, app f v)"
    
(*>*)
text_raw \<open>@{example StmLioStep}\<close>
text \<open>
  @{thm [mode=Rule] lio.simple} {\textsc{lioSimple}} \\[1ex]
  @{thm [mode=Rule] lio.bindStep} {\textsc{lioBindStep}} \\[1ex]
  @{thm [mode=Rule] lio.bindReturn} {\textsc{lioBindReturn}} \\[1ex]
  @{thm [mode=Rule] lio.atomicallyReturn} {\textsc{lioAtomicallyReturn}} \\[1ex]
  @{thm [mode=Rule] lio.atomicallyRetry} {\textsc{lioAtomicallyRetry}} \\[1ex]
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)
  
inductive_set
  (* deterministic eager small-step *)
  dess :: "('l, 's) env rel" and 
  dess_abv :: "('l, 's) env \<Rightarrow> ('l, 's) env \<Rightarrow> bool" ("_ \<leadsto>\<^sub>S\<^sub>C\<^sub>H _")
where
  "a \<leadsto>\<^sub>S\<^sub>C\<^sub>H b \<equiv> (a, b) \<in> dess"
| threadStep: "
    sched_step s threads = (n, s') \<Longrightarrow>
    threads `! n = Some e \<Longrightarrow>
    (S, e) \<rightarrow>\<^sub>L\<^sub>I\<^sub>O (S', e') \<Longrightarrow>
    (threads, S, s) \<leadsto>\<^sub>S\<^sub>C\<^sub>H (threads[n := e'], S', s')"
| threadReturn: "
    sched_step s threads = (n, s') \<Longrightarrow>
    threads `! n = Some (return\<^sub>L\<^sub>I\<^sub>O v) \<Longrightarrow>
    val v \<Longrightarrow>
    (threads, S, s) \<leadsto>\<^sub>S\<^sub>C\<^sub>H (threads, S, s')"
    
(*>*)
text_raw \<open>@{example StmThreadStep}\<close>
text \<open>
  @{thm [mode=Rule] threadStep[where s=sch and threads=T and s'=sch']} {\textsc{threadStep}}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

(*>*)
text_raw \<open>@{example StmThreadReturn}\<close>
text \<open>
  @{thm [mode=Rule] threadReturn[where s=sch and threads=T and s'=sch']} {\textsc{threadReturn}}
\<close>
text_raw \<open>@{endexample}\<close>
(*<*)

end

definition "rr (s :: nat) threads = (s mod size threads, s + 1)"

interpretation rrs: scheduler rr 
apply unfold_locales
by (simp add: rr_def)

end
(*>*)