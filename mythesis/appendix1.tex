\appendix
\chapter{Appendix 1}
\label{chap:app1}

\begin{gather*}
  \Snippet{simple_intro_left} \\
  \Snippet{simple_intro_right} \\
  \Snippet{simple_intro_eval} \\
  \Snippet{simple_eval_if} \\
  \Snippet{simple_eval_intOp} \\
  \Snippet{simple_eval_intComp} \\
  \Snippet{simple_eval_app} \\
\end{gather*}

\section{The Lambda Calculus}
\label{chap:app1:lambda_calculus}

The lambda calculus is a minimalistic turing-complete calculus of computation. Its syntax is outlined in \autoref{fig:app1:lambda_calculus_grammar}, and its reduction semantics in \autoref{fig:app1:lambda_calculus_semantics}. The term $\lambda x.\ e$ \textit{abstracts} over instances of $x$ in $e$ to produce a function, while $(a, b)$ \textit{applies} the concrete value $b$ to the function $a$.

The semantics are very consise in part because a lot of the complexity is hidden in the expression $a[x := b]$, which means `replace all instances of $x$ in $a$ with $b$, renaming any bound variables to avoid capture'.

Bound variables are those variables $x$ that occur at the start of an abstraction term, $\lambda x.\ a$. For example in $\lambda x.\ (x, a)$, $x$ is bound but $a$ is free. To demonstrate capture, consider the term $(\lambda a.\ \lambda b.\ (a, b), b)$. After applying App once, we have $(\lambda b.\ (a, b))[a := b]$. However, $b$ now refers to a bound \textit{abstraction variable} inside the term, as well as a free variable outside of the term. If we rename the bound $b$ to a fresh name such as $c$, we get the equivalent expression $(\lambda c.\ (a, c))[a := b] = \lambda c.\ (b, c)$.

\begin{figure}[pt]
\centering
\begin{minipage}{.5\textwidth}
\begin{grammar}
<var> ::= $x$, $y$, $z$, \dots

<expr> ::= <var>
\alt (<expr>, <expr>)
\alt $\lambda$ <var>. <expr>

\end{grammar}
\end{minipage}

\caption[Syntax of the Lambda Calculus]{Syntax of the lambda calculus.}
\label{fig:app1:lambda_calculus_grammar}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.5\textwidth}
\begin{gather*}
\prftree[r]{App}{(\lambda x.\ a, b) \rightarrow a[x := b]}
\\
\prftree[r]{App-L}{a \rightarrow a^\prime}{(a, b) \rightarrow (a^\prime, b)} 
\\
\prftree[r]{App-R}{b \rightarrow b^\prime}{(a, b) \rightarrow (a, b^\prime)}
\end{gather*}
\end{minipage}

\caption[Semantics of the Lambda Calculus]{Semantics of the lambda calculus.}
\label{fig:app1:lambda_calculus_semantics}
\end{figure}

\iffalse
\section{The Pi Calculus}
\label{chap:app1:pi_calculus}

The pi-calculus is a minimalistic algebra of concurrent processes with communication. Its syntax is outlined in \autoref{fig:app1:pi_calculus_grammar}, and its reduction semantics in \autoref{fig:app1:pi_calculus_semantics}. The calculus also has some \textit{congruences}, or term equivalences, which are shown in \autoref{fig:app1:pi_calculus_congruence}: these can be used to `re-write' any of the semantic rules.

The pi calculus has a single kind of variable called a \textit{channel}, which is why the grammar for variables and channels is shared. The expression $c ? x.\ P$ means `receive a variable on channel $c$, bind it to name $x$, then run $P$'. Note that $x$ is a bound name in $P$ as discussed in \autoref{chap:app1:lambda_calculus}. Similarly, $c ! x.\ P$ means `send the variable $x$ on channel $c$, then run $P$'. $\nu\ x.\ P$ creates a new bound variable $x$ in $P$ (which must be renamed if there exists a free variable $x$ in $P$).

$(P\vert Q)$ indicate processes $P$ and $Q$ run in parallel, potentially communicating with each other. $0$ is a process that does nothing. $P\star$ is the process that runs $P$ until completion, then runs $P\star$.

\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
\begin{grammar}
<channel> ::= $x$, $y$, $z$, \dots

<var> ::= <channel>

<process> ::= 0
  \alt (<process> $\vert$ <process>)
  \alt <channel> ? <var> . <process>
  \alt <channel> ! <var> . <process>
  \alt $\nu$ <var> . <process>
  \alt <process> $\star$
\end{grammar}
\end{minipage}

\caption[Syntax of the Pi Calculus]{Syntax of the pi calculus.}
\label{fig:app1:pi_calculus_grammar}
\end{figure}

\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
\begin{gather*}
\prftree[r]{New}{P \rightarrow P^\prime}{\nu\ x.\ P \rightarrow \nu\ x.\ P^\prime} 
\\
\prftree[r]{Par-L}{P \rightarrow P^\prime}{(P\vert Q) \rightarrow (P^\prime\vert Q)} 
\\
\prftree[r]{Par-R}{Q \rightarrow Q^\prime}{(P\vert Q) \rightarrow (P\vert Q^\prime)}
\\
\prftree[r]{Send}{(c ! v.\ P \vert c ? x.\ Q) \rightarrow (P \vert Q[x := v])}
\end{gather*}
\end{minipage}

\caption[Semantics of the Pi Calculus]{Semantics of the pi calculus.}
\label{fig:app1:pi_calculus_semantics}
\end{figure}

\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
\begin{gather*}
(P \vert 0) \equiv P \\
((P \vert Q) \vert R) \equiv (P \vert (Q \vert R)) \\
(P \vert Q) \equiv (Q \vert P) \\
P\star \equiv (P\vert P\star) 
\end{gather*}
\end{minipage}

\caption[Congruences of the Pi Calculus]{Congruences of the pi calculus.}
\label{fig:app1:pi_calculus_congruence}
\end{figure}
\fi

\section{A Simple Machine}
\label{chap:app1:simple_machine}
The lambda calculus is a heavily abstracted model of computation. For real-world concurrent systems, we prefer a model closer to how those systems work. One common model is a shared-memory thread-spawning machine running an imperative language. The syntax for an example language is given in \autoref{fig:app1:machine_syntax}.

We assume a fixed collection of shared variables $\mathcal{V}$, and we model memory as mappings from $\mathcal{V}$ to the integers $\mathbb{Z}$. For memory $m: \mathcal{V} \rightarrow \mathbb{Z}$, $m(x)$ is the value of $x$ and $m(x := n)$ is a new, updated memory $m^\prime$ such that for all $v \in \mathcal{V}$ if $v = x$ then $m^\prime(v) = n$ and otherwise $m^\prime(v) = m(v)$.

\autoref{fig:app1:machine_single_thread_eval} describes the semantics of each statement in a single-threaded context, except for \texttt{spawn}. \autoref{fig:app1:machine_multiple_thread_eval} describes how a collection of programs $[p_1\ \vert \cdots \vert \ p_n]$, each running in a separate thread, evaluate on shared memory. Notice that the evaluation rules aren't deterministic: a single configuration can have several allowable final states.

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{grammar}
<var> ::= $x$, $y$, $z$, \dots

<stmt> ::= <var> `:=' <expr>
  \alt <stmt> `;' <stmt>
  \alt `spawn' <stmt> `then' <stmt>
  \alt `while' <expr> `{' <stmt> `}'
  \alt `if' <expr> `{' <stmt> `} else {' <stmt> `}'
  \alt `done'

<op> ::= `+', `*', `=', \dots
  
<expr> ::= <var>
  \alt `(' <expr> <op> <expr> `)'
  \alt `0', `1', `2', \dots
\end{grammar}
\end{minipage}

\caption[Syntax of a Simple Imperative Language]{Syntax of a simple imperative language containing numerical terms.}
\label{fig:app1:machine_syntax}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{gather*}
\prftree[r]{Var}{m \vdash x \downarrow m(x)}
\\
\prftree[r]{Lit-0}{m \vdash \mathtt{0} \downarrow 0} 
\hspace{2em} 
\prftree[r]{Lit-1}{m \vdash \mathtt{1} \downarrow 1} \hspace{2em} 
\cdots 
\\
\prftree[r]{Op-+}{m \vdash x \downarrow a}{m \vdash y \downarrow b}{m \vdash (x + y) \downarrow a + b} 
\hspace{2em}
\prftree[r]{Op-*}{m \vdash x \downarrow a}{m \vdash y \downarrow b}{m \vdash (x * y) \downarrow a * b}
\hspace{2em}
\cdots
\end{gather*}
\end{minipage}

\caption[Expression Evaluation of a Simple Imperative Language]{Expression evaluation rules for a simple imperative language. The judgement $m \vdash e \downarrow v$ indicates that under memory $m$, expression $e$ evaluates to value $v$.}
\label{fig:app1:machine_expr_eval}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{gather*}
\prftree[r]{Upd}{m \vdash e \downarrow n}{(x\ \mathtt{:=}\ e, m) \rightarrow (\mathtt{done}, m[x := n])}
\\
\prftree[r]{SeqFst}{(p, m) \rightarrow (p^\prime, m^\prime)}{(p\mathtt{;}\ q, m) \rightarrow (p^\prime\mathtt{;}\ q, m^\prime)}
\\
\prftree[r]{SeqSnd}{(\mathtt{done;}\ q, m) \rightarrow (q, m)}
\\
\prftree[r]{IfTrue}{m \vdash e \downarrow n}{n \not= 0}{(\mathtt{if}\ e\ \mathtt{then\ \{}\ t\ \mathtt{\}\ else\ \{}\ f\ \mathtt{\}}, m) \rightarrow (t, m)}
\\
\prftree[r]{IfFalse}{m \vdash e \downarrow n}{n = 0}{(\mathtt{if}\ e\ \mathtt{then\ \{}\ t\ \mathtt{\}\ else\ \{}\ f\ \mathtt{\}}, m) \rightarrow (f, m)}
\\
\prftree[r]{WhileTrue}{m \vdash e \downarrow n}{n \not= 0}{(\mathtt{while}\ e\ \mathtt{\{}\ p\ \mathtt{\}}, m) \rightarrow (p\mathtt{;}\ \mathtt{while}\ e\ \mathtt{\{}\ p\ \mathtt{\}}, m)}
\\
\prftree[r]{WhileFalse}{m \vdash e \downarrow n}{n = 0}{(\mathtt{while}\ e\ \mathtt{\{}\ p\ \mathtt{\}}, m) \rightarrow (\mathtt{done}, m)}
\end{gather*}
\end{minipage}

\caption[Statement Evaluation of a Single Thread]{Statement evaluation rules for a single thread of a simple imperative language. The judgement $(p, m) \rightarrow (p^\prime, m^\prime)$ indicates that under memory $m$ the statement $p$ changes the memory to become $m^\prime$, and the program then runs statement $p^\prime$.}
\label{fig:app1:machine_single_thread_eval}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{gather*}
\prftree[r]{SingleStep}{(p, m) \rightarrow (p^\prime, m^\prime)}{([\cdots\vert\ p\ \vert\cdots], m) \rightrightarrows ([\cdots\vert\ p^\prime\ \vert\cdots], m^\prime)}
\\
\prftree[r]{Spawn}{([\cdots\vert\ \mathtt{spawn}\ p\ \mathtt{then}\ q\ \vert\cdots], m) \rightrightarrows ([\cdots\vert q \vert\cdots\vert p], m)}
\end{gather*}
\end{minipage}

\caption[Statement Evaluation of Multiple Threads]{Statement evaluation rules for multiple threads of a simple imperative language. The judgement $(t, m) \rightrightarrows (t^\prime, m^\prime)$ indicates that under memory $m$ the collection of threads $t$ changes the memory to become $m^\prime$, and the collection of threads becomes $t^\prime$.}
\label{fig:app1:machine_multiple_thread_eval}
\end{figure}

\section{Seth}
\label{chap:app1:seth}
\Seth\ is a toy language consisting of a small fragment of Haskell containing IO actions and STM expressions. For simplicity, we do not include exceptions. We borrow heavily from the original paper on STM in Haskell, \cite{harris_composable_2005}. \autoref{fig:app1:seth_syntax} describes the syntax of values and terms in \Seth. Note that $(a, b)$ and $\lambda x.\ a$ correspond to the similar lambda-calculus terms defined in \autoref{chap:app1:lambda_calculus}.

We assume an infinite set of available \textit{thread identifiers} $\mathcal{T}$ and \textit{heap references} $\mathcal{R}$. We model a virtual machine consisting of a \textit{thread pool} $P: \mathcal{T} \rightarrow \langle\mathrm{value}\rangle$ and a \textit{shared heap} $H: \mathcal{R} \rightarrow \langle\mathrm{value}\rangle$. Notice that the pool and heap are untyped: for simplicity we ignore type safety and focusing on operational semantics. The pool tracks each currently evaluating term; the heap tracks the value of (potentially) shared STM variables.

A list of primitive terms and their types is given in \autoref{fig:app1:seth_primitives}. The monadic bind operator $\bindSeth$ allows the sequential composition of terms in the IO and STM monads, in particular by allowing the syntactic sugar described in \autoref{fig:app1:seth_monad_sugar}.

We use a similar notation for the thread pool as in \autoref{chap:app1:simple_machine}: $[\cdots\vert\ t_n: p_n\ \vert\cdots]$ indicates that the thread associated with thread identifier $t_n$ is currently evaluating the term $p_n$. We use the shorthand $[p]$ for $[\cdots\vert\ t: p\ \vert\cdots]$ whenever we want to talk about any process in the pool and don't care about its identifier. We write the virtual machine state as $(t, m)$ where $t$ is the thread pool and $m$ is the heap. 

\autoref{fig:app1:seth_io} describes the evaluation rules for threads performing an IO action. The phrase `$t$ fresh' indicates $t \in \mathcal{T}$ is a new, currently unused thread identifier. \autoref{fig:app1:seth_stm} describe the rules for STM terms. The symbol $\rightarrow^\star$ is used for the reflexive transitive closure of $\rightarrow$: $a \rightarrow^\star b$ iff $a = b$ or there exists $b^\prime$ such that $a \rightarrow b^\prime$ and $b^\prime \rightarrow^\star b$.

\autoref{fig:app1:seth_other} collects the remaining rules: evaluation of the $\atomicSeth$ primitive, lambda calculus terms, and $\bindSeth$.

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{align*}
\newTVar &: \forall a.\ a \rightarrow \TVar\ a \\
\readTVar &: \forall a.\ \TVar\ a \rightarrow \STMt\ a \\
\writeTVar &: \forall a.\ \TVar\ a \rightarrow a \rightarrow \STMt\ () \\
\retrySeth &: \forall a.\ \STMt\ a \\
\orElseSeth &: \forall a.\ \STMt\ a \rightarrow \STMt\ a \rightarrow \STMt\ a \\
\atomicSeth &: \forall a.\ \STMt\ a \rightarrow \IOt\ a \\
\putChar &: \chart \rightarrow \IOt\ \chart \\
\getChar &: \IOt\ \chart \\
\forkIO &: \forall a.\ \IOt\ a \rightarrow \IOt\ \mathcal{T} \\
\bindSeth &: \forall m, a.\ \mathtt{Monad}\ m \rightarrow m\ a \rightarrow (a \rightarrow m\ b) \rightarrow m\ b \\
\returnSeth &: \forall m, a.\ \mathtt{Monad}\ m \rightarrow a \rightarrow m\ a
\end{align*}
\end{minipage}

\caption[Primitive Terms of \Seth]{The IO, STM, and monad combinator primitives of \Seth, along with their types.}
\label{fig:app1:seth_primitives}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{align*}
\doSeth\ \{ x \leftarrow e; Q \} &\equiv e \bindSeth\ (\lambda\ x.\ \doSeth\ \{ Q \}) \\
\doSeth\ \{ e; Q \} &\equiv e \bindSeth\ (\lambda\ \_.\ \doSeth\ \{ Q \}) \\
\doSeth\ \{ e \} &\equiv e
\end{align*}
\end{minipage}

\caption[Monadic bind notation]{Term equivalence rules for converting `do' notation into calls to $\bindSeth$.}
\label{fig:app1:seth_monad_sugar}
\end{figure}


\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{grammar}
<var> ::= $x$, $y$, $z$, \dots

<tname> ::= $t \in \mathcal{T}$

<rname> ::= $r \in \mathcal{R}$

<char> ::= `a', `b', \dots

<term> ::= <var> | <value> | (<term>, <term>)

<value> ::= <tname> | <rname> | <char> | $\lambda$ <var>. <term>
\alt $\returnSeth$ <term> | <term> $\bindSeth$ <term> 
\alt $\putChar$ <char> | $\getChar$
\alt $\retrySeth$ | <term> $\orElseSeth$ <term>
\alt $\forkIO$ <term> | $\atomicSeth$ <term>
\alt $\newTVar$ <term> | $\readTVar$ <rname>
\alt $\writeTVar$ <rname> <term>
\end{grammar}
\end{minipage}

\caption[Syntax of \Seth]{Syntax of values and terms in \Seth.}
\label{fig:app1:seth_syntax}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{gather*}
\prftree[r]{Put}{\mathrm{output}\ c}{([\putChar\ c], m) \rightarrow ([\returnSeth\ ()], m)} \\
\prftree[r]{Get}{\mathrm{receive}\ c}{([\getChar], m) \rightarrow ([\returnSeth\ c], m)} \\
\prftree[r]{Fork}{t^\prime\ \mathrm{fresh}}{([\cdots\vert\ t:\ \forkIO\ e\ \vert\cdots], m)
  \rightarrow ([\cdots\vert\ t:\ \returnSeth\ t^\prime\ \vert\cdots\vert\ t^\prime: e], m)}
\end{gather*}
\end{minipage}

\caption[IO action semantics in \Seth]{Term evaluation rules for IO terms in \Seth. The informal phrases `output $c$' and `receive $c$' mean the irreversible IO side effects associated with the execution of the terms.}
\label{fig:app1:seth_io}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{gather*}
\prftree[r]{New}{r\ \mathrm{fresh}}{([\newTVar\ e], m) \rightarrow ([\returnSeth\ r], m(r := e))} \\
\prftree[r]{Read}{r \in \dom(m)}{([\readTVar\ r], m) \rightarrow ([\returnSeth\ m(r)], m)} \\
\prftree[r]{Write}{r \in \dom(m)}{([\writeTVar\ r\ e], m) \rightarrow ([\returnSeth\ ()], m(r := e))} \\
\prftree[r]{OrLeft}{([e_1], m) \rightarrow^\star ([\returnSeth\ e_1^\prime], m^\prime)}
  {([e_1\ \orElseSeth\ e_2], m) \rightarrow ([\returnSeth\ e_1^\prime], m^\prime)} \\
\prftree[r]{OrRight}{([e_1], m) \rightarrow^\star ([\retrySeth], m^\prime)}
  {([e_1\ \orElseSeth\ e_2], m) \rightarrow ([e_2], m)}
\end{gather*}
\end{minipage}

\caption[STM action semantics in \Seth]{Term evaluation rules for STM terms in \Seth.}
\label{fig:app1:seth_stm}
\end{figure}

\begin{figure}[pt]
\centering
\begin{minipage}{.75\textwidth}
\begin{gather*}
\prftree[r]{Atomic}{([e], m) \rightarrow^\star ([\returnSeth\ e^\prime], m^\prime)}
  {([\atomicSeth\ e], m) \rightarrow ([\returnSeth\ e^\prime], m^\prime)} \\
\prftree[r]{App}{([(\lambda x.\ a, b)], m) \rightarrow ([a[x := b]], m)} \\
\prftree[r]{BindReduce}{([a], m) \rightarrow ([a^\prime], m^\prime)}
  {([a\ \bindSeth\ b], m) \rightarrow ([a^\prime\ \bindSeth\ b], m^\prime)} \\
\prftree[r]{BindReturn}{([\returnSeth\ e\ \bindSeth\ a], m) \rightarrow ([(a, e)], m)}
\end{gather*}
\end{minipage}

\caption[Other semantics in \Seth]{Term evaluation rules for $\atomicSeth$, lambda calculus expressions, and $\bindSeth$ in \Seth.}
\label{fig:app1:seth_other}
\end{figure}
