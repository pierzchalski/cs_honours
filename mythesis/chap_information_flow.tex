\chapter{Information Flow Security}
\label{chap:information_flow}

As previously mentioned, IFS concerns the ability (and restrictions on the ability) of agents interacting within a system to gain information about the behaviour of other agents. Formal analysis of this topic requires in turn specifying what we mean by these somewhat vague and intuitive notions.

\section{Describing Policy}
\label{sec:information_flow:policy}

The description of information flow is usually reinterpreted as flow between `domains' rather than agents. For instance, we might have three agents $A_{H1}$, $A_{H2}$ and $A_L$, with a subscript $H$ indicating high access and $L$ indicating low access. We don't want to over-complicate our analysis by considering three distinct entities when we really only care about two categories of security, and so we say that agents $A_{H1}$ and $A_{H2}$ belong to the domain $H$, that $A_L$ belongs to the domain $L$, and we require information to only flow from agents in $L$ to agents in $H$.

Once we've identified the domains $D$ of relevance, we can already formunate some simple policies by constructing a reflexive relation $\rightarrowtail: D \times D$, with $d \rightarrowtail d^\prime$ indicating that we allow information from domain $d$ to directly reach domain $d^\prime$ (and implicitly, if $d\ \slashed{\rightarrowtail}\ d^\prime$, that we do \textit{not} allow such direct information flow). \autoref{fig:information_flow:simple_hi_lo}, \autoref{fig:information_flow:separation}, and \autoref{fig:information_flow:auditing} apply this concept to the examples in \autoref{sec:intro:security}.

In much of the existing research in IFS, $D$ is a lattice with a least upper bound operator $\sqcap$, greatest lower bound operator $\sqcup$, and $\rightarrowtail$ a partial order.

\begin{figure}[p]
\centering
\begin{tikzpicture}[>->,>=stealth,shorten >=1pt,auto,node distance=1.5cm,
                    semithick]
  
  \node   (h) {$H$};
  \node   (l) [right of = h] {$L$};
  
  \path (l) edge node {} (h)
        ;
\end{tikzpicture}

\caption[High-Low Security Policy]{Policy for the `Security' example in \autoref{par:intro:simple_hi_lo}. $H$ is the domain corresponding to the high-access agent, $L$ to the low-access agent.}
\label{fig:information_flow:simple_hi_lo}
\end{figure}

\begin{figure}[p]
\centering
\begin{tikzpicture}[>->,>=stealth,shorten >=1pt,auto,node distance=1.5cm,
                    semithick]
  
  \node   (l) {$L$};
  \node   (e) [below left of = l] {$E$};
  \node   (m) [below right of = l] {$M$};
  
  \path (e) edge node {} (l)
	(m) edge node {} (l)
        ;
\end{tikzpicture}

\caption[Device Separation Policy]{Policy for the `Separation' example in \autoref{par:intro:separation}. $L$ is the domain corresponding to the global systems logger, $E$ to the engine, and $M$ to the music system.}
\label{fig:information_flow:separation}
\end{figure}

\begin{figure}[p]
\centering
\begin{tikzpicture}[>->,>=stealth,shorten >=1pt,auto,node distance=1.5cm,
                    semithick]
  
  \node   (l) {$L$};
  \node   (t) [left of = l] {$T$};
  \node   (j) [right of = l] {$J$};
  
  \path (t) edge node {} (l)
	(l) edge node {} (j)
        ;
\end{tikzpicture}

\caption[Data Auditing Policy]{Policy for the `Auditing' example in \autoref{par:intro:auditing}. $T$ is the domain corresponding to the stock trader, $L$ to the lawyer, and $J$ to the judge.}
\label{fig:information_flow:auditing}
\end{figure}

\section{Executions, Observations, and Non-Interference}
\label{sec:information_flow:equivalence}

We consider what it means for an agent acting in one domain to learn information about another domain. It is difficult to give both a precise and generically applicable description of the concept, since information flow as a concept can be applied to many different models with distinct semantics.

In general however, there is usually a formalisation consisting of a set of states $S$, domains $D$, observations $B$, and a method for `progressing' a state (for instance, state $s$ `reduces to', `evaluates to', or `becomes' $s^\prime$, written $s \rightarrow s^\prime$). Examples of such state progression models for the lambda calculus and a simple memory machine are given in \autoref{chap:app1}.

Given a particular (possibly not yet completed) execution of our system $s = s_0 \rightarrow \cdots \rightarrow s_n$, for each domain $d \in D$ we may define $\obs_d(s): B$, `the observations by $d$ on $s$'. This is deliberately vague: there are many choices for $\obs$ depending on the situation to be modelled.

In general, IFS properties are described in terms of \textit{non-interference}. For all domains $d$ we show that $\obs_d(s_1) = \obs_d(s_2)$ whenever $s_1$ and $s_2$ are equivalent up to components that can affect $d$. Examples of observations and equivalence are given below.

\subsection{The Lambda Calculus}
\label{ssec:information_flow:lambda_calculus}
When considering a variant of the lambda calculus\footnotemark, information flow is discussed in terms of distinguishable expressions. Let $e: t, d$ mean the expression $e$ has value type $t$ and is tagged with the domain $d$. We want to be able to say it is safe for $L_1$ to view $e: t, d$ if $d \rightarrowtail L_1$, and similarly for the other domains. 

\footnotetext{The syntax and semantics of the lambda calculus are outlined in \autoref{chap:app1:lambda_calculus}.}

A similar setup is discussed in \cite{zheng_dynamic_2005}, where they provide a method for constructing expressions with the guarantee that, if $e: t, d$ contains a sub-term $e^\prime: t^\prime, d^\prime$, and $d^\prime\ \slashed{\rightarrowtail}\ d$, then for any two values $x_1: t^\prime$ and $x_2: t^\prime$ we have $e[e^\prime := x_1] = e[e^\prime := x_2]$. This captures that the final observation is independent of values that we haven't `allowed' to affect it. \cite{lourenco_dependent_2015} generalise this to a dependently-typed lambda calculus.

This description works well if $\rightarrowtail$ is indeed a lattice, since then $d^\prime\rightarrowtail d$ accurately captures all sources of potential influence on $d$. We will later see models where this is not the case.

\iffalse
\subsection{Process Algebras}
The pi-calculus is an algebra for describing concurrent processes equipped with the ability to create new processes and send messages between themselves\footnotemark. Assuming a security lattice, \cite{hennessy_security_2005} describe a method for determining if two pi-calculus terms $P$ and $Q$ are equivalent up to variable observations under constraints $\Gamma$, written $\Gamma \vartriangleright^d P \simeq Q$, where $d$ is the highest domain of observation allowed. $\Gamma$ tracks what `types' of variables a process term is capable of reading or writing, where each type is associated with a domain.

\footnotetext{The syntax and semantics of the pi calculus are outlined in \autoref{chap:app1:pi_calculus}.}

The authors show the above judgement implies $P \vert H \simeq Q \vert K$ for $H$, $K$ constrained by $\Gamma$. Choosing $Q = P$ and $H = 0$ gives $P \simeq P \vert K$, showing that $K$ cannot interfere with (have information flow to) $P$. \cite{van_der_meyden_comparison_2010} present a similar definition for a general labelled transition system.
\fi

\subsection{Simple Memory Machines}
We now turn to a model closer in design to how an actual computer is structured. Consider a simple shared-memory multi-threaded abstract machine\footnotemark. Say the memory $m$ of the machine is modelled as a mapping from variables $\mathcal{V}$ to values $\mathbb{V}$, with each variable assigned an `owner' by $\dom: \mathcal{V} \rightarrow D$. Assume the policy is a lattice such as in \autoref{fig:information_flow:simple_hi_lo}.

\footnotetext{The syntax and semantics of such a machine are outlined in \autoref{chap:app1:simple_machine}.}

Let $m\vert_X$ be $m$ restricted to those variables $v$ with $\dom(v) \in X$. Then at any moment, we allow domain $d$ to observe $m\vert_d$. Two memories $m$ and $m^\prime$ are observationally indistinguishable to domain $d$ if $m\vert_d = m^\prime\vert_d$.

We want domain $d$ to never be able to deduce the values in $m\vert_{d^\prime}$, where $d^\prime\ \slashed{\rightarrowtail}\ d$.  This means we want $d$ to never distinguish between executions that start in memories $m$ and $m^\prime$, where the memories are indistinguishable \textit{up to variables that can affect} $d$, i.e. $m\vert_X = m^\prime\vert_X$, $X = \{d^\prime \vert d\prime \rightarrowtail d\}$. 

\cite{russo2010dynamic} provide a type system to ensure such a property in a simple single-threaded language. \cite{mantel_assumptions_2011} produce a similar result in a mulithreaded system, extending the model of observation to allow variables in a domain to be temporarily `upgraded' to a higher domain and using an assume-guarantee model to ensure no other threads exploit this to violate IFS.

\section{Implicit Information Flow}
\label{sec:information_flow:implicit}

When determining if a given program is IFS, it is important to consider the effects of \textit{control flow} on IF. An example of using conditional execution to leak information between two domains without directly writing between them can be found in \autoref{fig:information_flow:implicit}. It is apparent the function \texttt{test} would allow the domain $L$ to quickly determine the value of \texttt{hi}, in violation of policy.

To detect and prevent this, \cite{hunt_flow-sensitive_2006} and others use the trick of tracking not just the domain of values and variables, but also of the `program counter' or program execution. In the example in \autoref{fig:information_flow:implicit} once one of the \texttt{if} branches were to evaluate all actions would be considered to be done `in domain $H$', and so the program would be judged insecure. A similar technique is used in lambda-calculus-based models, such as in \cite{lourenco_dependent_2015}.

Termination is another potential source of leaks. \cite{askarov_termination-insensitive_2008} demonstrate this in the context of imperative programs with observable intermediate output. They additionally provide bounds on the ability of an observer to extract information using termination-sensitive techniques.

\begin{figure}[pt]
\centering
\begin{lstlisting}
function leak(test: int) {
  if *hi > test {
    *lo = true;
  } else {
    *lo = false;
  }
}
\end{lstlisting}

\caption[Implicit Information Flow]{An example of implicit information flow in violation of a policy. Variable \texttt{hi} is in domain $H$, \texttt{lo} in $L$, and the policy is as in \autoref{fig:information_flow:simple_hi_lo}.}

\label{fig:information_flow:implicit}
\end{figure}

\section{Value-Dependent Domains}

Current research in applying IFS to functional languages focuses on \textit{typing judgements} similar to those outlined in \autoref{ssec:information_flow:lambda_calculus}: deduction rules are used to show that an expression $e$ has domain $d$. \textit{Dependently typed} languages allow typing judgements to refer to the value of types in scope. The canonical example is a fixed-length vector: the judgement $\vdash x: \mathtt{Vec}\ a\ 5$ indicates that $x$ is a vector with exactly 5 elements, all of type $a$.

Allowing domains to similarly depend on values allows for very expressive policies. \cite{lourenco_dependent_2015} describe such a system in a dependent lambda calculus and provide an entertaining motivation in the form of a paper review committee, however they don't consider concurrency. \cite{Murray_SPR_16} explore a similar system in an imperative, mulithreaded, shared mutable memory model using assume-guarantee reasoning to ensure security. Both of these show their results over a security lattice.

$\mathrm{F}^\star$ is a dependently-typed variant of F with a focus on security in distributed systems \cite{export:150012}. The authors delegate the actual verification of IFS properties to a separate refinement typing analysis. In \cite{export:115445}, the authors describe \textsc{Fine}, a language that uses dependent, refinement, and affine types to ensure enforcement of security policies described in a subset of Prolog.

\section{Non-Lattice Policies}
\label{sec:information_flow:non_lattice_policies}

So far all of the above examples have discussed IFS properties when the security policy $\rightarrowtail$ is a lattice. Importantly, in this case $\rightarrowtail$ is transitive: if $a \rightarrowtail b$ and $b \rightarrowtail c$, then $a \rightarrowtail c$. This gives a concise way to capture all allowable and non-allowable effects on a given domain, and is also algebraically easier to work with. However, as alluded to in \autoref{par:intro:auditing} IFS can be used to discuss not just the \textit{prevention} of information flow but also \textit{control}.

One common motivation for intransitive policies is downgrading, where for instance in \autoref{fig:information_flow:auditing} the trader $T$ holds sensitive information which is selectively downgraded by the lawyer $L$ (whom we trust to only downgrade when appropriate) and who then releases the information to the judge $J$, who is only privy to insensitive information.

\cite{bossi_modelling_2004} constructs a semantics for downgrading in a process algebra, defining a downgrade to be secure when there exists a high-domain behaviour with the same low-domain observations. \cite{li_practical_2005} describe the need for downgrading in practical information-secure analysis and provide a simple typing system which contains expressions to define downgrading, however they do not prove enforcement of any IFS properties. 

Jif is an extension to Java designed to enforce IFS properties. It provides an `escape hatch' where an arbitrary security-labelled term can be relabelled, as long as the caller \textit{has permission} to perform the downgrading \cite{pullicino_jif:_2014}. In this sense it requires an `augmented' security policy beyond what we have discussed so far.

\cite{vdm2015} discuss the semantics of IFS for a general policy relation. In particular they focus on \textit{knowledge}, in this case the ability for a collection of domains to infer information about another domain.