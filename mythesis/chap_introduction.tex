\chapter{Introduction}
\label{chap:intro}

\section{Security}
\label{sec:intro:security}

Interest in security is a growing trend in software engineering. Engineers are more capable of applying security reasoning due to the increased ease and sophistication of static analysis and verification techniques, and customers are more interested in the security guarantees of their products as they become more pervasive (internet of things), more valuable (satellites), or more dangerous if they become out-of-control (cars, rockets, medical equipment). In addition, the rise of social media services and of information sharing, as well as several widely-publicised privacy violations, highlight the need for security of information access.

Information flow security (IFS) is a branch of security analysis concerning itself with the privacy and integrity of information access in systems with several interacting users, where we may wish to restrict what behaviours the users can perform or the observations they can make. There are several potential use cases for enforcing such properties:

\paragraph{Secrecy:}
\label{par:intro:simple_hi_lo}
The canonical example of information security is a shared system between one user who has access to `Top Secret' or `High Security' information, and another user who only has `Not Secret' or `Low Security' access. The low-access user must not be able to learn any high-security information (which in turn constrains the high-access user to not reveal any) \cite{icloud_2016} \cite{zeitung_all}.

\paragraph{Separation:}
\label{par:intro:separation}
Consider a car, with device drivers for the engine and for the speakers, as well as a global systems log. We want to ensure the two devices (the engines and speakers) are independent - one should not influence the other - but at the same time, we want the global log to be able to observe the behaviour of both systems. This is closely related to another property desired in similar systems, `failure tolerance': the ability of one component to continue operation unaffected by the failure of another \cite{panaroni_safety_2008}\footnotemark.

\footnotetext{\cite{levine_goldman_2016} describes a similar separation requirement in finance. Including a regulatory body such as the SEC recovers our original example, with the SEC acting as the global log.}

\paragraph{Auditing:}
\label{par:intro:auditing}
Say we have a stock trader, a lawyer, and a judge embroiled in an insider trading scandal. The judge has issued a warrant for communications by the stock trader. We never want the trader to give these communications directly to the judge, since the trader doesn't have the expertise to know what is and isn't a reasonable request for such information, and might reveal something sensitive. Instead, we want all these requests to go through the lawyer, who indeed knows how to justify not providing the data. Notice that here the focus is not on \textit{preventing} information flow (the judge will surely learn something about the actions of the trader), but on \textit{controlling} it \cite{chong_security_2004}.

We will see more about defining and enforcing such models and policies in \autoref{chap:information_flow}.

\section{Concurrency}
\label{sec:intro:concurrency}

The previous examples of environments where we might wish for information security all share the property that they are equally well defined and realistic in a system with concurrent execution of actions by users. Consider that modern embedded systems - including those in automobiles - contain many processing units managing sub-systems and running in parallel. There is a recent trend towards consolidating these units into fewer, more complex processors that will also run these sub-systems in parallel, in separate threads on separate cores \cite{heiser_role_2008}.

There is thus interest in generalising the definitions of IFS policies and the techniques for their enforcement to concurrent and parallel systems. This area of research now coincides with practical application: the Cross-Domain Desktop Compositor project (CDDC) in development at Data61 is a hardware system designed to enforce a particular security policy between several devices. The CDDC set-up involves several components that act concurrently: research is ongoing to formally verify that the entire system indeed enforces the policy.

There are many hazards and issues that hinder the ability to write concurrent programs with shared state. We will discuss these potential problems, their solutions, and how to model their semantics in \autoref{chap:concurrency}. In particular, we will find that monadic software transactional memory is both pragmatic from a software engineering perspective and useful in its theoretical properties.
