\documentclass[xcolor={dvipsnames}]{beamer}

\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{matrix}

\title{Information Security Properties in Concurrent Functional Languages}
\author{Edward Pierzchalski (z3379830) \\ Toby Murray (Co-Supervisor) \\ Gabriele Keller (Co-Supervisor)}
\date{}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\section{Information Security Properties}

\begin{frame}
\frametitle{Information Flow}

\pause
\begin{itemize}
\item Users (or roles) interact with each other
\pause
\item Want to control what information is shared (or inferred)
\pause
\item Databases, file systems, routers
\pause
\item Pacemakers, cars, IoT, rockets, satellites
\pause
\item `Just air gap it' doesn't cut it
\pause
\item How to formalise?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Transitive Non-Interference}

Specify where information is allowed to flow (positive policy)

\begin{columns}

\begin{column}{0.5\textwidth}
\begin{itemize}
\pause
\item Captain Carol can talk to Bureaucrat Bob
\pause
\item Bureaucrat Bob can talk to Admiral Alice
\pause
\item \textit{Transitively}, Carol can talk to Alice
\pause
\item Alice can't talk to (\textit{interfere with}) Carol, so Carol shouldn't learn what Alice is doing
\end{itemize}
\end{column}

\pause
\begin{column}{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[yscale=-2.0, xscale=2.0]
\node[name=B] at (0, 0) {Bob};
\node[name=A] at (1, 0) {Alice};
\node[name=C] at (0, 1) {Carol};

\path 
  (A) edge [<-] node {} (B)
  (B) edge [<-] node {} (C)
  (A) edge [<-, dotted] node {} (C)
  ;
\end{tikzpicture}

Example security policy, where $a \rightarrow b$ means `information can flow from $a$ to $b$'.
\end{center}
\end{column}

\end{columns}
\end{frame}

\begin{frame}
\frametitle{Capturing Semantics Formally}

\begin{itemize}
\pause
\item Consider sequences of actions (by system, or by agents)
\pause
\item Each agent makes observations
\pause
\item `System is secure' $\equiv$ `agents can't distinguish between certain observations'
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Capturing Semantics Formally - An Example}

\pause
Alice and Bob share and see variable $v$

\pause
\begin{table}
\begin{tabular}[t]{l | p{4cm} | p{4cm} }
 & Sequence 1 & Sequence 2 \\
\hline \hline
Actions & Alice sets $v$ to 0; \newline Bob sets $v$ to 1; & Bob sets $v$ to 1; \newline Alice sets $v$ to 0;  \\ 
\hline
Bob sees & $v \equiv 1$ & $v \equiv 0$
\end{tabular}
\end{table}

\pause
Bob does the same actions both times, but sees different results; information flows from Alice to Bob, which isn't allowed!
\end{frame}

\begin{frame}
\frametitle{Intransitive Non-Interference}

\begin{columns}

\begin{column}{0.5\textwidth}
\begin{itemize}
\pause
\item Transitive non-interference is very restrictive
\pause
\item Admiral Alice can't send orders to Captain Carol
\pause
\item Adding `Alice $\rightarrow$ Carol' to our transitive policy makes it trivial
\pause
\item Remove transitivity, force Alice to send orders via Bob, Carol to report via Bob
\end{itemize}
\end{column}

\pause
\begin{column}{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[yscale=-2.0, xscale=2.0]
\node[name=B] at (0, 0) {Bob};
\node[name=A] at (1, 0) {Alice};
\node[name=C] at (0, 1) {Carol};

\path 
  (A) edge [<-, bend left=10] node {} (B)
      edge [->, bend right=10] node {} (B)
  (B) edge [<-, bend left=10] node {} (C)
      edge [->, bend right=10] node {} (C)
  (A) edge [<->, dotted] node {\textbackslash} (C)
  ;
\end{tikzpicture}

\end{center}
\end{column}

\end{columns}
\end{frame}


\section{Concurrency in Languages}

\begin{frame}
\frametitle{Process Algebras, Pi Calculus}

\begin{itemize}
\pause
\item Large family of process calculi to choose from
\pause
\item Simple concurrent semantics plus (less simple) inter-process messaging
\pause
\item Existing literature on security focuses on timing, non-determinism
\pause
\item Less research focus on static analysis and strong typing
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Lambda Calculus}

\begin{itemize}
\pause
\item The Original Functional Language
\pause
\item Foundation of many real-world ones
\pause
\item No concurrency in original formulation
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Software Transactional Memory}

\pause
\begin{center}
Just assume we have a transactional primitive: $STM(a := 1, b := 2)$ sets $a$ and $b$ if and only if no other transaction modified $a$ or $b$.
\end{center}
\end{frame}

\begin{frame}
\frametitle{STM Semantics}

\pause
Imperative semantics:

\begin{itemize}
\pause
\item Mark blocks of commands as transactional
\pause
\item Need to deal with `transaction leaks': variables are touched inside and outside of transactions
\pause
\item Memory ordering semantics are important
\end{itemize}

\pause
Haskell semantics (only complete and useful implementation):

\begin{itemize}
\pause
\item Uses type system to track terms that use STM
\pause
\item Also tracks IO actions (question - how to model `observations'?)
\end{itemize}
\end{frame}


\lstset{escapeinside={<@}{@>}}
\defverbatim[colored]\lstI{
\begin{lstlisting}[language=Haskell,basicstyle=\ttfamily,]
main = do 
  <@\textcolor{BrickRed}{v <- atomically (newTVar 0)}@>
  <@\textcolor{BlueViolet}{forkIO}@> (alice v)
  <@\textcolor{BlueViolet}{forkIO}@> (bob v)
          
alice v = <@\textcolor{OliveGreen}{atomically (writeTVar v 0)}@>
bob v = <@\textcolor{OliveGreen}{atomically (writeTVar v 0)}@>
\end{lstlisting}
}

\begin{frame}
\frametitle{Haskell STM}

\lstI

\begin{itemize}
\pause
\item Make a \textcolor{BrickRed}{shared STM variable}
\pause
\item Do some things with the variable in \textcolor{BlueViolet}{separate threads}
\pause
\item Use the variable \textcolor{OliveGreen}{transactionally}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Not-Quite-Haskell STM}

\begin{itemize}
\pause
\item Haskell is huge
\pause
\item `Eagerly evaluated lambda-calculus plus STM' is not
\pause
\item Can we statically check if a simple mini-Haskell STM program follows a security policy?
\end{itemize}

\end{frame}


\section{Chocolate and Peanut Butter}

\begin{frame}
\frametitle{Utility}

\begin{itemize}
\pause
\item More applications care about security
\pause
\item More applications are concurrent
\pause
\item Few languages are directly relatable to process calculi
\pause
\item STM has a straightforward refinement to imperative languages
\pause
\item Security is great for grant funding
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Novelty}

\begin{table}
\begin{tabular}{ p{3cm} | p{3cm} | p{3cm} }
& \textbf{Simple Security Model} & \textbf{Fancy Security Model} \\
\hline
\textbf{Simple Execution Model} & \textit{Everything} & Dependent types, \newline Dynamic types, \newline Non-Transitivity \\ 
\hline
\textbf{Fancy Execution Model} & Concurrency, \newline Non-determinism, \newline Timing & \textit{Nothing} \pause\newline\textcolor{BrickRed}{(so far)}
\end{tabular}
\end{table}
\end{frame}

\section{Question Time}

\begin{frame}
\frametitle{Bibliography}

lol todo
\end{frame}


\end{document}
