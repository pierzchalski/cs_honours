\documentclass[xcolor={dvipsnames}]{beamer}

\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage[normalem]{ulem}
\usepackage{prftree}
\usepackage{tikz}
\usepackage{pifont}
\usepackage{wasysym}
\usetikzlibrary{matrix}

\title{Information Security Properties in Concurrent Functional Languages}
\author{Edward Pierzchalski (z3379830) \\ Toby Murray (Co-Supervisor) \\ Gabriele Keller (Co-Supervisor)}
\date{}

\usefonttheme[onlymath]{serif}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\prfinterspace=1em

\begin{document}

\frame{\titlepage}

\section{Recap: Information Security Properties}

\begin{frame}
\frametitle{Information Flow}

\pause
\begin{itemize}
\item Users (or roles) interact with each other
\pause
\item Want to control what information is shared (or inferred)
\pause
\item Databases, file systems, routers
\pause
\item Pacemakers, cars, IoT, rockets, satellites
\pause
\item `Just air gap it' doesn't cut it
\pause
\item How to formalise?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Transitive Non-Interference}

Specify where information is allowed to flow (positive policy)

\begin{columns}

\begin{column}{0.5\textwidth}
\begin{itemize}
\pause
\item Captain Carol can talk to Bureaucrat Bob
\pause
\item Bureaucrat Bob can talk to Admiral Alice
\pause
\item \textit{Transitively}, Carol can talk to Alice
\pause
\item Alice can't talk to (\textit{interfere with}) Carol, so Carol shouldn't learn what Alice is doing
\end{itemize}
\end{column}

\pause
\begin{column}{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[yscale=-2.0, xscale=2.0]
\node[name=B] at (0, 0) {Bob};
\node[name=A] at (1, 0) {Alice};
\node[name=C] at (0, 1) {Carol};

\path 
  (A) edge [<-] node {} (B)
  (B) edge [<-] node {} (C)
  (A) edge [<-, dotted] node {} (C)
  ;
\end{tikzpicture}

Example security policy, where $a \rightarrow b$ means `information can flow from $a$ to $b$'.
\end{center}
\end{column}

\end{columns}
\end{frame}

\begin{frame}
\frametitle{Capturing Semantics Formally}

\begin{itemize}
\pause
\item Consider sequences of actions (by system, or by agents)
\pause
\item Each agent makes observations
\pause
\item `System is secure' $\equiv$ `agents can't distinguish between certain observations'
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Capturing Semantics Formally - An Example}

\pause
Alice and Bob share and see variable $v$

\pause
\begin{table}
\begin{tabular}[t]{l | p{4cm} | p{4cm} }
 & Sequence 1 & Sequence 2 \\
\hline \hline
Actions & Alice sets $v$ to 0; \newline Bob sets $v$ to 1; & Bob sets $v$ to 1; \newline Alice sets $v$ to 0;  \\ 
\hline
Bob sees & $v \equiv 1$ & $v \equiv 0$
\end{tabular}
\end{table}

\pause
Bob does the same actions both times, but sees different results; information flows from Alice to Bob, which isn't allowed!
\end{frame}

\section{Concurrency in Languages}

\begin{frame}
\frametitle{Software Transactional Memory}

\pause
\begin{center}
Just assume we have a transactional primitive: $STM(a := 1, b := 2)$ sets $a$ and $b$ if and only if no other transaction modified $a$ or $b$.
\end{center}

\pause
Haskell semantics (only complete and useful implementation):

\begin{itemize}
\pause
\item Uses type system to track terms that use STM
\pause
\item Tracks and separates IO actions, preventing `transaction leaks'
\end{itemize}
\end{frame}

\lstset{escapeinside={<@}{@>}}
\defverbatim[colored]\lstI{
\begin{lstlisting}[language=Haskell,basicstyle=\ttfamily,]
main = do 
  <@\textcolor{BrickRed}{v <- atomically (newTVar 0)}@>
  <@\textcolor{BlueViolet}{forkIO}@> (alice v)
  <@\textcolor{BlueViolet}{forkIO}@> (bob v)
          
alice v = <@\textcolor{OliveGreen}{atomically (writeTVar v 0)}@>
bob v = <@\textcolor{OliveGreen}{atomically (writeTVar v 0)}@>
\end{lstlisting}
}

\begin{frame}
\frametitle{Haskell STM}

\lstI

\begin{itemize}
\pause
\item Make a \textcolor{BrickRed}{shared STM variable}
\pause
\item Do some things with the variable in \textcolor{BlueViolet}{separate threads}
\pause
\item Use the variable \textcolor{OliveGreen}{transactionally}
\end{itemize}
\end{frame}

\section{Progress}

\begin{frame}
\frametitle{Goals}
\begin{itemize}
  \pause
  \item Formalise a simple lambda calculus \pause \cmark
  \pause
  \item Define and show a simple non-interference property on it \pause \cmark
  \pause
  \item Mechanise these proofs in Isabelle \pause \cmark
  \pause
  \item Formalise a small Haskell-like language with STM \pause \cmark?
  \pause
  \item Define and show a simple non-interference property on it \pause \xmark?
  \pause
  \item Mechanise these proofs in Isabelle \pause \xmark
\end{itemize}
\end{frame}

\section{A Simple Lambda Calculus}

\begin{frame}
\frametitle{Simple Lambda Calculus}
\begin{itemize}
  \pause
  \item Terms are de Bruijn-encoded lambda calculus terms
  \pause
  \item De Bruijn encodings are surprisingly nice to work with!
  \pause
  \item Typing judgements of the form $\Gamma \vdash \textcolor{BrickRed}{e}: (\textcolor{OliveGreen}{t} \vert \textcolor{BlueViolet}{l})$
  \pause
  \item Under $\Gamma$, $\textcolor{BrickRed}{e}$ is a value of type $\textcolor{OliveGreen}{t}$ and security level $\textcolor{BlueViolet}{l}$
  \pause
  \item Include a language term for explicitly labelling terms with a security level
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Simple Lambda Calculus: Non-Interference}
\begin{itemize}
  \pause
  \item Define an `indistinguishability' judgement on terms, $\Gamma \vdash \textcolor{BrickRed}{a} =_{\textcolor{BlueViolet}{l}} \textcolor{BrickRed}{b}: \textcolor{OliveGreen}{(t \vert l^\prime)}$
  \pause
  \item Under $\Gamma$, terms $\textcolor{BrickRed}{a}$ and $\textcolor{BrickRed}{b}$ are indistinguishable to observers at security level $\textcolor{BlueViolet}{l}$ (and have type/label $\textcolor{OliveGreen}{(t \vert l^\prime)}$)
  \pause
  \item Defined ``obviously'':
  \begin{itemize}
    \pause
    \item Equal constants are always indistinguishable
    \pause
    \item Two terms with level $\textcolor{BlueViolet}{l^\prime}$ are indistuingishable to an observer at level $\textcolor{BlueViolet}{l}$ if $\textcolor{BlueViolet}{l^\prime > l}$
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Simple Lambda Calculus: Results}
\begin{itemize}
  \pause
  \item We prove the most straightforward information-flow property: non-interference
  \pause
  \item If two terms are judged indistinguishable, then they will remain indistinguishable after execution
  \pause
  \item Formalised in Isabelle!
\end{itemize}
\end{frame}

\section{A Simple STM Language}

\begin{frame}
\frametitle{Differences, Challenges}
\begin{itemize}
  \pause
  \item Terms that are security-relevant are cleanly separated by the STM monad \pause (easier!)
  \pause
  \item Concurrent threads with a global, shared heap of transaction variables \pause (harder!)
  \pause
  \item Transactions are atomic
  \begin{itemize}
    \pause
    \item A single evaluation step ($\rightarrow$) in a thread might involve many evaluation steps ($\rightarrow^\star$) for an STM term \pause (induction is \textit{much} harder!)
  \end{itemize}
  \pause
  \item More top-level language concepts (STM combinators, IO actions, etc...)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{STM Types}
\begin{itemize}
  \pause
  \item Previously, we gave every term a typing judgement that included a security label
  \pause
  \item For STM, we instead only include labels for STM and IO actions
  \pause
  \item $\Gamma \vdash \textcolor{BrickRed}{e}: \textsf{STM}\ (\textsf{R:}\ \textcolor{MidnightBlue}{r})\ (\textsf{W:}\ \textcolor{OliveGreen}{w})\ \textcolor{BrickRed}{t}$:
  \begin{itemize}
    \pause
    \item $\textcolor{BrickRed}{e}$ is an STM expression that, when run atomically, produces a value of type $\textcolor{BrickRed}{t}$
    \pause
    \item Reads data of security level at most $\textcolor{MidnightBlue}{r}$
    \pause
    \item Writes to STM variables of security level at least $\textcolor{OliveGreen}{w}$
  \end{itemize}
  \pause
  \item $\Gamma \vdash \textcolor{BrickRed}{e}: \textsf{IO}\ \textcolor{MidnightBlue}{l}\ \textcolor{BrickRed}{t}$: 
  \begin{itemize}
    \pause
    \item $\textcolor{BrickRed}{e}$ is a side-effectful computation producing a value of type $\textcolor{BrickRed}{t}$
    \pause
    \item Respects non-interference of a thread allowed to observe at security level $\textcolor{MidnightBlue}{l}$
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Concurrency and Correctness}
\begin{itemize}
  \pause
  \item Multiple threads run concurrently with shared access to a heap of `transaction variables' (referenced using \texttt{TVar}s)
  \pause
  \item Need to track the security level of each variable (for equivalence later)
  \pause
  \item Need to track type of each variable (for functional correctness and constraining execution)
  \begin{itemize}
    \pause
    \item We cheat: \texttt{TVar}s carry with them their security level and the type they store
    \pause
    \item Only need to show preservation that all \texttt{TVar}s are `correct' during execution
    \pause
    \item TODO: show we can eliminate this `run time overhead', since it's a straightforward program rewrite plus refinement
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Concurrency and Security}
\begin{itemize}
  \pause
  \item For the simple lambda calculus, we had an `indistinguishability relation' between terms
  \pause
  \item This was hard to work with!
  \begin{itemize}
    \pause
    \item Need to consider all the ways that two terms could be related
    \pause
    \item Then consider how either of them could execute...
  \end{itemize}
  \pause
  \item Instead, use \textit{erasure}:
  \begin{itemize}
    \pause
    \item Define a function $\epsilon_l$ that erases information sensitive to an observer at level $l$
    \pause
    \item Show that a term $e$ is indistinguishable from its erasure $\epsilon_l(e)$
  \end{itemize}
  \pause
  \item Need to erase entire program context, including threads and STM heap
\end{itemize}
\end{frame}


\section{An Aside: Defining Terms and their Structural Properties}

\lstset{escapeinside={<@}{@>}}
\defverbatim[colored]\lstTraditionalDescription{
\begin{lstlisting}[language=Haskell,basicstyle=\ttfamily,]
datatype <@\textcolor{BrickRed}{expr}@> =
  Var <@\textcolor{BlueViolet}{nat}@> 
| Abs <@\textcolor{BrickRed}{expr}@>
| App <@\textcolor{BrickRed}{expr expr}@>
| If <@\textcolor{BrickRed}{expr expr expr}@>
| While <@\textcolor{BrickRed}{expr expr expr}@>
| IntLit <@\textcolor{BlueViolet}{int}@> 
| ...
\end{lstlisting}
}

\begin{frame}
\frametitle{Describing Program Terms - Traditionally}

\lstTraditionalDescription

\begin{itemize}
  \pause
  \item One top-level variant per feature
  \pause
  \item Unwieldly for structural definitions
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Structural Definitions on Programs}
\framesubtitle{Term substitution}

\begin{align*}
(\texttt{Var}\ n)[x \mapsto y] & = \text{fancy de bruijn stuff} \\
(\texttt{Abs}\ f)[x \mapsto y] & = \text{more de bruijn stuff} \\
(\texttt{If}\ b\ t\ f)[x \mapsto y] & = \texttt{If}\ b[x \mapsto y]\ t[x \mapsto y]\ f[x \mapsto y] \\
\vdots & 
\end{align*}

\end{frame}

\begin{frame}
\frametitle{Structural Definitions on Programs}
\framesubtitle{Term extraction}

\begin{align*}
\textsf{tVarsIn}\ (\texttt{TVar}\ n) & = \{\texttt{TVar}\ n\} \\
\textsf{tVarsIn}\ (\texttt{Abs}\ f) &= \textsf{tVarsIn}\ f \\
\textsf{tVarsIn}\ (\texttt{If}\ b\ t\ f) & = (\textsf{tVarsIn}\ b) \cup (\textsf{tVarsIn}\ t) \cup (\textsf{tVarsIn}\ f) \\
\vdots & 
\end{align*}

\end{frame}

\begin{frame}
\frametitle{Structural Definitions on Programs}
\framesubtitle{Deterministic Eager Small-Step Semantics}

\begin{gather*}
\prftree{b \rightarrow b^\prime}{\texttt{If}\ b\ t\ f \rightarrow \texttt{If}\ b^\prime\ t\ f}
\\
\prftree{\textsf{isValue}\ b}{t \rightarrow t^\prime}{\texttt{If}\ b\ t\ f \rightarrow \texttt{If}\ b\ t^\prime\ f}
\\
\prftree{\textsf{isValue}\ b}{\textsf{isValue}\ t}{f \rightarrow f^\prime}{\texttt{If}\ b\ t\ f \rightarrow \texttt{If}\ b\ t\ f^\prime}
\\
\vdots
\end{gather*}

\end{frame}

\lstset{escapeinside={<@}{@>}}
\defverbatim[colored]\lstAlternativeDescription{
\begin{lstlisting}[language=Haskell,basicstyle=\ttfamily,]
datatype <@\textcolor{BrickRed}{expr}@> =
  Var <@\textcolor{BlueViolet}{nat}@> 
| Abs <@\textcolor{BrickRed}{expr}@>
| Leaf
| Node <@\textcolor{OliveGreen}{atom}@> <@\textcolor{BrickRed}{expr expr}@> 

datatype <@\textcolor{OliveGreen}{atom}@> =
  If 
| While 
| IntLit <@\textcolor{BlueViolet}{int}@> 
| IntOp <@\textcolor{BlueViolet}{int -> int -> int}@> 
| ...  
\end{lstlisting}
}

\begin{frame}
\frametitle{Describing Program Terms - The Lisp Way}

\lstAlternativeDescription

\begin{itemize}
  \pause
  \item Top layer factors out $\lambda$-abstraction
  \pause
  \item Use lots of abbreviations like $\texttt{TVar}\ n \equiv \texttt{Node}\ (\texttt{atom.TVar}\ n)\ \texttt{Leaf}\ \texttt{Leaf}$
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Structural Definitions on Programs}
\framesubtitle{Term substitution}

\begin{align*}
(\texttt{Var}\ n)[x \mapsto y] & = \text{fancy de bruijn stuff} \\
(\texttt{Abs}\ f)[x \mapsto y] & = \text{more de bruijn stuff} \\
(\texttt{Node}\ a\ l\ r)[x \mapsto y] & = \texttt{Node}\ a\ l[x \mapsto y]\ r[x \mapsto y]
\end{align*}

\end{frame}

\begin{frame}
\frametitle{Structural Definitions on Programs}
\framesubtitle{Term extraction}

\begin{align*}
\textsf{tVarsIn}\ (\texttt{Var}\ n) & = \{ \} \\
\textsf{tVarsIn}\ (\texttt{Abs}\ f) &= \textsf{tVarsIn}\ f \\
\textsf{tVarsIn}\ \texttt{Leaf} &= \{ \} \\
\textsf{tVarsIn}\ (\texttt{Node}\ a\ l\ r) & = (\textsf{tVarsIn}\ l) \cup (\textsf{tVarsIn}\ r) \\
  & \cup (\textrm{if}\ a = \texttt{TVar}\ n\ \textrm{then}\ \{\texttt{TVar}\ n\}\ \textrm{else}\ \{ \})
\end{align*}

\end{frame}

\begin{frame}
\frametitle{Structural Definitions on Programs}
\framesubtitle{Deterministic Eager Small-Step Semantics}
\begin{gather*}
\prftree{l \rightarrow l^\prime}{\texttt{Node}\ a\ l\ r \rightarrow \texttt{Node}\ a\ l^\prime\ r}
\\
\prftree{\textsf{isValue}\ l}{r \rightarrow r^\prime}{\texttt{Node}\ a\ l\ r \rightarrow \texttt{Node}\ a\ l\ r^\prime}
\\
\prftree{\textsf{isValue}\ l}{\textsf{isValue}\ r}{\texttt{Node}\ a\ l\ r\ \textrm{evaluates to}\ x}{\texttt{Node}\ a\ l\ r \rightarrow x}
\end{gather*}
\end{frame}

\begin{frame}
\frametitle{Types, Subtypes, and Structural Properties}
\begin{itemize}
  \pause
  \item Type preservation under simple evaluation \textit{would} be a simple structural property...
  \pause
  \item Except for subtyping!
\end{itemize}
\pause
\begin{gather*}
\prftree
  {\Gamma \vdash \textcolor{BrickRed}{e}: \textsf{STM}\ 
    (\textsf{R:}\ \textcolor{MidnightBlue}{r})\ 
    (\textsf{W:}\ \textcolor{OliveGreen}{w})\ 
    \textcolor{BrickRed}{t}}
  {\textcolor{MidnightBlue}{r^\prime \geq r}}
  {\textcolor{OliveGreen}{w \geq w^\prime}}
  {\Gamma \vdash \textcolor{BrickRed}{e}: \textsf{STM}\ 
    (\textsf{R:}\ \textcolor{MidnightBlue}{r^\prime})\ 
    (\textsf{W:}\ \textcolor{OliveGreen}{w^\prime})\ 
    \textcolor{BrickRed}{t}}
\end{gather*}
\begin{itemize}
  \pause
  \item In Isabelle, this rule gives a straightforward induction rule over typing judgements...
  \pause
  \item But it gives a completely useless one for case elimination!
  \begin{itemize}
    \item If we use structural induction on $\rightarrow$, need to use case elimination to use inductive hypotheses
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Case Study: `Return'}

\pause
This rule for the STM term \texttt{return} (switching to Isabelle-ish syntax for formatting reasons):
\pause
\begin{align*}
&\ \Gamma \vdash v: \ t \\
\Rightarrow &\ \Gamma \vdash \texttt{return}\ v:  \textsf{STM}\ (\textsf{R:}\ \bot)\ (\textsf{W:}\ \top)\ t
\end{align*}

\pause
Leads to this elimination rule:
\pause
\begin{align*}
&\ \Gamma \vdash \texttt{return}\ \textcolor{BrickRed}{v}:  \textsf{STM}\ 
  (\textsf{R:}\ \textcolor{MidnightBlue}{r})\ 
  (\textsf{W:}\ \textcolor{OliveGreen}{w})\ 
  \textcolor{BrickRed}{t} \\
\Rightarrow &\ \forall\ 
  \textcolor{MidnightBlue}{r^\prime}\ 
  \textcolor{OliveGreen}{w^\prime}. \\ 
&\ \Gamma \vdash \texttt{return}\ \textcolor{BrickRed}{v}: \textsf{STM}\ 
  (\textsf{R:}\ \textcolor{MidnightBlue}{r^\prime})\ 
  (\textsf{W:}\ \textcolor{OliveGreen}{w^\prime})\ 
  \textcolor{BrickRed}{t} \\
&\ \wedge\ \textcolor{MidnightBlue}{r \geq r^\prime} 
  \wedge\ \textcolor{OliveGreen}{w^\prime \geq w} 
  \rightarrow P \\
\Rightarrow &\ \Gamma \vdash \textcolor{BrickRed}{v}: \textcolor{BrickRed}{t} \wedge 
  \textcolor{MidnightBlue}{r = \bot} 
  \wedge \textcolor{OliveGreen}{w = \top} 
  \rightarrow P \\
\Rightarrow &\ P
\end{align*}

\end{frame}

\begin{frame}
\frametitle{Case Study: `Return'}
\begin{itemize}
  \pause
  \item We really only care about the second case, where we get the inner ``$\Gamma \vdash v: t$''
  \pause
  \item But the first case is the same as the case we're trying to eliminate!
  \pause
  \item We can manually make a `folded-up' version of case elimination:
\end{itemize}

\pause
\begin{align*}
&\ \Gamma \vdash \texttt{return}\ \textcolor{BrickRed}{v}: \textsf{STM}\ 
  (\textsf{R:}\ \textcolor{MidnightBlue}{r})\ 
  (\textsf{W:}\ \textcolor{OliveGreen}{w})\ 
  \textcolor{BrickRed}{t} \\
\Rightarrow &\ 
  \Gamma \vdash \textcolor{BrickRed}{v}: \textcolor{BrickRed}{t}
  \wedge\ \textcolor{MidnightBlue}{r \geq \bot} 
  \wedge\ \textcolor{OliveGreen}{\top \geq w} 
  \rightarrow P \\
\Rightarrow &\ P
\end{align*}

\begin{itemize}
  \pause
  \item This looks automatable! \pause (TODO \frownie{})
\end{itemize}

\end{frame}


\section{Future Work}

\begin{frame}
\frametitle{Things that went out of scope}
\begin{itemize}
  \pause
  \item Concurrency- and termination-sensitive models of security
  \pause
  \item More expressive security policy languages, such as intransitive non-interference
  \pause
  \item A full IO model in the STM language (\texttt{fork}, \texttt{getChar}, etc)
  \pause
  \item Refinement (especially regarding STM implementation)
  \pause
  \item Automating subtyping-relation-style lemmas
\end{itemize}
\end{frame}

\section{Question Time}

\end{document}
